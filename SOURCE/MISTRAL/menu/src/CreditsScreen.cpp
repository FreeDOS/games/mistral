
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "Compat.h"
#include "Enums.h"
#include "FixP.h"
#include "Utils.h"
#include "Vec.h"
#include "Dungeon.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "Renderer.h"
#include "Engine.h"
#include "Actor.h"
#include "PackedFileReader.h"
#include "Rasterizer.h"
#include "Tesselation.h"
#include "LoadBitmap.h"
#include "UI.h"
#include "SoundSystem.h"

#include "CreditsScreen.h"

const static char *CreditsScreen_options[1] = {"Back"};

const static enum EGameMenuState CreditsScreen_nextStateNavigation[1] = {
		kMainMenu,
};

CreditsScreen::CreditsScreen(int32_t tag) : MenuState(tag, kMainMenu) {

	PackedFile::StaticBuffer fileInput = PackedFile::loadFromPath("Credits.txt");

	memset(&textBuffer[0], ' ', (XRES_FRAMEBUFFER / 8) * 25);
	mainText = &textBuffer[0];
	memset(&textBuffer[0], 0, ((XRES_FRAMEBUFFER / 8) * 25));
	memcpy(&textBuffer[0], fileInput.data, fileInput.size);

	optionsCount = sizeof(CreditsScreen_nextStateNavigation)/sizeof(enum EGameMenuState);

	monty = loadBitmap("monty.img");
	belle = loadBitmap("belle.img");
	stdmatt = loadBitmap("stdmatt.img");
}

void CreditsScreen::initialPaint(void) {}

void CreditsScreen::repaint(void) {
	int lines = Utils::countLines(mainText);

	int optionsHeight = 8 * (optionsCount);
	size_t len = UI::computeBoxWidth("Credits", CreditsScreen_options, 1);

	if (currentBackgroundBitmap != NULL) {
		drawRepeatBitmap(0, 0, XRES_FRAMEBUFFER, YRES_FRAMEBUFFER, currentBackgroundBitmap);
	}

	if (currentPresentationState == kAppearing || currentPresentationState == kFadingOut) {
		if (timeUntilNextState > UI::kMenuItemTimeToBecomeActiveInMs) {
			return;
		}
	}

	long progression = UI::kMenuItemTimeToBecomeActiveInMs - timeUntilNextState;

	if (mainText != NULL) {
		UI::drawTextWindow(1, 1, (XRES_FRAMEBUFFER / 8), (lines + 3) + 1, "Credits", mainText, currentPresentationState, UI::kMenuItemTimeToBecomeActiveInMs - timeUntilNextState);
	}

	UI::drawImageWindow( 2, 16, 8, 9, "Monty",   monty,   currentPresentationState, progression);
	UI::drawImageWindow(11, 16, 8, 9, "StdMatt", stdmatt, currentPresentationState, progression);
	UI::drawImageWindow(20, 16, 8, 9, "Belle",    belle,  currentPresentationState, progression);

	UI::drawTextOptionsWindow(
			(XRES_FRAMEBUFFER / 8) - len - 3,
			((YRES_FRAMEBUFFER - optionsHeight) / 8) - 2,
			"Credits",
			&CreditsScreen_options[0],
			optionsCount,
			cursorPosition,
			currentPresentationState,
			progression);

}

enum EGameMenuState CreditsScreen::tick(int32_t tag, long delta) {

	timeUntilNextState -= delta;

	if (timeUntilNextState <= 0) {
		enum EGameMenuState navigation = blinkCursor();
		if (navigation != kNoChange) {
			return navigation;
		}
	}

	if (currentPresentationState == kWaitingForInput) {
		enum EGameMenuState navigation = handleNavigation(tag, &CreditsScreen_nextStateNavigation[0]);
		if (navigation != kNoChange) {
			return navigation;
		}
	}

	return kNoChange;
}

CreditsScreen::~CreditsScreen() {
	releaseBitmap(monty);
	releaseBitmap(belle);
	releaseBitmap(stdmatt);
}
