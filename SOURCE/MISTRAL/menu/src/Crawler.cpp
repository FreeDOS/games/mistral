#include <string.h>

#include "Compat.h"
#include "SpyTravel.h"
#include "FixP.h"
#include "Utils.h"
#include "Enums.h"
#include "Vec.h"
#include "Actor.h"
#include "LoadBitmap.h"
#include "Dungeon.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "Renderer.h"
#include "Rasterizer.h"
#include "Tesselation.h"
#include "Engine.h"
#include "SpyTravel.h"
#include "SoundSystem.h"
#include "UI.h"

#include "Crawler.h"

#define DEFAULT_TURN_TIME 4000

const static char *missionNames[9] = {
		"Ribeira de Gaia, Porto",
		"Boca do Inferno, Lisboa",
		"Parque de El Retiro, Madrid",
		"Casa Mila, Barcelona",
		"Erdolraffinerie, Frankfurt",
		"Hamburger Hafen, Hamburg",
		"La maison de maitre, Luxembourg",
		"Base secrete, Brussels",
		"Camp dentrainement, Charleroi"
};

bool autoPassTurn = false;
bool spyHasClue = false;
char *thisMissionName;
size_t thisMissionNameLen;
bool showPromptToAbandonMission = false;
int mapIndex = 0;
int timeUntilEndOfTurn = DEFAULT_TURN_TIME;

const static char *AbandonMission_Title = "Abandon mission?";
const static char *AbandonMission_options[2] = {"Continue", "End game"};
const static enum EGameMenuState AbandonMission_navigation[2] = {kNoChange, kMainMenu};

PlayMissionState::PlayMissionState(int32_t tag) : MenuState(tag, kPlayGame) {
	optionsCount = sizeof(AbandonMission_navigation) / sizeof(enum EGameMenuState);
	spyHasClue = false;
	showPromptToAbandonMission = false;

	biggestOption = UI::computeBoxWidth(AbandonMission_Title, AbandonMission_options, optionsCount);

	mapIndex = SpyGame::getPlayerLocation();
	thisMissionName = (char *) missionNames[mapIndex];
	thisMissionNameLen = (int16_t) (strlen(thisMissionName));
}

void PlayMissionState::initialPaint() {
	drawRepeatBitmap(0, 0, XRES_FRAMEBUFFER, YRES_FRAMEBUFFER, currentBackgroundBitmap);

	fill(12 * 8, 12 * 8, 18 * 8, 16, getPaletteEntry(0xFF000000), true);
	fill(11 * 8, 12 * 8, 18 * 8, 8, getPaletteEntry(0xFFFFFFFF), false);
	fill(11 * 8, 11 * 8, 18 * 8, 8, getPaletteEntry(0xFF000000), false);
	drawRect(11 * 8, 11 * 8, 18 * 8, 16, getPaletteEntry(0xFF000000));
	drawTextAt(13, 13, "Decoding mission", getPaletteEntry(0xFF000000));
	drawTextAt(13, 12, "Please wait...", getPaletteEntry(0xFFFFFFFF));

	needsToRedrawVisibleMeshes = true;
	flipRenderer();
	initRoom(mapIndex);
}

void PlayMissionState::repaint() {

	if (currentPresentationState == kFadingOut || currentPresentationState == kAppearing) {
		long progression = timeUntilNextState;

		if (currentPresentationState == kAppearing) {
			progression = UI::kMenuItemTimeToBecomeActiveInMs - timeUntilNextState;
		}

		int size = Utils::lerpInt(0, 160, progression, UI::kMenuItemTimeToBecomeActiveInMs);

		if (timeUntilNextState > UI::kMenuItemTimeToBecomeActiveInMs) {
			return;
		}

		drawRepeatBitmap(0, 0, XRES_FRAMEBUFFER, YRES_FRAMEBUFFER, currentBackgroundBitmap);

		drawRect(8 + 80 - size / 2, 16 + 80 - size / 2, size, size, getPaletteEntry(0xFF000000));
		return;
	} else if (showPromptToAbandonMission) {

		int optionsHeight = 8 * (optionsCount);

		drawRepeatBitmap(0, 0, XRES_FRAMEBUFFER, YRES_FRAMEBUFFER, currentBackgroundBitmap);

		fill(0, 0, XRES_FRAMEBUFFER, YRES_FRAMEBUFFER, getPaletteEntry(0xFF000000), true);

		fill(XRES_FRAMEBUFFER - (biggestOption * 8) - 8 - 16, YRES_FRAMEBUFFER - optionsHeight - 8 - 16,
			 (biggestOption * 8) + 16, optionsHeight + 16, getPaletteEntry(0xFF000000), true);

		fill(XRES_FRAMEBUFFER - (biggestOption * 8) - 16 - 16, YRES_FRAMEBUFFER - optionsHeight - 16 - 16,
			 (biggestOption * 8) + 16, optionsHeight + 16, getPaletteEntry(0xFFFFFFFF), false);

		drawRect(XRES_FRAMEBUFFER - (biggestOption * 8) - 16 - 16,
				 YRES_FRAMEBUFFER - optionsHeight - 16 - 16, (biggestOption * 8) + 16,
				 optionsHeight + 16, getPaletteEntry(0xFF000000));

		if (AbandonMission_Title != NULL) {

			fill(((XRES_FRAMEBUFFER / 8) - biggestOption - 2 - 2) * 8,
				 ((26 - optionsCount) - 2 - 1 - 2) * 8,
				 (biggestOption + 2) * 8, 8, getPaletteEntry(0xFF000000), false);

			drawTextAt((XRES_FRAMEBUFFER / 8) - biggestOption - 2, (26 - optionsCount) - 4,
					   AbandonMission_Title, getPaletteEntry(0xFFFFFFFF));
		}

		for (int c = 0; c < optionsCount; ++c) {

			bool isCursor = (cursorPosition == c)
							&& ((currentPresentationState == kConfirmInputBlink1)
								|| (currentPresentationState == kConfirmInputBlink3)
								|| (currentPresentationState == kConfirmInputBlink5)
								|| (currentPresentationState == kWaitingForInput));

			if (isCursor) {
				fill(XRES_FRAMEBUFFER - (biggestOption * 8) - 16 - 8 - 8,
					 (YRES_FRAMEBUFFER - optionsHeight) + (c * 8) - 8 - 8,
					 (biggestOption * 8) + 16, 8, getPaletteEntry(0xFF000000), false);
			}

			drawTextAt(
					(XRES_FRAMEBUFFER / 8) - biggestOption - 2, (26 - optionsCount) + c - 2,
					&AbandonMission_options[c][0],
					isCursor ? getPaletteEntry(0xFFFF0000) : getPaletteEntry(0xFF000000));
		}
	} else {
		renderTick(1);

		if (currentPresentationState == kAppearing) {
			drawTextAt(16 - (thisMissionNameLen / 2), 10, thisMissionName, getPaletteEntry(0xFF000000));
		}
	}
}

enum EGameMenuState Crawler_success(int crawlerCode) {

	if (crawlerCode == kCrawlerGameOver) {
		return kGameOver;
	}

	if (spyHasClue) {
		int location = SpyGame::getPlayerLocation();

		SpyGame::setAsRescued(location);
		if (location < 7) {
			SpyGame::goTo(SpyGame::getPlayerLocation() + 1);
		} else {
			return kVictory;
		}

	}

	return kGameMenu;
}

enum EGameMenuState PlayMissionState::tick(int32_t tag, long delta) {
	enum ECommand cmd = (enum ECommand) (tag);

	if (showPromptToAbandonMission) {

		timeUntilNextState -= delta;
		if (timeUntilNextState <= 0) {
			enum EGameMenuState navigation = blinkCursor();
			if (navigation != kNoChange) {
				return navigation;
			}
		}

		if (currentPresentationState == kWaitingForInput) {

			switch (tag) {
				case kCommandBack:
					return kMainMenu;
				case kCommandUp:
					playSound(MENU_SELECTION_CHANGE_SOUND);
					cursorPosition = (cursorPosition - 1);

					if (cursorPosition >= optionsCount) {
						cursorPosition = optionsCount - 1;
					}
					break;
				case kCommandDown:
					playSound(MENU_SELECTION_CHANGE_SOUND);
					cursorPosition =
							(uint8_t) ((cursorPosition + 1) % optionsCount);

					break;
				case kCommandFire1:
				case kCommandFire2:
				case kCommandFire3:

					if (cursorPosition == 0) {
						showPromptToAbandonMission = false;
						needsToRedrawVisibleMeshes = true;
						currentPresentationState = kAppearing;
						timeUntilNextState = UI::kMenuItemTimeToBecomeActiveInMs;
						return kNoChange;
					}
					timeUntilNextState = 0;
					nextNavigationSelection = AbandonMission_navigation[cursorPosition];
					currentPresentationState = kConfirmInputBlink1;
					break;
			}
		}

		return kNoChange;
	}

	if (cmd == kCommandBack) {
		showPromptToAbandonMission = true;
		timeUntilNextState = 0;
		return kNoChange;
	}

	if (timeUntilNextState != kNonExpiringPresentationState) {
		timeUntilNextState -= delta;
	}

	if (currentPresentationState != kFadingOut) {

		if (autoPassTurn) {
			timeUntilEndOfTurn -= delta;

			if (timeUntilEndOfTurn <= 0) {
				cmd = kCommandPassTurn;
				timeUntilEndOfTurn = DEFAULT_TURN_TIME;
			}
		}

		int returnCode = loopTick(cmd);
		spyHasClue = (returnCode == kCrawlerClueAcquired);

		switch (returnCode) {
			case kCrawlerGameOver: {

				prepareCameraForPlayerDeath();
				hideGun();

				currentPresentationState = kFadingOut;
				timeUntilNextState = kDefaultPresentationStateInterval;
			}
				break;
			case kCrawlerClueAcquired:
				return Crawler_success(returnCode);
		}
	}

	if (timeUntilNextState <= 0) {

		switch (currentPresentationState) {
			case kAppearing:
				currentPresentationState = kWaitingForInput;
				timeUntilNextState = kNonExpiringPresentationState;
				break;
			case kFadingOut:
				return Crawler_success(kCrawlerGameOver);
			case kWaitingForInput:
				return kNoChange;
			case kConfirmInputBlink1:
			case kConfirmInputBlink2:
			case kConfirmInputBlink3:
			case kConfirmInputBlink4:
			case kConfirmInputBlink5:
			case kConfirmInputBlink6:
				break;
		}

		needsToRedrawVisibleMeshes = true;
	}

	return kNoChange;
}

PlayMissionState::~PlayMissionState() {}
