#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "Compat.h"
#include "SpyTravel.h"
#include "FixP.h"
#include "Enums.h"
#include "Vec.h"
#include "Actor.h"
#include "LoadBitmap.h"
#include "Dungeon.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "Renderer.h"
#include "Engine.h"
#include "PackedFileReader.h"
#include "SoundSystem.h"

#include "MainMenu.h"
#include "Crawler.h"
#include "Interrogation.h"
#include "HelpScreen.h"
#include "CreditsScreen.h"
#include "GameMenu.h"
#include "UI.h"

MenuState *currentMenuState = NULL;
bool isRunning = true;
enum EGameMenuState currentGameMenuState = kNoChange;
const char *mainText = NULL;
char textBuffer[(XRES_FRAMEBUFFER / 8) * 25];
enum ESoundDriver soundDriver = kNoSound;

MenuState::MenuState(int32_t tag, enum EGameMenuState up) :
		currentBackgroundBitmap(loadBitmap("pattern.img")),
		cursorPosition(0),
		biggestOption(0),
		timeUntilNextState(UI::kMenuItemTimeToBecomeActiveInMs),
		nextNavigationSelection(kNoChange),
		stateUp(up),
		currentPresentationState(kAppearing) {
}

MenuState::~MenuState() {
	if (currentBackgroundBitmap != NULL) {
		releaseBitmap(currentBackgroundBitmap);
		currentBackgroundBitmap = NULL;
	}
}

enum EGameMenuState MenuState::handleNavigation(int32_t cmd, const enum EGameMenuState *routes) {

		switch (cmd) {
			case kCommandBack:
				return stateUp;
			case kCommandUp:
				playSound(MENU_SELECTION_CHANGE_SOUND);
				cursorPosition = (cursorPosition - 1);

				if (cursorPosition >= optionsCount) {
					cursorPosition = (optionsCount - 1);
				}
				break;
			case kCommandDown:
				playSound(MENU_SELECTION_CHANGE_SOUND);
				cursorPosition =
						(uint8_t) ((cursorPosition + 1) % optionsCount);
				break;
			case kCommandFire1:
			case kCommandFire2:
			case kCommandFire3:
				nextNavigationSelection =
						routes[cursorPosition];
				currentPresentationState = kConfirmInputBlink1;
				break;
			default:
				return kNoChange;
		}
	return kNoChange;
}

enum EGameMenuState  MenuState::blinkCursor() {
		switch (currentPresentationState) {
			case kAppearing:
				timeUntilNextState = UI::kMenuItemTimeToBecomeActiveInMs;
				currentPresentationState = kWaitingForInput;
				break;
			case kWaitingForInput:
				break;
			case kConfirmInputBlink1:
			case kConfirmInputBlink2:
			case kConfirmInputBlink3:
			case kConfirmInputBlink4:
			case kConfirmInputBlink5:
			case kConfirmInputBlink6:
				timeUntilNextState = kMenuItemBlinkIntervalInMs;
				currentPresentationState =
						(enum EPresentationState) ((int) currentPresentationState + 1);
				break;
			case kFadingOut:
				return nextNavigationSelection;
		}

		return kNoChange;
}

void enterState(enum EGameMenuState newState) {

	if (currentMenuState != NULL) {
		delete currentMenuState;
		currentMenuState = NULL;
	}

	switch (newState) {
		default:
		case kMainMenu:
			currentMenuState = new MainMenu(newState);
			break;
		case kHelp:
			currentMenuState = new HelpScreen(newState);
			break;
		case kCredits:
			currentMenuState = new CreditsScreen(newState);
			break;
			/* this has to fall thru to kPlayGame, so we continue playing */
		case kPlayGame:
		case kPlayStory:
			SpyGame::init(kPlayStory == newState);
		case kGameMenu:
			newState = kGameMenu;
		case kStatusMenu:
		case kDossiersMenu:
		case kReadDossier_Sofia:
		case kReadDossier_Ricardo:
		case kReadDossier_Lola:
		case kReadDossier_Pau:
		case kReadDossier_Lina:
		case kReadDossier_Elias:
		case kReadDossier_Carmen:
		case kVictory:
		case kGameOver:
		case kReadDossier_Jean:
			currentMenuState = new GameMenu(newState);
			break;
		case kPrologue:
			currentMenuState = new PlayMissionState(newState);
			break;
		case kInterrogate_Sofia:
		case kInterrogate_Ricardo:
		case kInterrogate_Lola:
		case kInterrogate_Pau:
		case kInterrogate_Lina:
		case kInterrogate_Jean:
		case kInterrogate_Elias:
		case kInterrogate_Carmen:
			currentMenuState = new Interrogation(newState);
			break;

		case kInvestigateMenu: {
			currentMenuState = new PlayMissionState(newState);
		}
			break;
		case kQuit:
#ifndef UWP
			isRunning = false;
#else
			graphicsShutdown();
#endif
			break;
		case kEndGame:
			currentMenuState = new GameMenu(newState);
			break;
	}

	currentGameMenuState = newState;

	if (currentMenuState != NULL) {
		currentMenuState->initialPaint();
	}
}

bool menuTick(long delta_time) {

	handleSystemEvents();

	if (soundDriver != kNoSound) {
		soundTick();
	}

	enum ECommand input = getInput();

	if (currentMenuState == NULL) {
		return true;
	}

	enum EGameMenuState newState = currentMenuState->tick(input, delta_time);

	if (input == kCommandQuit) {
#ifndef UWP
		isRunning = false;
#else
		graphicsShutdown();
#endif
		return false;
	}

	if (newState != currentGameMenuState && newState != -1) {
		playSound(STATE_CHANGE_SOUND);
		enterState(newState);
	}

	if (currentMenuState != NULL) {
		currentMenuState->repaint();
	}

	flipRenderer();

	return isRunning;
}

#ifdef __EMSCRIPTEN__
void mainLoop () {
  menuTick ( 50 );
}
#endif
