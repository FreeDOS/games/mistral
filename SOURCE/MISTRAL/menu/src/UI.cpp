/*
* Created by Daniel Monteiro on 2019-08-02.
*/

#include <stdio.h>
#include <string.h>

#include "Compat.h"
#include "Enums.h"
#include "FixP.h"
#include "Utils.h"
#include "LoadBitmap.h"
#include "Vec.h"
#include "Dungeon.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "Renderer.h"
#include "Rasterizer.h"
#include "Tesselation.h"
#include "Engine.h"
#include "UI.h"

namespace UI {
	void drawWindow(const int x,
					const int y,
					const int dx,
					const int dy,
					const char *title) {
		const static FramebufferPixelFormat black = getPaletteEntry(0xFF000000);
		const static FramebufferPixelFormat white = getPaletteEntry(0xFFFFFFFF);

		fill((x) * 8, (y) * 8, dx * 8, dy * 8, black, true);
		fill((x - 1) * 8, (y - 1) * 8, dx * 8, dy * 8, white, false);
		drawRect((x - 1) * 8, (y - 1) * 8, dx * 8, dy * 8, black);
		fill((x - 1) * 8, (y - 1) * 8, dx * 8, 8, black, false);
		drawTextAt(x + 1, y, title, white);
	}

	void drawTextWindow(const int x,
						const int y,
						const int dx,
						const int dy,
						const char *title,
						const char *content,
						const EPresentationState presentationState,
						long progression) {

		if (presentationState == kAppearing || presentationState == kFadingOut) {

			if (presentationState == kFadingOut) {
				progression = kMenuItemTimeToBecomeActiveInMs - progression;
			}

			int sizeX = Utils::lerpInt(0, dx * 8, progression, kMenuItemTimeToBecomeActiveInMs);
			int sizeY = Utils::lerpInt(0, dy * 8, progression, kMenuItemTimeToBecomeActiveInMs);

			drawRect(((x - 1) * 8) + (((dx * 8) - sizeX) / 2),
					 ((y - 1) * 8) + (((dy * 8) - sizeY) / 2),
					 sizeX,
					 sizeY,
					 getPaletteEntry(0xFF000000));
		} else {
			drawWindow(x, y, dx, dy, title);
			drawTextAt(x + 1, y + 2, content, getPaletteEntry(0xFF000000));
		}
	}

	void drawImageWindow(const int x,
						 const int y,
						 const int dx,
						 const int dy,
						 const char *title,
						 struct Bitmap *content,
						 enum EPresentationState presentationState,
						 long progression) {

		const static FramebufferPixelFormat black = getPaletteEntry(0xFF000000);
		const static FramebufferPixelFormat white = getPaletteEntry(0xFFFFFFFF);

		if (presentationState == kAppearing || presentationState == kFadingOut) {

			if (presentationState == kFadingOut) {
				progression = kMenuItemTimeToBecomeActiveInMs - progression;
			}

			int sizeX = Utils::lerpInt(0, dx * 8, progression, kMenuItemTimeToBecomeActiveInMs);
			int sizeY = Utils::lerpInt(0, dy * 8, progression, kMenuItemTimeToBecomeActiveInMs);

			drawRect(((x - 1) * 8) + (((dx * 8) - sizeX) / 2),
					 ((y - 1) * 8) + (((dy * 8) - sizeY) / 2),
					 sizeX,
					 sizeY,
					 getPaletteEntry(0xFF000000));
		} else {
			fill((x) * 8, (y) * 8, dx * 8, dy * 8, black, true);
			fill((x - 1) * 8, (y - 1) * 8, dx * 8, dy * 8, white, false);
			drawBitmap((x - 1) * 8, (y) * 8, content, true);
			drawRect((x - 1) * 8, (y - 1) * 8, dx * 8, dy * 8, black);
			fill((x - 1) * 8, (y - 1) * 8, dx * 8, 8, black, false);
			drawTextAt(x + 1, y, title, white);
		}
	}

	void drawTextOptionsWindow( const int x,
								const int y,
								const char* title,
								const char** options,
								const int optionsCount,
								const int cursorPosition,
								const EPresentationState presentationState,
								long progression) {

		// + 2 here is for the margins
		long biggestOption = UI::computeBoxWidth(title, options, optionsCount) + 2;

		uint8_t optionsHeight = 8 * (optionsCount + 2);

		if (presentationState == kAppearing || presentationState == kFadingOut) {

			if (presentationState == kFadingOut) {
				progression = kMenuItemTimeToBecomeActiveInMs - progression;
			}

			size_t sizeX = Utils::lerpInt(0, biggestOption * 8, progression, UI::kMenuItemTimeToBecomeActiveInMs);
			size_t sizeY = Utils::lerpInt(0, optionsHeight, progression, UI::kMenuItemTimeToBecomeActiveInMs);

			drawRect( ((x - 1) * 8) + ((biggestOption * 8) / 2) - sizeX / 2,
					  ((y - 1) * 8) + (optionsHeight / 2) - sizeY / 2,
					  sizeX,
					  sizeY,
					  getPaletteEntry(0xFF000000));
		} else {
			UI::drawWindow(
					x,
					y,
					biggestOption,
					optionsCount + 2, // + 2 for title and padding
					title);

			int px = (x - 1) * 8;
			int py = (y + 1) * 8; // + 2 for the same reason as above

			FramebufferPixelFormat selectedItemColor = getPaletteEntry(0xFFFF0000);
			FramebufferPixelFormat normalItemColor = getPaletteEntry(0xFF000000);
			FramebufferPixelFormat cursorColor = getPaletteEntry(0xFF000000);

			for (int c = 0; c < optionsCount; ++c) {

				bool isCursor =
						(cursorPosition == c) &&
						((presentationState == kConfirmInputBlink1) ||
						 (presentationState == kConfirmInputBlink3) ||
						 (presentationState == kConfirmInputBlink5) ||
						 (presentationState == kWaitingForInput));

				if (isCursor) {
					fill(px,
						 py,
						 (biggestOption * 8),
						 8,
						 cursorColor,
						 false);
				}

				drawTextAt(
						x + 1, // + 1 for margin
						y + c + 2, // + 2 for title and padding, just like above
						&options[c][0],
						isCursor ? selectedItemColor : normalItemColor);

				py += 8;
			}
		}
	}

	size_t computeBoxWidth( const char *title,
							const char **options,
							const int optionsCount) {
		size_t biggestOption = strlen(title);

		for (int c = 0; c < optionsCount; ++c) {
			size_t len = strlen(options[c]);

			if (len > biggestOption) {
				biggestOption = len;
			}
		}

		return biggestOption;
	}
}
