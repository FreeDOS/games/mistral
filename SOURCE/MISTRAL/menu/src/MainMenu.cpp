#include <string.h>

#include "Compat.h"
#include "FixP.h"
#include "Utils.h"
#include "Enums.h"
#include "Vec.h"
#include "Actor.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "Vec.h"
#include "LoadBitmap.h"
#include "Dungeon.h"
#include "Renderer.h"
#include "Engine.h"
#include "Renderer.h"
#include "Rasterizer.h"
#include "Tesselation.h"
#include "UI.h"
#include "SoundSystem.h"

#include "MainMenu.h"

#ifdef MAIN_MENU_HAS_QUIT_OPTION
const static char *MainMenu_options[4] = {
		"New game", "Credits", "Help", "Quit"};

const static enum EGameMenuState MainMenu_nextStateNavigation[4] = {
		kPlayGame, kCredits, kHelp,
		kQuit};
#else
const static char *MainMenu_options[3] = {
		"New game", "Credits", "Help"};

const static enum EGameMenuState MainMenu_nextStateNavigation[3] = {
		kPlayGame, kCredits, kHelp};
#endif

const static char *kMainMenuTitleText = "Play Game";

MainMenu::MainMenu(int32_t tag) : MenuState(tag, kQuit) {
	logoBitmap = loadBitmap("mistral.img");
	optionsCount = sizeof(MainMenu_nextStateNavigation)/sizeof(enum EGameMenuState);
	biggestOption = UI::computeBoxWidth(kMainMenuTitleText, MainMenu_options, optionsCount);
}

void MainMenu::initialPaint() {}

void MainMenu::repaint(void) {

	drawRepeatBitmap(0, 0, XRES_FRAMEBUFFER, YRES_FRAMEBUFFER, currentBackgroundBitmap);

	uint8_t optionsHeight = 8 * optionsCount;

	UI::drawImageWindow(2, 2, 21, 22, "Invisible affairs", logoBitmap, currentPresentationState, UI::kMenuItemTimeToBecomeActiveInMs - timeUntilNextState);

	UI::drawTextOptionsWindow(
			(XRES_FRAMEBUFFER / 8) - (int) biggestOption - 3,
			(YRES_FRAMEBUFFER / 8) - 4 - (optionsHeight / 8),
			kMainMenuTitleText,
			MainMenu_options,
			optionsCount,
			cursorPosition,
			currentPresentationState,
			UI::kMenuItemTimeToBecomeActiveInMs - timeUntilNextState);
}

enum EGameMenuState MainMenu::tick(int32_t tag, long delta) {

	timeUntilNextState -= delta;

	if (timeUntilNextState <= 0) {
		enum EGameMenuState navigation = blinkCursor();
		if (navigation != kNoChange) {
			return navigation;
		}
	}

	enum EGameMenuState navigation = blinkCursor();
	if (navigation != kNoChange) {
		return navigation;
	}

	if (currentPresentationState == kWaitingForInput) {
		enum EGameMenuState navigation = handleNavigation(tag, &MainMenu_nextStateNavigation[0]);
		if (navigation != kNoChange) {
			return navigation;
		}
	}

	return kNoChange;
}

MainMenu::~MainMenu() {
	releaseBitmap(logoBitmap);
}
