#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "Compat.h"
#include "SpyTravel.h"
#include "FixP.h"
#include "Utils.h"
#include "Enums.h"
#include "Vec.h"
#include "Actor.h"
#include "LoadBitmap.h"
#include "Dungeon.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "Renderer.h"
#include "Rasterizer.h"
#include "Tesselation.h"
#include "Engine.h"
#include "SpyTravel.h"
#include "SoundSystem.h"
#include "UI.h"
#include "PackedFileReader.h"

#include "Interrogation.h"

#define kMazeSideSize 8

bool interrogationMap[kMazeSideSize][kMazeSideSize];
bool interrogationReveal[kMazeSideSize][kMazeSideSize];

void Interrogation::initMaze(const int indexZeroBased) {
	char filename[16];

	char *index;
	char mazeBuffer[(kMazeSideSize + 1) * kMazeSideSize]; /* + 1 for \n */
	int randomTarget = 1 + (rand() % ((kMazeSideSize * 2) - 1));
	int randomStart = 1 + (rand() % ((kMazeSideSize * 2) - 1));
	int randomOrder = (rand() % 2);

	if (indexZeroBased != -1) {
		SpyGame::getSuspectName(indexZeroBased, &suspectName[0], 256);

		sprintf(&filename[0], "%s.img", suspectName);
		character = loadBitmap(filename);
		sprintf(&filename[0], "%s.maze", suspectName);
	} else {
		sprintf(&filename[0], "Sofia.img");
		character = loadBitmap(filename);
		sprintf(&filename[0], "practice.maze");
	}

	memset(interrogationReveal, 0,
		   kMazeSideSize * kMazeSideSize * sizeof(bool));

	PackedFile::StaticBuffer fileInput = PackedFile::loadFromPath(filename);
	memcpy(&mazeBuffer[0], fileInput.data, fileInput.size);

	index = mazeBuffer;
	for (int y = 0; y < kMazeSideSize; ++y) {
		for (int x = 0; x < kMazeSideSize; ++x) {
			interrogationMap[y][x] = ((*index) == '1');

			if (!interrogationMap[y][x]) {
				if (randomOrder) {
					if (y < 4) {
						randomTarget--;
					} else {
						randomStart--;
					}

				} else {
					if (y > 4) {
						randomTarget--;
					} else {
						randomStart--;
					}
				}
			}

			if (!randomStart) {
				interrogationPosition.x = x;
				interrogationPosition.y = y;
				interrogationMap[y][x] = false;
			}

			if (!randomTarget) {
				interrogationTargetSpot.x = x;
				interrogationTargetSpot.y = y;
				interrogationMap[y][x] = false;
			}

			++index;
		}
		++index;
	}
}

Interrogation::Interrogation(int32_t tag) : MenuState(tag, kMainMenu) {
	isPracticingInterrogation = false;
	noMoreMoves = false;
	initMaze(tag - kInterrogate_Sofia);

	emotions[0] = loadBitmap("love.img");
	emotions[1] = loadBitmap("rage.img");
	emotions[2] = loadBitmap("nostalgia.img");
	emotions[3] = loadBitmap("volatility.img");

	eyesL = loadBitmap("eyesl.img");
	eyesR = loadBitmap("eyesr.img");
	eyesW = loadBitmap("eyesw.img");
	eyesB = loadBitmap("eyesb.img");
	eyesC = loadBitmap("eyesc.img");

	timeUntilNextState = 10000 - 1;

	currentPresentationState = kWaitingForInput;
}

void Interrogation::initialPaint() {}

void Interrogation::repaint() {

	int distance;
	int blinkTime;
	int sideEyeTime;

	drawRepeatBitmap(0, 0, XRES_FRAMEBUFFER, YRES_FRAMEBUFFER, currentBackgroundBitmap);

	fill(64 + 8, 8 + 8, 160, 128, getPaletteEntry(0xFF000000), true);
	fill(64, 8, 160, 128, getPaletteEntry(0xFFFFFFFF), false);
	drawBitmap(64, 8, character, true);

	drawRect(64, 8, 160, 128, getPaletteEntry(0xFF000000));
	fill(64, 8, 160, 8, getPaletteEntry(0xFF000000), false);
	drawTextAt(10, 2, suspectName, getPaletteEntry(0xFFFFFFFF));

	fill(8, 144, 120, 8, getPaletteEntry(0xFF000000), false);
	fill(142, 144, 80, 8, getPaletteEntry(0xFF000000), false);
	fill(236, 144, 64, 8, getPaletteEntry(0xFF000000), false);

	drawTextAt(3, 19, "Interrogation", getPaletteEntry(0xFFFFFFFF));
	drawTextAt(20, 19, "Emotions", getPaletteEntry(0xFFFFFFFF));
	drawTextAt(32, 19, "Stress", getPaletteEntry(0xFFFFFFFF));

	fill(8 + 8, 144 + 8, 120, 48, getPaletteEntry(0xFF000000), true);
	fill(142 + 8, 144 + 8, 80, 48, getPaletteEntry(0xFF000000), true);
	fill(236 + 8, 144 + 8, 64, 48, getPaletteEntry(0xFF000000), true);

	fill(8, 152, 120, 40, getPaletteEntry(0xFFFFFFFF), false);
	fill(142, 152, 80, 40, getPaletteEntry(0xFFFFFFFF), false);
	fill(236, 152, 64, 40, getPaletteEntry(0xFFFFFFFF), false);

	drawRect(8, 152, 120, 40, getPaletteEntry(0xFF000000));
	drawRect(142, 152, 80, 40, getPaletteEntry(0xFF000000));
	int c;
	for (c = 0; c < 4; ++c) {
		drawBitmap(142 + 8, (156 + (c * 8)), emotions[c],
				   false);
	}

	for (c = 0; c < 4; ++c) {
		fill(142 + 20, (156 + (c * 8)), emotionAmount[c] * 2,
			 8, getPaletteEntry(0xFF999999), false);

		drawRect(142 + 20, (156 + (c * 8)),
				 emotionAmount[c] * 2, 8, getPaletteEntry(0xFF000000));
	}

	drawBitmap(40, 154, emotions[0], false);
	drawBitmap(40, 182, emotions[1], false);
	drawBitmap(88, 154, emotions[2], false);
	drawBitmap(88, 182, emotions[3], false);
	int y;
	for (y = 0; y < kMazeSideSize; ++y) {
		for (int x = 0; x < kMazeSideSize; ++x) {

			if (!interrogationReveal[y][x]
				&& ((abs(interrogationPosition.x - x) > 1)
					|| (abs(interrogationPosition.y - y) > 1))) {
				fill(52 + x * 4, 156 + (y * 4), 4, 4, getPaletteEntry(0xFF222222), false);
				continue;
			}

			if (interrogationTargetSpot.x == x && interrogationTargetSpot.y == y) {
				fill(52 + x * 4, 156 + (y * 4), 4, 4, getPaletteEntry(0xFF888888), false);
			} else {


				if (interrogationMap[y][x]) {
					fill(52 + x * 4, 156 + (y * 4), 4, 4, getPaletteEntry(0xFF559955), false);
				} else {
					fill(52 + x * 4, 156 + (y * 4), 4, 4, getPaletteEntry(0xFFFFFFFF), false);
				}
			}


			if (interrogationPosition.x == x && interrogationPosition.y == y) {
				fill(52 + x * 4, 156 + (y * 4), 4, 4,
					 (((timeUntilNextState / 100) % 2) == 0) ? getPaletteEntry(0xFF326496) : getPaletteEntry(
							 0xFF966432), false);
				continue;
			}
		}
	}

	for (y = 0; y < kMazeSideSize; ++y) {
		for (int x = 0; x < kMazeSideSize; ++x) {
			drawRect(52 + x * 4, 156 + (y * 4), 4, 4, getPaletteEntry(0xFF000000));
		}
	}

	distance = (abs(interrogationPosition.x - interrogationTargetSpot.x) +
				abs(interrogationPosition.y - interrogationTargetSpot.y));
	blinkTime = (((timeUntilNextState / 1000) % 4) == 0);
	sideEyeTime = (((timeUntilNextState / 1000) % 2) == 0);

	if (distance <= 2 && !blinkTime) {
		drawBitmap(236, 152, eyesB, true);
	} else if (timeUntilNextState >= (10000 - kMenuItemBlinkIntervalInMs)) {
		drawBitmap(236, 152, eyesW, true);
	} else if (blinkTime) {
		drawBitmap(236, 152, eyesC, true);
	} else {
		if (distance > 8) {
			if (sideEyeTime) {
				drawBitmap(236, 152, eyesL, true);
			}

			if (!sideEyeTime) {
				drawBitmap(236, 152, eyesR, true);
			}
		} else {
			drawBitmap(236, 152, eyesW, true);
		}
	}

	drawRect(236, 152, 64, 40, getPaletteEntry(0xFF000000));


	switch (currentPresentationState) {
		case kConfirmInputBlink1:
		case kConfirmInputBlink3:
		case kConfirmInputBlink5: {
			FramebufferPixelFormat color = getPaletteEntry(0xFF777777);
			const char *text = (noMoreMoves) ? "Failed!" : "GOTCHA!";
			size_t len = strlen(text);
			fill(0, 64, XRES_FRAMEBUFFER, 8, getPaletteEntry(0xFF000000), false);
			drawTextAt(20 - (len / 2), 9, text, color);
		}
			break;

		case kConfirmInputBlink2:
		case kConfirmInputBlink4:
		case kConfirmInputBlink6: {
			FramebufferPixelFormat color = getPaletteEntry(0xFF000000);
			const char *text = (noMoreMoves) ? "Failed!" : "GOTCHA!";
			size_t len = strlen(text);
			fill(0, 64, XRES_FRAMEBUFFER, 8, getPaletteEntry(0xFF000000), false);
			drawTextAt(20 - (len / 2), 9, text, color);
		}
		case kAppearing:
		case kFadingOut:
		case kWaitingForInput:
		default:
			break;
	}
}

enum EGameMenuState Interrogation::tick(int32_t tag, long delta) {

	timeUntilNextState -= delta;

	if (timeUntilNextState <= 0) {

		switch (currentPresentationState) {
			case kAppearing:
				timeUntilNextState = UI::kMenuItemTimeToBecomeActiveInMs;
				currentPresentationState = kWaitingForInput;
				break;
			case kWaitingForInput:
				timeUntilNextState = 10000 - 1;
				/* return kMainMenu; */
				break;
			case kConfirmInputBlink1:
			case kConfirmInputBlink2:
			case kConfirmInputBlink3:
			case kConfirmInputBlink4:
			case kConfirmInputBlink5:
			case kConfirmInputBlink6:
				timeUntilNextState = kMenuItemTimeToBlinkInMs;
				currentPresentationState =
						(enum EPresentationState) ((int) currentPresentationState + 1);
				break;
			case kFadingOut:
				return nextNavigationSelection;
		}
	}

	if (currentPresentationState == kWaitingForInput) {

		/* did we reach the target? give the clue and go straight to the dossier, showing the clue. */
		if (interrogationPosition.x == interrogationTargetSpot.x
			&& interrogationPosition.y == interrogationTargetSpot.y) {

			timeUntilNextState = UI::kMenuItemTimeToBecomeActiveInMs;
			playSound(INFORMATION_ACQUIRED_SOUND);
			currentPresentationState = kConfirmInputBlink1;

			if (isPracticingInterrogation) {
				nextNavigationSelection = kMainMenu;
			} else {
				nextNavigationSelection = (enum EGameMenuState) (kReadDossier_Sofia + SpyGame::getPlayerLocation());
			}

			return kNoChange;
		}

		interrogationReveal[interrogationPosition.y][interrogationPosition.x] = true;

		/* perform checks for early bailing out if no more moves are available */

		if (noMoreMoves) {
			timeUntilNextState = UI::kMenuItemTimeToBecomeActiveInMs;
			currentPresentationState = kConfirmInputBlink1;
			if (isPracticingInterrogation) {
				nextNavigationSelection = kMainMenu;
			} else {
				nextNavigationSelection = kGameMenu;
			}
			return kNoChange;
		}

		switch (tag) {
			case kCommandBack:
				timeUntilNextState = 0;
				return kGameMenu;
			case kCommandUp:

				if (!emotionAmount[0] || !emotionAmount[2]) {
					noMoreMoves = true;
					playSound(FAILED_TO_GET_INFORMATION_SOUND);
					return kNoChange;
				}

				if (interrogationPosition.y <= 0) {
					return kNoChange;
				}

				if (interrogationMap[interrogationPosition.y - 1]
					[interrogationPosition.x]
					== 1) {
					return kNoChange;
				}
				playSound(MENU_SELECTION_CHANGE_SOUND);
				interrogationPosition.y--;

				if (!interrogationReveal[interrogationPosition.y]
				[interrogationPosition.x]
					&& emotionAmount[0] > 0 && emotionAmount[2] > 0) {

					emotionAmount[0]--;
					emotionAmount[2]--;
				}

				break;
			case kCommandDown:
				if (!emotionAmount[1] || !emotionAmount[3]) {
					noMoreMoves = true;
					playSound(FAILED_TO_GET_INFORMATION_SOUND);
					return kNoChange;
				}

				if (interrogationPosition.y >= 7) {
					return kNoChange;
				}

				if (interrogationMap[interrogationPosition.y + 1]
					[interrogationPosition.x]
					== 1) {
					return kNoChange;
				}

				playSound(MENU_SELECTION_CHANGE_SOUND);
				interrogationPosition.y++;

				if (!interrogationReveal[interrogationPosition.y]
				[interrogationPosition.x]
					&& emotionAmount[1] > 0 && emotionAmount[3] > 0) {
					emotionAmount[1]--;
					emotionAmount[3]--;
				}
				break;

			case kCommandLeft:
				if (!emotionAmount[1] || !emotionAmount[0]) {
					noMoreMoves = true;
					playSound(FAILED_TO_GET_INFORMATION_SOUND);
					return kNoChange;
				}

				if (interrogationPosition.x <= 0) {
					return kNoChange;
				}

				if (interrogationMap[interrogationPosition.y]
					[interrogationPosition.x - 1]
					== 1) {
					return kNoChange;
				}

				playSound(MENU_SELECTION_CHANGE_SOUND);
				interrogationPosition.x--;

				if (!interrogationReveal[interrogationPosition.y]
				[interrogationPosition.x]
					&& emotionAmount[0] > 0 && emotionAmount[1] > 0) {
					emotionAmount[0]--;
					emotionAmount[1]--;
				}
				break;
			case kCommandRight:
				if (!emotionAmount[2] || !emotionAmount[3]) {
					noMoreMoves = true;
					playSound(FAILED_TO_GET_INFORMATION_SOUND);
					return kNoChange;
				}

				if (interrogationPosition.x >= 7) {
					return kNoChange;
				}

				if (interrogationMap[interrogationPosition.y]
					[interrogationPosition.x + 1]
					== 1) {
					return kNoChange;
				}

				playSound(MENU_SELECTION_CHANGE_SOUND);
				interrogationPosition.x++;

				if (!interrogationReveal[interrogationPosition.y]
				[interrogationPosition.x]
					&& emotionAmount[2] > 0 && emotionAmount[2] > 0) {
					emotionAmount[2]--;
					emotionAmount[3]--;
				}
				break;

			case kCommandFire1:
				break;
			default:
				return kNoChange;
		}
	}

	return kNoChange;
}

Interrogation::~Interrogation() {
	releaseBitmap(character);
	releaseBitmap(eyesL);
	releaseBitmap(eyesR);
	releaseBitmap(eyesW);
	releaseBitmap(eyesB);
	releaseBitmap(eyesC);
}
