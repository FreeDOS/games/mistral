#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Compat.h"
#include "SpyTravel.h"
#include "FixP.h"
#include "Enums.h"
#include "Vec.h"
#include "Actor.h"
#include "LoadBitmap.h"
#include "Dungeon.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "Renderer.h"
#include "Engine.h"
#include "PackedFileReader.h"

#ifndef ANDROID
#ifdef WIN32
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
				   LPSTR lpCmdLine, int nCmdShow) {
	createWindow(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
#else

int main(int argc, char **argv) {
#endif

	int start_clock, end_clock, prev;

	puts("The Mistral Report - Invisible Affairs, 2018-2022 - by the "
		 "Brotherhood of 13h");

	srand(time(NULL));
	PackedFile::init("base.pfs");
	graphicsInit();

	enterState(kMainMenu);

	end_clock = uclock();
	prev = 0;

	start_clock = uclock();

#ifdef __EMSCRIPTEN__
	emscripten_set_main_loop(mainLoop, 0, 1);
#else
	long delta_time;
	do {
		long now = (end_clock - start_clock);
		delta_time = (now - prev) / (UCLOCKS_PER_SEC / 1000);
		prev = now;

		/* protect against machines too fast for their own good. */
		if (delta_time < 50) {
			delta_time = 50;
		}
	} while (menuTick(delta_time));
#endif
	delete currentMenuState;
	graphicsShutdown();

	return 0;
}
#endif
