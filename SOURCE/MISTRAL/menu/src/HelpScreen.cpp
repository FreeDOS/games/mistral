#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "Compat.h"
#include "Enums.h"
#include "FixP.h"
#include "Utils.h"
#include "Vec.h"
#include "Dungeon.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "Renderer.h"
#include "Engine.h"
#include "Actor.h"
#include "PackedFileReader.h"
#include "Rasterizer.h"
#include "Tesselation.h"
#include "LoadBitmap.h"
#include "UI.h"
#include "SoundSystem.h"

#include "HelpScreen.h"

const static char *HelpScreen_options[1] = {"Back"};

const static enum EGameMenuState HelpScreen_nextStateNavigation[1] = {
		kMainMenu,
};

HelpScreen::HelpScreen(int32_t tag) : MenuState(tag, kMainMenu) {

	PackedFile::StaticBuffer fileInput = PackedFile::loadFromPath("Help.txt");
	memset(&textBuffer[0], ' ', (XRES_FRAMEBUFFER / 8) * 25);
	mainText = &textBuffer[0];
	memset(&textBuffer[0], 0, ((XRES_FRAMEBUFFER / 8) * 25));
	memcpy(&textBuffer[0], fileInput.data, fileInput.size);
	optionsCount = sizeof(HelpScreen_nextStateNavigation)/sizeof(enum EGameMenuState);
}

void HelpScreen::initialPaint(void) {}

void HelpScreen::repaint(void) {

	int lines;
	size_t len = UI::computeBoxWidth("Help", HelpScreen_options, 1);
	int optionsHeight = 8 * (optionsCount);

	lines = Utils::countLines(mainText);

	if (currentBackgroundBitmap != NULL) {
		drawRepeatBitmap(0, 0, XRES_FRAMEBUFFER, YRES_FRAMEBUFFER, currentBackgroundBitmap);
	}

	if (mainText != NULL) {
		UI::drawTextWindow(1, 1, (XRES_FRAMEBUFFER / 8), lines + 3, "Help", mainText, currentPresentationState, UI::kMenuItemTimeToBecomeActiveInMs - timeUntilNextState);
	}

	UI::drawTextOptionsWindow(
			(XRES_FRAMEBUFFER / 8) - len - 3,
			((YRES_FRAMEBUFFER - optionsHeight) / 8) - 2,
			"Help",
			HelpScreen_options,
			optionsCount,
			cursorPosition,
			currentPresentationState,
			UI::kMenuItemTimeToBecomeActiveInMs - timeUntilNextState);
}

enum EGameMenuState HelpScreen::tick(int32_t tag, long delta) {

	timeUntilNextState -= delta;

	if (timeUntilNextState <= 0) {
		enum EGameMenuState navigation = blinkCursor();
		if (navigation != kNoChange) {
			return navigation;
		}
	}

	if (currentPresentationState == kWaitingForInput) {
		enum EGameMenuState navigation = handleNavigation(tag, &HelpScreen_nextStateNavigation[0]);
		if (navigation != kNoChange) {
			return navigation;
		}
	}

	return kNoChange;
}

HelpScreen::~HelpScreen() {}
