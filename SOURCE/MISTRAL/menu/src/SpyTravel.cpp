#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "Compat.h"
#include "Enums.h"
#include "SpyTravel.h"
#include "PackedFileReader.h"

namespace SpyGame {
	struct Agent {
		const char *name;
		int location;
		bool rescued;
	};

	const char *locationName[8];
	struct Agent agents[8];
	struct Agent player;

	const static char *spiesClues[8] = {
			"Look for a guard that patrols the north sector, near the rocks.",
			"There's a computer room in the basement to the east.",
			"The important guard you are looking for will be patrolling the rooftop.",
			"The guards on the southwest zone take\nturns holding the info card.",
			"There's a good chance the key holder\nwill be on the aft control tower.",
			"There's a room overlooking the river.\nThe soldier holding the card should be\nthere.",
			"We have very little intel on Charleroi.\nTry to take advantage of the available\ncover area.",
			"", //the player will never see this.
	};

	void setAsRescued(bool c) {
		agents[c].rescued = true;
	}

	bool hasBeenRescued(int c) {
		return agents[c].rescued;
	}

	void init(bool isStoryMode) {

		memset(&agents[0], 0, sizeof(struct Agent) * 8);

		locationName[0] = "Porto";
		locationName[1] = "Lisbon";
		locationName[2] = "Madrid";
		locationName[3] = "Barcelona";
		locationName[4] = "Frankfurt";
		locationName[5] = "Hamburg";
		locationName[6] = "Luxembourg";
		locationName[7] = "Brussels";

		agents[0].name = "Sofia";
		agents[0].location = 0;

		agents[1].name = "Ricardo";
		agents[1].location = 1;

		agents[2].name = "Lola";
		agents[2].location = 2;

		agents[3].name = "Pau";
		agents[3].location = 3;

		agents[4].name = "Lina";
		agents[4].location = 4;

		agents[5].name = "Elias";
		agents[5].location = 5;

		agents[6].name = "Carmen";
		agents[6].location = 6;

		agents[7].name = "Jean";
		agents[7].location = 7;

		player.location = 0;
		player.rescued = true;
	}

	size_t getDossierText(int c, char *buffer, size_t size) {
		char filename[16];
		char suspectName[256];

		getSuspectName(c, suspectName, 256);
		sprintf(&filename[0], "%s.txt", suspectName);
		PackedFile::StaticBuffer fileInput = PackedFile::loadFromPath(filename);

		size_t bufferUsage = 0;
		bufferUsage += sprintf(buffer + bufferUsage,
							   "Bio:\n%s\n", fileInput.data);

		if (hasBeenRescued(c)) {
			bufferUsage += sprintf(buffer + bufferUsage, "\nIntel:\n");
			bufferUsage += getSuspectClue(c, buffer + bufferUsage, size - bufferUsage);
		} else {
			bufferUsage += sprintf(buffer + bufferUsage, "\nAgent is Missing In Action\n");
		}

		return bufferUsage;
	}

	int getPlayerLocation() {
		return player.location;
	}

	size_t getLocationName(int c, char *buffer, size_t size) {

		size_t bufferUsage = 0;

		bufferUsage += sprintf(buffer + bufferUsage, "%s",
							   locationName[c]);

		return bufferUsage;
	}

	size_t getSuspectName(int c, char *buffer, size_t size) {
		return sprintf(buffer, "%s", agents[c].name);
	}

	size_t getSuspectClue(int c, char *buffer, size_t size) {

		size_t bufferUsage = 0;

		bufferUsage += sprintf(buffer + bufferUsage, "%s", spiesClues[c]);

		return bufferUsage;
	}

	size_t getDisplayStatusText(char *buffer, size_t size) {

		size_t bufferUsage = 0;

		bufferUsage +=
				sprintf(buffer + bufferUsage, "MIA:\n");
		assert(bufferUsage < size);

		for (int c = 0; c < 8; ++c) {
			struct Agent *agent = &agents[c];

			if (!agent->rescued) {
				bufferUsage += sprintf(buffer + bufferUsage,
									   "-%s: %s\n", agent->name, locationName[c]);
				assert(bufferUsage < size);
			}
		}

		return bufferUsage;
	}

	void goTo(int newLocation) {
		player.location = newLocation;
	}
}
