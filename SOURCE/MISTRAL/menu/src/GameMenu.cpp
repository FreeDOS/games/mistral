#include <stdio.h>
#include <string.h>

#include "Compat.h"
#include "Enums.h"
#include "FixP.h"
#include "Utils.h"
#include "LoadBitmap.h"
#include "Vec.h"
#include "Dungeon.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "Renderer.h"
#include "Engine.h"
#include "SpyTravel.h"
#include "Actor.h"
#include "Rasterizer.h"
#include "Tesselation.h"
#include "UI.h"
#include "Globals.h"
#include "SoundSystem.h"

#include "GameMenu.h"

const char **GameMenu_options;
const enum EGameMenuState *GameMenu_nextStateNavigation;
const char *GameMenu_StateTitle;
enum EGameMenuState GameMenu_substate;
bool drawFilter = false;

const static char *GameMenu_Main_options[3] = {
		"Read Dossiers",
		"Investigate",
		"End game"
};

const static enum EGameMenuState GameMenu_Main_nextStateNavigation[3] = {
		kDossiersMenu,
		kInvestigateMenu,
		kEndGame
};

const static char *GameMenu_Dossier_options[9] = {"Sofia", "Ricardo", "Lola",
										   "Pau", "Lina", "Elias",
										   "Carmen", "Jean", "Back"};

const static enum EGameMenuState GameMenu_Dossier_nextStateNavigation[9] = {
		kReadDossier_Sofia, kReadDossier_Ricardo, kReadDossier_Lola,
		kReadDossier_Pau, kReadDossier_Lina, kReadDossier_Elias,
		kReadDossier_Carmen, kReadDossier_Jean, kGameMenu};

const static enum EGameMenuState GameMenu_EndGame_nextStateNavigation[2] = {kGameMenu, kMainMenu};

const static char *GameMenu_EndGame_options[2] = {"No", "Yes"};

const static char *GameMenu_ReadDossier_options[1] = {"Back"};

const static enum EGameMenuState GameMenu_ReadDossier_nextStateNavigation[1] = {
		kDossiersMenu,
};

const static char *GameMenu_Story_options[1] = {"Continue"};

const static enum EGameMenuState GameMenu_Story_nextStateNavigation[1] = {kMainMenu};


GameMenu::GameMenu(int32_t tag) : MenuState(tag, kMainMenu) {
	featuredBitmap = NULL;
	GameMenu_StateTitle = NULL;
	GameMenu_substate = (enum EGameMenuState) tag;
	memset(&textBuffer[0], ' ', (XRES_FRAMEBUFFER / 8) * 25);
	drawFilter = false;

	switch (tag) {

		case kEndGame:
			timeUntilNextState = 0;
			drawFilter = true;
			GameMenu_StateTitle = "End session?";
			SpyGame::getDisplayStatusText(&textBuffer[0], (XRES_FRAMEBUFFER / 8) * 10);
			mainText = &textBuffer[0];
			GameMenu_options = &GameMenu_EndGame_options[0];
			optionsCount = sizeof(GameMenu_EndGame_nextStateNavigation) / sizeof(enum EGameMenuState);
			GameMenu_nextStateNavigation = &GameMenu_EndGame_nextStateNavigation[0];
			break;
		case kPlayGame:
		case kGameMenu:
			GameMenu_StateTitle = "Investigation";
			SpyGame::getDisplayStatusText(&textBuffer[0], (XRES_FRAMEBUFFER / 8) * 10);
			mainText = &textBuffer[0];
			GameMenu_options = &GameMenu_Main_options[0];
			optionsCount = sizeof(GameMenu_Main_nextStateNavigation) / sizeof(enum EGameMenuState);
			GameMenu_nextStateNavigation = &GameMenu_Main_nextStateNavigation[0];
			break;

		case kDossiersMenu:
			GameMenu_StateTitle = "Dossiers";
			SpyGame::getDisplayStatusText(&textBuffer[0], (XRES_FRAMEBUFFER / 8) * 10);
			mainText = &textBuffer[0];
			GameMenu_options = &GameMenu_Dossier_options[0];
			optionsCount = sizeof(GameMenu_Dossier_nextStateNavigation) / sizeof(enum EGameMenuState);
			mainText = NULL;
			GameMenu_nextStateNavigation = &GameMenu_Dossier_nextStateNavigation[0];
			break;

		case kVictory:
			sprintf(textBuffer,
					"Victory!\n"
					"You rescued all the\n"
					"agents that were \n"
					"captive and now, \n"
					"it's time to enjoy \n"
					"some downtime at\n"
					"your favorite beach.");
			mainText = &textBuffer[0];

			GameMenu_StateTitle = "Victory";
			GameMenu_options = &GameMenu_Story_options[0];
			optionsCount = sizeof(GameMenu_Story_nextStateNavigation) / sizeof(enum EGameMenuState);
			GameMenu_nextStateNavigation = &GameMenu_Story_nextStateNavigation[0];
			break;

		case kGameOver:
			sprintf(textBuffer, "You're dead!\n\n\n\n\n\n");
			mainText = &textBuffer[0];
			GameMenu_StateTitle = "Game Over";
			GameMenu_options = &GameMenu_Story_options[0];
			optionsCount = sizeof(GameMenu_Story_nextStateNavigation) / sizeof(enum EGameMenuState);
			GameMenu_nextStateNavigation = &GameMenu_Story_nextStateNavigation[0];
			break;

		case kPrologue:
			sprintf(textBuffer, "Out of prison");
			mainText = &textBuffer[0];
			GameMenu_StateTitle = "Everything's changed...but still feels the same.";
			GameMenu_options = &GameMenu_Story_options[0];
			optionsCount = sizeof(GameMenu_Story_nextStateNavigation) / sizeof(enum EGameMenuState);
			GameMenu_nextStateNavigation = &GameMenu_Story_nextStateNavigation[0];
			break;

		case kReadDossier_Sofia:
		case kReadDossier_Ricardo:
		case kReadDossier_Lola:
		case kReadDossier_Pau:
		case kReadDossier_Lina:
		case kReadDossier_Elias:
		case kReadDossier_Carmen:
		case kReadDossier_Jean: {
			GameMenu_StateTitle = "Dossier";
			mainText = &textBuffer[0];
			SpyGame::getDossierText(tag - kReadDossier_Sofia, &textBuffer[0], (XRES_FRAMEBUFFER / 8) * 25);

			GameMenu_options = &GameMenu_ReadDossier_options[0];
			optionsCount = sizeof(GameMenu_ReadDossier_nextStateNavigation) / sizeof(enum EGameMenuState);
			GameMenu_nextStateNavigation =
					&GameMenu_ReadDossier_nextStateNavigation[0];
		}
	}

	biggestOption = UI::computeBoxWidth(GameMenu_StateTitle, GameMenu_options, optionsCount);
}

void GameMenu::initialPaint(void) {
	featuredBitmap = NULL;
}

void GameMenu::repaint(void) {
	char turnBuffer[64];
	char locationBuffer[64];
	int lines = 0;

	if (mainText != NULL) {
		lines = Utils::countLines(mainText);
	}

	if (currentBackgroundBitmap != NULL) {
		drawRepeatBitmap(0, 0, XRES_FRAMEBUFFER, YRES_FRAMEBUFFER, currentBackgroundBitmap);
	}

	char photoBuffer[16];
	const char *name = NULL;
	int wrappedCursor = (cursorPosition < 8) ? cursorPosition : SpyGame::getPlayerLocation();

	int optionsHeight = 8 * (optionsCount);

	if (GameMenu_substate == kGameOver) {
		featuredBitmap = loadBitmap("gameover.img");
		name = "Game Over!";
	}

	if (GameMenu_substate == kVictory) {
		featuredBitmap = loadBitmap("win.img");
		name = "Victory!";
	}

	if (GameMenu_substate == kPlayGame || GameMenu_substate == kGameMenu || GameMenu_substate == kEndGame) {
		int location = SpyGame::getPlayerLocation();
		if (featuredBitmap == NULL && location < 8) {
			sprintf(photoBuffer, "location%d.img", location);
			featuredBitmap = loadBitmap(photoBuffer);
		}

		SpyGame::getLocationName(SpyGame::getPlayerLocation(), &locationBuffer[0], 64);
		name = &locationBuffer[0];
	}

	if (GameMenu_substate == kDossiersMenu) {

		name = GameMenu_Dossier_options[wrappedCursor];

		if (featuredBitmap == NULL) {
			sprintf(photoBuffer, "%s.img", name);
			featuredBitmap = loadBitmap(photoBuffer);
		}
	}

	if (mainText != NULL) {

		int textWidth;
		int posX;
		int posY;

		switch (GameMenu_substate) {
			case kGameMenu:
			case kPlayGame:
			case kGameOver:
			case kVictory:
			case kEndGame:
				textWidth = 168;
				posX = 200 - 56;
				posY = 8;
				break;

			case kReadDossier_Sofia:
			case kReadDossier_Ricardo:
			case kReadDossier_Lola:
			case kReadDossier_Pau:
			case kReadDossier_Lina:
			case kReadDossier_Elias:
			case kReadDossier_Carmen:
			case kReadDossier_Jean:
				textWidth = XRES_FRAMEBUFFER;
				posX = 0;
				posY = 0;
				break;
			default:
				textWidth = 176;
				posX = 8;
				posY = 8;
		}

		switch (GameMenu_substate) {
			case kReadDossier_Sofia:
			case kReadDossier_Ricardo:
			case kReadDossier_Lola:
			case kReadDossier_Pau:
			case kReadDossier_Lina:
			case kReadDossier_Elias:
			case kReadDossier_Carmen:
			case kReadDossier_Jean: {
				SpyGame::getLocationName(GameMenu_substate - kReadDossier_Sofia, locationBuffer, 64);
				sprintf(turnBuffer, "%s", locationBuffer);
			}
				break;
			default:
				SpyGame::getLocationName(SpyGame::getPlayerLocation(), locationBuffer, 64);
				sprintf(turnBuffer, "Mission %d", SpyGame::getPlayerLocation() + 1);
		}

		UI::drawTextWindow((posX / 8) + 1, (posY / 8) + 1, textWidth / 8, lines + 4, turnBuffer, mainText, currentPresentationState,  UI::kMenuItemTimeToBecomeActiveInMs - timeUntilNextState);
	}

	if (featuredBitmap != NULL && name != NULL) {
		UI::drawImageWindow(2, 2, featuredBitmap->width / 8, (featuredBitmap->height / 8) + 1, name, featuredBitmap, currentPresentationState, UI::kMenuItemTimeToBecomeActiveInMs - timeUntilNextState);
	}

	if (drawFilter) {
		fill(0, 0, XRES_FRAMEBUFFER, YRES_FRAMEBUFFER, getPaletteEntry(0xFF000000), true);
	}

	UI::drawTextOptionsWindow(
			(XRES_FRAMEBUFFER / 8) - (int) biggestOption - 3,
			25 - (optionsHeight / 8) - 3,
			GameMenu_StateTitle,
			GameMenu_options,
			optionsCount,
			cursorPosition,
			currentPresentationState,
			UI::kMenuItemTimeToBecomeActiveInMs - timeUntilNextState);
}

enum EGameMenuState GameMenu::tick(int32_t tag, long delta) {

	timeUntilNextState -= delta;

	if (timeUntilNextState <= 0) {
		enum EGameMenuState navigation = blinkCursor();
		if (navigation != kNoChange) {
			return navigation;
		}
	}

	if (currentPresentationState == kWaitingForInput) {

		switch (tag) {
			case kCommandBack:
				return stateUp;
			case kCommandUp:
				playSound(MENU_SELECTION_CHANGE_SOUND);
				cursorPosition = (cursorPosition - 1);

				if (cursorPosition >= optionsCount) {
					cursorPosition = optionsCount - 1;
				}

				if (GameMenu_substate == kDossiersMenu) {
					featuredBitmap = NULL;
				}
				break;
			case kCommandDown:
				playSound(MENU_SELECTION_CHANGE_SOUND);
				cursorPosition =
						(uint8_t) ((cursorPosition + 1) % optionsCount);

				if (GameMenu_substate == kDossiersMenu) {
					featuredBitmap = NULL;
				}
				break;
			case kCommandFire1:
			case kCommandFire2:
			case kCommandFire3:
				if (GameMenu_substate == kDossiersMenu) {
					featuredBitmap = NULL;
				}
				nextNavigationSelection = GameMenu_nextStateNavigation[cursorPosition];
				currentPresentationState = kConfirmInputBlink1;
				break;
		}
	}

	return kNoChange;
}

GameMenu::~GameMenu() {
	if (featuredBitmap != NULL ) {
		releaseBitmap(featuredBitmap);
		featuredBitmap = NULL;
	}
}
