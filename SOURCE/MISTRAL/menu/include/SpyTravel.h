#ifndef SPYTRAVEL_H
#define SPYTRAVEL_H

namespace SpyGame {
	size_t getDisplayStatusText(char *buffer, size_t size);

	size_t getDossierText(int suspect, char *buffer, size_t size);

	int getPlayerLocation();

	size_t getLocationName(int c, char *buffer, size_t size);

	bool hasBeenRescued(int c);

	size_t getSuspectName(int c, char *buffer, size_t size);

	size_t getSuspectClue(int c, char *buffer, size_t size);

	void init(bool isStoryMode);

	void setAsRescued(bool c);

	void goTo(int newLocation);
}
#endif
