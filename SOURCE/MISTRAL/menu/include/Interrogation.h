//
// Created by Daniel Monteiro on 8/24/22.
//

#ifndef INTERROGATION_H
#define INTERROGATION_H

class Interrogation : public MenuState {
	int emotionAmount[4];
	struct Bitmap *emotions[4];
	Vec::Vec2i interrogationPosition;
	Vec::Vec2i interrogationTargetSpot;
	struct Bitmap *character;
	struct Bitmap *eyesL;
	struct Bitmap *eyesR;
	struct Bitmap *eyesW;
	struct Bitmap *eyesB;
	struct Bitmap *eyesC;
	bool isPracticingInterrogation;
	char suspectName[256];
	bool noMoreMoves;

	void initMaze(const int indexZeroBased);
public:
	Interrogation(int32_t tag);
	virtual ~Interrogation();
	virtual void initialPaint();
	virtual void repaint();
	virtual enum EGameMenuState tick(int32_t tag, long interval);
};

#endif // INTERROGATION_H
