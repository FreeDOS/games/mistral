//
// Created by Daniel Monteiro on 8/24/22.
//

#ifndef HELPSCREEN_H
#define HELPSCREEN_H

class HelpScreen : public MenuState {
public:
	HelpScreen (int32_t tag);
	virtual ~HelpScreen();
	virtual void initialPaint();
	virtual void repaint();
	virtual enum EGameMenuState tick(int32_t tag, long interval);
};

#endif // HELPSCREEN_H
