//
// Created by Daniel Monteiro on 8/24/22.
//

#ifndef CREDITSSCREEN_H
#define CREDITSSCREEN_H

class CreditsScreen : public MenuState {
	struct Bitmap *monty;
	struct Bitmap *belle;
	struct Bitmap *stdmatt;
public:
	CreditsScreen(int32_t tag);
	virtual ~CreditsScreen();
	virtual void initialPaint();
	virtual void repaint();
	virtual enum EGameMenuState tick(int32_t tag, long interval);
};

#endif // CREDITSSCREEN_H
