//
// Created by Daniel Monteiro on 8/24/22.
//

#ifndef OPAL_STANDOFF_CRAWLER_H
#define OPAL_STANDOFF_CRAWLER_H

class PlayMissionState : public MenuState {
public:
	PlayMissionState(int32_t tag);
	virtual ~PlayMissionState();
	virtual void initialPaint();
	virtual void repaint();
	virtual enum EGameMenuState tick(int32_t tag, long interval);
};

#endif //OPAL_STANDOFF_CRAWLER_H
