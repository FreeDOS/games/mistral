//
// Created by Daniel Monteiro on 8/24/22.
//

#ifndef MAINMENU_H
#define MAINMENU_H

class MainMenu : public MenuState {
private:
	struct Bitmap *logoBitmap;
public:
	MainMenu(int32_t tag);
	virtual ~MainMenu();
	virtual void initialPaint();
	virtual void repaint();
	virtual enum EGameMenuState tick(int32_t tag, long interval);
};

#endif // MAINMENU_H
