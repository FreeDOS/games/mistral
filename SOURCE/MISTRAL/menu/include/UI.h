/*
* Created by Daniel Monteiro on 2019-08-02.
*/

#ifndef UI_H
#define UI_H

namespace UI {
	const static int kMenuItemTimeToBecomeActiveInMs = 256;

	void drawWindow(const int x, const int y, const int dx, const int dy, const char *title);

	void drawTextWindow(const int x, const int y, const int dx, const int dy, const char *title, const char *content, const EPresentationState currentPresentationState, long progression);

	void
	drawImageWindow(const int x, const int y, const int dx, const int dy, const char *title, struct Bitmap *content, enum EPresentationState presentationState, long progression);

	void drawTextOptionsWindow(const int x, const int y, const char* title, const char** options, const int optionsCount, const int cursorPosition, const EPresentationState currentPresentationState, long progression);

	size_t computeBoxWidth( const char* title, const char** options, const int optionsCount );
}
#endif /*UI_H*/
