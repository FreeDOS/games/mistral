//
// Created by Daniel Monteiro on 8/24/22.
//

#ifndef GAMEMENU_H
#define GAMEMENU_H

class GameMenu : public MenuState {
	struct Bitmap *featuredBitmap;
public:
	GameMenu(int32_t tag);
	virtual ~GameMenu();
	virtual void initialPaint();
	virtual void repaint();
	virtual enum EGameMenuState tick(int32_t tag, long interval);
};

#endif // GAMEMENU_H
