#ifndef ENGINE_H
#define ENGINE_H

#if !defined(ANDROID) && !defined(__EMSCRIPTEN__)
#define MAIN_MENU_HAS_QUIT_OPTION
#endif

enum EPresentationState {
	kAppearing,
	kWaitingForInput,
	kConfirmInputBlink1,
	kConfirmInputBlink2,
	kConfirmInputBlink3,
	kConfirmInputBlink4,
	kConfirmInputBlink5,
	kConfirmInputBlink6,
	kFadingOut
};

enum EGameMenuState {
	kNoChange = -1,
	kMainMenu,
	kPlayGame,
	kPlayStory,
	kGameMenu,
	kHelp,
	kCredits,
	kQuit,
	kDossiersMenu,
	kInvestigateMenu,
	kStatusMenu,
	kEndGame,
	kReadDossier_Sofia,
	kReadDossier_Ricardo,
	kReadDossier_Lola,
	kReadDossier_Pau,
	kReadDossier_Lina,
	kReadDossier_Elias,
	kReadDossier_Carmen,
	kReadDossier_Jean,
	kVictory,
	kGameOver,
	kPrologue,
	kInterrogate_Sofia,
	kInterrogate_Ricardo,
	kInterrogate_Lola,
	kInterrogate_Pau,
	kInterrogate_Lina,
	kInterrogate_Elias,
	kInterrogate_Carmen,
	kInterrogate_Jean
};

const static int kMenuItemBlinkIntervalInMs = 250;
/* 84ms * 6 blinks == ~500ms */
const static int kMenuItemTimeToBlinkInMs = 84;
const static uint32_t kNonExpiringPresentationState = 0xFFFF;
const static int kDefaultPresentationStateInterval = 500;
#define TEXT_BUFFER_SIZE (XRES_FRAMEBUFFER / 8) * 25

extern enum EGameMenuState currentGameMenuState;

extern const char *mainText;
extern char textBuffer[TEXT_BUFFER_SIZE];

bool menuTick(long ms);

class MenuState {
protected:
	uint8_t cursorPosition;
	int optionsCount;
	long timeUntilNextState;
	struct Bitmap *currentBackgroundBitmap;
	size_t biggestOption;
	enum EPresentationState currentPresentationState;
	enum EGameMenuState nextNavigationSelection;
	enum EGameMenuState stateUp;
	enum EGameMenuState blinkCursor();
	enum EGameMenuState handleNavigation(int32_t cmd, const enum EGameMenuState *routes);
public:
	MenuState(int32_t tag, enum EGameMenuState up);
	virtual ~MenuState();
	virtual void initialPaint() = 0;
	virtual void repaint() = 0;
	virtual enum EGameMenuState tick(int32_t tag, long interval) = 0;
};

void enterState(enum EGameMenuState State);

extern MenuState *currentMenuState;

#endif
