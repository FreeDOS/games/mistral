Elias is a very obstuse character,
known to take his orders, no matter
the cost of doing so.

A grizzly old agent that could
blend in any crowd. He's known to
also change appearance constantly.