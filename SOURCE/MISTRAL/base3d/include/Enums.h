#ifndef ENUMS_H
#define ENUMS_H

enum EActorsSnapshotElement {
	kNobody, kEnemy0, kEnemy1, kEnemyBack, kPlayer, kEnemyFiring
};

enum EItemsSnapshotElement {
	kNoItem,
	kDeadEnemy,
	kHostage,
	kClue
};

enum EDirection {
	kNorth, kEast, kSouth, kWest
};

enum CrawlerState {
	kCrawlerGameOver = -1,
	kCrawlerQuit = 0,
	kCrawlerGameInProgress = 1,
	kCrawlerClueAcquired = 2
};

enum ECommand {
	kCommandNone,
	kCommandUp,
	kCommandRight,
	kCommandDown,
	kCommandLeft,
	kCommandFire1,
	kCommandFire2,
	kCommandBack,
	kCommandQuit,
	kCommandStrafeLeft,
	kCommandStrafeRight,
	kCommandFire3,
	kCommandFire4,
	kCommandPassTurn
};

enum ESoundDriver {
	kNoSound, kPcSpeaker, kOpl2Lpt, kAdlib, kCovox
};

enum ESounds {
	MENU_SELECTION_CHANGE_SOUND,
	STATE_CHANGE_SOUND,
	INFORMATION_ACQUIRED_SOUND,
	FAILED_TO_GET_INFORMATION_SOUND,
	PLAYER_GOT_DETECTED_SOUND,
	PLAYER_FIRING_GUN,
	ENEMY_FIRING_GUN,
	PLAYER_GET_HURT_SOUND
};

#endif
