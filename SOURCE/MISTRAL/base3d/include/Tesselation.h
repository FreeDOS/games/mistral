#ifndef TESSELATION_H
#define TESSELATION_H

#define MASK_LEFT 1
#define MASK_FRONT 2
#define MASK_RIGHT 4
#define MASK_BEHIND 8
#define MASK_FORCE_LEFT 16
#define MASK_FORCE_RIGHT 32
#define NATIVE_TEXTURE_SIZE 32

#define TEXTURE_PIXEL_TO_FB_PIXEL(pixel) (pixel)

#ifndef TRUE_COLOURS_FRAMEBUFFER
typedef uint8_t TexturePixelFormat;
#else
typedef uint32_t TexturePixelFormat;
#endif

const static int kMaxTextures = 64;
const static uint8_t kNoTexture = 0xFF;
const static uint8_t kInvalidElement = 0xFF;

struct Projection {
	Vec::Vec3 first;
	Vec::Vec2 second;
};

struct Texture {
	TexturePixelFormat rotations[4][32 * 32];
	TexturePixelFormat rowMajor[32 * 32];
	uint32_t uploadId;
};

typedef uint8_t UVCoord;

extern bool shouldDrawLights;
extern bool useDither;

//test carefully!
extern int distanceForPenumbra;
extern int distanceForDarkness;

extern FixP::type playerHeight;
extern FixP::type walkingBias;

extern FixP::type playerHeightChangeRate;
extern FixP::type playerHeightTarget;
extern Vec::Vec3 cameraOffset;
extern Vec::Vec2i cameraPosition;
extern enum EDirection cameraDirection;

extern struct Texture textures[kMaxTextures];
extern int usedTexture;

extern struct Projection projectionVertices[4];

void projectAllVertices(const uint32_t count);

void drawFloorAt(const Vec::Vec3 center,
				 const struct Texture *texture, enum EDirection cameraDirection);

void drawRampAt(const Vec::Vec3 p0, const Vec::Vec3 p1,
				const struct Texture *texture, enum EDirection cameraDirection, bool flipTexture);

void drawCeilingAt(const Vec::Vec3 center,
				   const struct Texture *texture, enum EDirection cameraDirection);

void drawLeftNear(const Vec::Vec3 center,
				  const FixP::type scale,
				  const struct Texture *texture,
				  const uint8_t mask,
				  const bool repeatedTexture);

void drawRightNear(const Vec::Vec3 center,
				   const FixP::type scale,
				   const struct Texture *texture,
				   const uint8_t mask,
				   const bool repeatedTexture);

void drawCornerAt(const Vec::Vec3 center,
				  const FixP::type scale,
				  const struct Texture *texture,
				  const uint8_t mask,
				  const bool enableAlpha,
				  const bool repeatTexture);

void drawColumnAt(const Vec::Vec3 center,
				  const FixP::type scale,
				  const struct Texture *texture,
				  const uint8_t mask,
				  const bool enableAlpha,
				  const bool repeatedTexture);

void drawBillboardAt(const Vec::Vec3 center,
					 struct Bitmap *bitmap,
					 const FixP::type scale,
					 const int size);


void fillTriangle(int *coords, FramebufferPixelFormat colour);

void drawTexturedTriangle(int *coords, UVCoord *uvCoords, struct Texture *texture);

void drawWall(FixP::type x0,
			  FixP::type x1,
			  FixP::type x0y0,
			  FixP::type x0y1,
			  FixP::type x1y0,
			  FixP::type x1y1,
			  const TexturePixelFormat *texture,
			  const FixP::type textureScaleY,
			  const int z);

void maskFloor(FixP::type y0, FixP::type y1, FixP::type x0y0, FixP::type x1y0, FixP::type x0y1, FixP::type x1y1);

void drawFloor(FixP::type y0,
			   FixP::type y1,
			   FixP::type x0y0,
			   FixP::type x1y0,
			   FixP::type x0y1,
			   FixP::type x1y1,
			   int z,
			   const TexturePixelFormat *texture);

void drawFrontWall(FixP::type x0,
				   FixP::type y0,
				   FixP::type x1,
				   FixP::type y1,
				   const TexturePixelFormat *texture,
				   const FixP::type textureScaleY,
				   const int z,
				   const bool enableAlpha,
				   const int size);

void drawMask(const FixP::type x0,
			  const FixP::type y0,
			  const FixP::type x1,
			  const FixP::type y1);

void maskWall(
		FixP::type x0,
		FixP::type x1,
		FixP::type x0y0,
		FixP::type x0y1,
		FixP::type x1y0,
		FixP::type x1y1);

void maskFloor(
		FixP::type y0,
		FixP::type y1,
		FixP::type x0y0,
		FixP::type x1y0,
		FixP::type x0y1,
		FixP::type x1y1
);

void clearTextures(void);

struct Texture *makeTextureFrom(const char *filename);

#endif // TESSELATION_H
