#ifndef TILE3DPROPERTIES_H
#define TILE3DPROPERTIES_H

typedef uint8_t TextureIndex;

enum GeometryType {
	kNoGeometry,
	kCube,
	kLeftNearWall,
	kRightNearWall,
	kFloor,
	kRampNorth,
	kRampEast,
	kRampSouth,
	kRampWest,
	kWallNorth,
	kWallWest,
	kWallCorner
};

struct CTile3DProperties {
	bool mNeedsAlphaTest;
	bool mBlockVisibility;
	bool mBlockMovement;
	bool mBlockEnemySight;
	bool mRepeatMainTexture;
	TextureIndex mCeilingTextureIndex;
	TextureIndex mFloorTextureIndex;
	TextureIndex mMainWallTextureIndex;
	TextureIndex mCeilingRepeatedTextureIndex;
	TextureIndex mFloorRepeatedTextureIndex;
	enum GeometryType mGeometryType;
	uint8_t mCeilingRepetitions;
	uint8_t mFloorRepetitions;
	FixP::type mCeilingHeight;
	FixP::type mFloorHeight;
};

void loadPropertyList(const char *propertyFile, MapWithCharKey<CTile3DProperties> *map);

#endif
