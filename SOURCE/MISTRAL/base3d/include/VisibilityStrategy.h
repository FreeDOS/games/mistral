#ifndef VISIBILITY_H
#define VISIBILITY_H

namespace Visibility {
	enum EStatus {
		kInvisible, kVisible
	};

	Vec::Vec2i transform(const enum EDirection from,
						 const Vec::Vec2i currentPos);

	void castVisibility(const enum EDirection from,
						enum EStatus *visMap,
						const Vec::Vec2i pos,
						Vec::Vec2i *distances,
						const bool cleanPrevious,
						MapWithCharKey<bool> *occluderTiles);
}
#endif
