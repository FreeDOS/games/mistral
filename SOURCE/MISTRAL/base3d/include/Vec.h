#ifndef VEC_H
#define VEC_H
namespace Vec {
	struct Vec3 {
		FixP::type mX;
		FixP::type mY;
		FixP::type mZ;
	};

	struct Vec2i {
		int32_t x;
		int32_t y;
	};

	struct Vec2 {
		FixP::type mX;
		FixP::type mY;
	};

	enum EDirection leftOf(const enum EDirection d);

	enum EDirection rightOf(const enum EDirection d);

	Vec::Vec2i mapOffsetForDirection(const enum EDirection direction);

	void init(Vec2i *vec, int8_t x, int8_t y);

	void init(Vec3 *vec, FixP::type x, FixP::type y, FixP::type z);

	void addTo(Vec3 *to, FixP::type x, FixP::type y, FixP::type z);

	static const int kDirectionInvalid = -1;
}
#endif
