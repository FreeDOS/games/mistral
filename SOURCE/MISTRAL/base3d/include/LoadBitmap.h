#ifndef BITMAP_H
#define BITMAP_H

struct Bitmap *loadBitmap(const char *filename);

void releaseBitmap(struct Bitmap *ptr);

#endif
