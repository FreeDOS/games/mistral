//
// Created by Daniel Monteiro on 4/16/22.
//

#ifndef COMPAT_H
#define COMPAT_H

#ifdef __EMSCRIPTEN__
#include <emscripten/html5.h>
#include <emscripten/emscripten.h>

void mainLoop();

#endif

#ifdef WIN32
#ifndef UWP
#include <windows.h>
#include "Win32Int.h"
#include "resource.h"

void createWindow(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow);
extern long uclock();
const long UCLOCKS_PER_SEC = 1000;
#endif
#else

#include <stdint.h>

#ifndef __DJGPP__
const long UCLOCKS_PER_SEC = 1000;

extern long timeEllapsed;

long uclock();

#endif

#define min(v1, v2) (( (v1) < (v2) ) ? (v1) : (v2) )
#define max(v1, v2) (( (v1) > (v2) ) ? (v1) : (v2) )

#endif

#endif //COMPAT_H
