#ifndef MAPWITHCHARKEY_H
#define MAPWITHCHARKEY_H

template<typename T>
class MapWithCharKey {
public:
	T mMap[256];
	bool mPresent[256];

	const T getFromMap(const uint8_t key) {
		return mMap[key];
	}

	void setInMap(const uint8_t key, const T value) {
		mMap[key] = value;
		mPresent[key] = true;
	}

	const bool isPresent(const uint8_t key) {
		return mPresent[key];
	}

	void clearMap() {
		memset(mMap, 0, sizeof(const T) * 256);
		for (int c = 0; c < 256; ++c ) {
			mPresent[c] = false;
		}
	}
};
#endif
