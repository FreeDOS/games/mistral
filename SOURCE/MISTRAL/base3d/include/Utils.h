#ifndef UTILS_H
#define UTILS_H

namespace Utils {
	FixP::type lerpFix(const FixP::type v0, const FixP::type v1, const FixP::type t, const FixP::type total);

	int lerpInt(const int v0, const int v1, const long t, const long total);

	bool isBigEndian();

	uint32_t toNativeEndianess(const uint32_t val);

	int countLines(const char *text);
}
#endif
