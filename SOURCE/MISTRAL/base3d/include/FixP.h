#ifndef FIXP_H
#define FIXP_H

namespace FixP {
	typedef int64_t type;

	int toInt(const type fp);

	type fromInt(const int v);

	type Mul(const type v1, const type v2);

	type Div(const type v1, const type v2);

	const static FixP::type zero = 0;
	const static FixP::type one = FixP::fromInt(1);
	const static FixP::type two = FixP::fromInt(2);
	const static FixP::type minusOne = -one;
	const static FixP::type four = FixP::fromInt(4);
	const static FixP::type halfOne = FixP::Div(one, two);
}
#endif
