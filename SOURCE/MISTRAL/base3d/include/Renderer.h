#ifndef RENDERER_H
#define RENDERER_H

#ifndef RATIO_16_9
#define XRES 256
#define YRES 200
#define XRES_FRAMEBUFFER 320
#define YRES_FRAMEBUFFER 200
#define PAGE_FLIP_INCREMENT 32
#else
#define YRES_FRAMEBUFFER 200
#define XRES_FRAMEBUFFER ( ( 16 * ( YRES_FRAMEBUFFER ) ) / 9 )
#define XRES ((XRES_FRAMEBUFFER) - 64)
#define YRES (YRES_FRAMEBUFFER)
#define PAGE_FLIP_INCREMENT 32
#endif

#define HALF_XRES ( XRES / 2 )
#define HALF_YRES ( YRES / 2 )

#if defined(__DJGPP__) || (defined(WIN32) && !defined(UWP)) || defined(__MWERKS__)
#define PALETTE_COLOURS_FRAMEBUFFER
#else
#define TRUE_COLOURS_FRAMEBUFFER
#define GAMMA_DECAY
#endif

#if (defined(WIN32) && !defined(UWP)) || defined(__MWERKS__)
#define SOFTWARE_PALETTE_FRAMEBUFFER
#endif

const static int kMuzzleFlashTimeInMs = 5;
const static FixP::type kWalkingBias = 4096;

extern bool visibilityCached;
extern bool needsToRedrawVisibleMeshes;
extern int currentTarget;

extern MapWithCharKey<CTile3DProperties> tileProperties;

extern int highlightDisplayTime;
extern bool shouldShowDamageHighlight;
extern bool shouldShowDetectedHighlight;

extern MapWithCharKey<bool> occluders;
extern MapWithCharKey<bool> enemySightBlockers;
extern MapWithCharKey<bool> colliders;

extern bool linesOfSight[Mission::kMapSize][Mission::kMapSize];
extern bool revealed[Mission::kMapSize][Mission::kMapSize];

void graphicsInit(void);
void graphicsShutdown(void);
void clearRenderer(void);
void flipRenderer(void);
enum ECommand getInput(void);
void handleSystemEvents(void);
void render(long ms);
int loopTick(enum ECommand cmd);
void initRoom(int room);
void renderTick(long ms);
void updateCursorForRenderer(const int x, const int y);
void drawMap(const struct Mission::Actor *current);
void tickCamera();
void prepareCameraForPlayerDeath();
void hideGun(void);
void resetGun(void);
void shootGun(void);
void grabDisk(void);
void showGun(const bool showMuzzleFlash);
void loadTexturesForLevel(const uint8_t levelNumber);
void loadTileProperties(const uint8_t levelNumber);
#endif
