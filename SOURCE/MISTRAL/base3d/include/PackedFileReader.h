#ifndef PACKEDFILEREADER_H
#define PACKEDFILEREADER_H

namespace PackedFile {

	struct StaticBuffer {
		uint8_t *data;
		size_t size;
	};

	void init(const char *dataFilePath);

	struct StaticBuffer loadFromPath(const char *path);
}

#endif // PACKEDFILEREADER_H
