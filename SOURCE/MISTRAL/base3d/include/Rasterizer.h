#ifndef RASTERIZER_H
#define RASTERIZER_H

const static int kTransparencyColour = 199;

typedef uint32_t ABGRPixelFormat;

#ifndef TRUE_COLOURS_FRAMEBUFFER
typedef uint8_t OutputPixelFormat;
typedef uint8_t FramebufferPixelFormat;
typedef uint8_t BitmapPixelFormat;
typedef uint8_t PaletteFormat;
#else
typedef uint32_t OutputPixelFormat;
typedef uint32_t FramebufferPixelFormat;
typedef uint32_t BitmapPixelFormat;
typedef uint32_t PaletteFormat;
#endif

#define BITMAP_PIXEL_TO_FB_PIXEL(pixel) (pixel)

#ifndef SOFTWARE_PALETTE_FRAMEBUFFER
extern uint8_t palette; // dummy to satisfy the macro
#define FB_PIXEL_TO_OUTPUT_PIXEL(palette, index) (index)
#else
extern PaletteFormat palette[256];
#define FB_PIXEL_TO_OUTPUT_PIXEL(palette, index) (palette[(index)])
#endif

#ifdef PALETTE_COLOURS_FRAMEBUFFER
// ARGB -> index
#define getPaletteEntry(ARGB) ((!((ARGB) & 0xFF000000)) ? kTransparencyColour : (((ARGB) & 0xC00000) >> 16) + (((ARGB) & 0x00E000) >> 10) + (((ARGB) & 0x0000E0) >> 5))
#else
// ARGB -> RGBA
#define getPaletteEntry(ARGB) ((!((ARGB) & 0xFF000000)) ? kTransparencyColour : 0xFF + ((ARGB) << 8))
#endif

struct Bitmap {
	BitmapPixelFormat *data;
	int width;
	int height;
	uint32_t uploadId;
};

struct Texture;
typedef uint8_t UVCoord;

extern FramebufferPixelFormat framebuffer[XRES_FRAMEBUFFER * YRES_FRAMEBUFFER];
extern FramebufferPixelFormat previousFrame[XRES_FRAMEBUFFER * YRES_FRAMEBUFFER];
extern struct Bitmap *defaultFont;
extern int clippingY1;
extern int turnTarget;
extern int turnStep;
extern enum ECommand mTurnBuffer;
extern bool shouldDrawLights;
extern bool useDither;
extern enum ECommand mBufferedCommand;

void graphicsInit(void);

void graphicsShutdown(void);

void clearRenderer(void);

void flipRenderer(void);

void render(long ms);

enum ECommand getInput(void);

void handleSystemEvents(void);

void initHW(void);

void shutdownHW(void);

int loopTick(enum ECommand cmd);

void fill(
		const int x, const int y,
		const size_t dx, const size_t dy,
		const FramebufferPixelFormat pixel, const bool stipple);

void drawTextAt(const int x,
				const int y,
				const char *text,
				const FramebufferPixelFormat colour);

void drawBitmap(const int x,
				const int y,
				struct Bitmap *tile,
				const bool transparent);

void drawRepeatBitmap(
		const int x,
		const int y,
		const size_t dx,
		const size_t dy,
		struct Bitmap *tile);

void drawRect(const int x,
			  const int y,
			  const size_t dx,
			  const size_t dy,
			  const FramebufferPixelFormat pixel);

void renderPageFlip(OutputPixelFormat *stretchedBuffer, FramebufferPixelFormat *currentFrame, FramebufferPixelFormat *prevFrame, int turnState, int turnTarget, bool scale200To240);

#endif // RASTERIZER_H
