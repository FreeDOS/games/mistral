#ifndef DUNGEON_H
#define DUNGEON_H

namespace Mission {
	static const int kTargetNone = -1;
	const static int kMapSize = 64;
	const static int kMaxAgentsInBase = 32;

	static const char kPlayerSpawnPointToken = '4';
	static const char kClueSpawnPointToken = 'K';
	static const char kEnemyType0SpawnPointToken = 'e';
	static const char kEnemyType1SpawnPointToken = 'f';
	static const char kHostageSpawnPointToken = '?';
	static const char kMapExitToken = 'E';
	static const char kTileCollisionToken = '1';

	struct Item {
		Vec::Vec2i position;
		bool present;
	};

	struct Actor {
		enum EDirection direction;
		enum EActorsSnapshotElement symbol;
		Vec::Vec2i position;
		Vec::Vec2i target;
		uint8_t life;
		uint8_t ammo;
	};

	struct GameSnapshot {
		Vec::Vec2i playerTarget;
		enum CrawlerState should_continue;
		int turn;
		int mapIndex;
		bool detected;
		bool covered;
		bool keyCollected;
		bool infoCollected;
		bool targetLocated;
	};

	struct GameSnapshot tick(const enum ECommand cmd);

	void loadMap(
			const uint8_t *mapData,
			const char *collisions,
			const int map);

	int canSeeSpy(
			const Vec::Vec2i seer,
			int direction,
			const Vec::Vec2i target);

	void setIsDamage(void);

	void setPlayerWasDetected(void);

	void selectNextPlayerTarget(void);

	extern uint8_t map[Mission::kMapSize][Mission::kMapSize];
	extern Vec::Vec2i previousPlayerPosition;
	extern long gameTicks;
	extern enum EItemsSnapshotElement mItems[Mission::kMapSize][Mission::kMapSize];
	extern enum EActorsSnapshotElement mActors[Mission::kMapSize][Mission::kMapSize];
	extern struct Mission::Actor playerCrawler;
}
#endif
