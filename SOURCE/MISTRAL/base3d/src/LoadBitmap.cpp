#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "Compat.h"
#include "Enums.h"
#include "FixP.h"
#include "LoadBitmap.h"
#include "PackedFileReader.h"
#include "Vec.h"
#include "Actor.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "Dungeon.h"
#include "Renderer.h"
#include "Rasterizer.h"
#include "Tesselation.h"

struct Bitmap *loadBitmap(const char *filename) {

	PackedFile::StaticBuffer src = PackedFile::loadFromPath(filename);
	size_t sizeInDisk  = src.size - 4; //total size minus the header

	struct Bitmap *toReturn =
			(struct Bitmap *) calloc(1, sizeof(struct Bitmap));

	uint16_t tmp;
	uint8_t *ptr = src.data;

	tmp = *ptr++;
	toReturn->width = (tmp & 0xFF) << 8;
	tmp = *ptr++;
	toReturn->width += tmp & 0xFF;

	tmp = *ptr++;
	toReturn->height = (tmp & 0xFF) << 8;
	tmp = *ptr++;
	toReturn->height += tmp & 0xFF;

	size_t size = toReturn->width * toReturn->height * sizeof(BitmapPixelFormat);
	uint8_t *buffer = (uint8_t *) calloc(1, sizeInDisk);


	memcpy( buffer, ptr, sizeInDisk);

	toReturn->data = (TexturePixelFormat *) calloc(1, size);

	uint8_t repetitions;
	int pixelIndex = 0;
	BitmapPixelFormat pixel;

#ifdef PALETTE_COLOURS_FRAMEBUFFER
	for (size_t c = 0; c < sizeInDisk; c += 2) {
		pixel = buffer[c];
		repetitions = buffer[c + 1];

		for (uint8_t d = 0; d < repetitions; ++d) {
			toReturn->data[pixelIndex++] = pixel;
		}
	}
#else
	for (size_t c = 0; c < sizeInDisk; c += 5) {
		pixel = 0;

		if (buffer[c + 3] < 255) {
			pixel = kTransparencyColour;
		} else {
			pixel += buffer[c + 0] << 24;
			pixel += buffer[c + 1] << 16;
			pixel += buffer[c + 2] << 8;
			pixel += buffer[c + 3] << 0;
		}
		repetitions = buffer[c + 4];

		for (uint8_t d = 0; d < repetitions; ++d) {
			toReturn->data[pixelIndex++] = pixel;
		}
	}
#endif
	free(buffer);

	toReturn->uploadId = -1;

	return toReturn;
}

void releaseBitmap(struct Bitmap *ptr) {
	assert(ptr != NULL);

	free(ptr->data);
	free(ptr);
}
