#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Compat.h"
#include "FixP.h"
#include "Utils.h"
#include "Enums.h"
#include "Vec.h"
#include "Dungeon.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "Renderer.h"
#include "Rasterizer.h"
#include "Tesselation.h"
#include "Engine.h"
#include "PackedFileReader.h"
#include "SoundSystem.h"

enum CrawlerState shouldContinue = kCrawlerGameInProgress;
struct Mission::GameSnapshot snapshot; //duplication detected!
bool eventsDetected = false;
const static FixP::type kCameraYDeltaPlayerDeath = FixP::Div(FixP::fromInt(9), FixP::fromInt(10));
const static FixP::type kCameraYSpeedPlayerDeath = FixP::Div(FixP::fromInt(1), FixP::fromInt(10));

void onLevelLoaded(int index) {
	eventsDetected = false;
	shouldShowDetectedHighlight = false;
	highlightDisplayTime = 0;
	tileProperties.clearMap();
	loadTexturesForLevel(index);
	loadTileProperties(index);
	memset(&revealed[0], false, 40 * 40 * sizeof(bool));
}

void tickMission(enum ECommand cmd) {

	snapshot = Mission::tick(cmd);

	eventsDetected = snapshot.detected;
	shouldContinue = snapshot.should_continue;
	updateCursorForRenderer(snapshot.playerTarget.x, snapshot.playerTarget.y);

	if (Mission::playerCrawler.life <= 0) {
		shouldContinue = kCrawlerGameOver;
	}

	if (shouldContinue != kCrawlerGameInProgress) {
		Mission::gameTicks = 0;
	}
}

void Mission::setIsDamage() {
	shouldShowDamageHighlight = true;
	highlightDisplayTime = 10;
	/* enemy hit you */
}

void Mission::setPlayerWasDetected() {
	shouldShowDetectedHighlight = true;
	highlightDisplayTime = 10;
	/* enemy detected you */
	playSound(PLAYER_GOT_DETECTED_SOUND);
}

void prepareCameraForPlayerDeath() {
	playerHeightChangeRate = kCameraYSpeedPlayerDeath;
}

void loadMap(int map, MapWithCharKey<bool> *collisionMap) {

	/* all the char keys plus null terminator */
	char collisions[256 + 1];
	char nameBuffer[16];

	collisions[256] = 0;

	for (int c = 0; c < 256; ++c) {
		collisions[c] = collisionMap->getFromMap(c) ? '1' : '0';
	}

	/* 16 bytes should be enough here... */

	sprintf(nameBuffer, "map%d.txt", map);
	struct PackedFile::StaticBuffer buffer = PackedFile::loadFromPath(nameBuffer);
	Mission::loadMap(buffer.data, collisions, map - 1);
	free(buffer.data);
}

int Mission::canSeeSpy(const Vec::Vec2i seer,
					   int direction,
					   const Vec::Vec2i target) {

	int dx = (target.x - seer.x);
	int dy = (target.y - seer.y);
	FixP::type x = FixP::fromInt(seer.x);
	FixP::type y = FixP::fromInt(seer.y);
	FixP::type incX;
	FixP::type incY;
	FixP::type targetX = FixP::fromInt(target.x);
	FixP::type targetY = FixP::fromInt(target.y);

	/* we must pick the bigger, iterate on each square of it and slowly increment the smaller */

	if (abs(dx) >= abs(dy)) {

		/* ++x, y += incY */
		FixP::type inc = FixP::zero;

		if (dx != 0) {
			inc = FixP::Div(FixP::fromInt(abs(dy)), FixP::fromInt(abs(dx)));

			if ((direction == 0 || direction == 2)) {
				return false;
			}

			if (dy < 0) {
				inc = -inc;
			}
		}

		incY = inc;

		if (dx >= 0) {
			incX = FixP::one;
		} else {
			incX = -FixP::one;
		}

		if ((direction == 3) && incX > FixP::zero) {
			return false;
		}
		if ((direction == 1) && incX < FixP::zero) {
			return false;
		}

	} else {
		/* ++y, x += incX */
		FixP::type inc = FixP::zero;

		if (dy != 0) {
			inc = FixP::Div(FixP::fromInt(abs(dx)), FixP::fromInt(abs(dy)));

			if ((direction == 1 || direction == 3)) {
				return false;
			}

			if (dx < 0) {
				inc = -inc;
			}
		}

		incX = inc;

		if (dy >= 0) {
			incY = FixP::one;
		} else {
			incY = -FixP::one;
		}

		if ((direction == 2) && incY < FixP::zero) {
			return false;
		}

		if ((direction == 0) && incY > FixP::zero) {
			return false;
		}
	}

    int x0 = seer.x;
    int y0 = seer.y;
    int x1 = target.x;
    int y1 = target.y;
    dx = abs(x1 - x0);
    int sx = x0 < x1 ? 1 : -1;
    dy = -abs(y1 - y0);
    int sy = y0 < y1 ? 1 : -1;
    int error = dx + dy;

	while (true) {


		int tile = map[y0][x0];

		if (tile == 0) {
			/* map is not loaded  yet... */
			return false;
		}

		if (enemySightBlockers.getFromMap(tile)) {
			return false;
		}

		if ((abs(x - targetX) < FixP::one) && (abs(y - targetY) < FixP::one)) {
			return true;
		}

	    if (x0 >= Mission::kMapSize && y0 >= Mission::kMapSize && x0 < 0 && y0 < 0) {
	    	return false;
	    }

		linesOfSight[y0][x0] = true;

		if (x0 == x1 && y0 == y1) {
			return true;
		}

		int e2 = 2 * error;

		if (e2 >= dy) {
            if (x0 == x1) {
                return true;
            }
			error = error + dy;
			x0 = x0 + sx;
		}

		if (e2 <= dx) {
            if (y0 == y1) {
                return true;
            }
			error = error + dx;
			y0 = y0 + sy;
		}
	}

	return false;
}

void renderTick(long ms) {
	render(ms);

	// dont render the mission status when we're about to
	// rotate the camera
	if (textBuffer[0] != 0 && (mTurnBuffer == kCommandNone && turnStep == turnTarget)) {
		int lines = Utils::countLines(&textBuffer[0]);

#ifndef TRUE_COLOURS_FRAMEBUFFER
		fill(0, 8, XRES, lines * 8, getPaletteEntry(0xFF000000), true);
		drawTextAt(1, 2, textBuffer, getPaletteEntry(0xFFFFFFFF));
#else
		fill(0, 8, XRES, lines * 8, getPaletteEntry(0xFF999999), false);
		drawTextAt(1, 2, textBuffer, getPaletteEntry(0xFFFFFFFF));
#endif


	}

	if (eventsDetected) {
		drawTextAt(21, 1, "(detected)", getPaletteEntry(0xFFFFFFFF));
	}

	if (snapshot.keyCollected) {
		drawTextAt(((XRES_FRAMEBUFFER / 8) - 6), 20, "Key", getPaletteEntry(0xFF00AA00));
	}
}

int loopTick(enum ECommand command) {

	bool needRedraw = false;

	if (command == kCommandBack) {
		shouldContinue = kCrawlerQuit;

		// if it's the first tick of a given mission
	} else if (command != kCommandNone || Mission::gameTicks == 0) {

		if (command == kCommandFire1 ||
			command == kCommandFire2 ||
			command == kCommandFire3 ||
			command == kCommandFire4) {

			int showMuzzleFlash = ((Mission::playerCrawler.ammo > 0) &&
					command == kCommandFire1 && currentTarget != -1);

			visibilityCached = false;

			if (showMuzzleFlash) {
				/* you firing a shot */
				playSound(PLAYER_FIRING_GUN);
			}

			showGun(showMuzzleFlash);

		} else {
			hideGun();
		}

		tickMission(command);

		if (Mission::gameTicks != 0) {
#ifdef ENABLE_VERTICAL_MOVEMENT
			struct CTile3DProperties prop = tileProperties.getFromMap(Mission::map[Mission::playerCrawler.position.y][Mission::playerCrawler.position.x]);

            struct CTile3DProperties prevProp = tileProperties.getFromMap(Mission::map[Mission::previousPlayerPosition.y][Mission::previousPlayerPosition.x]);


			cameraOffset.mY = prop.mFloorHeight - prevProp.mFloorHeight;
#else
			cameraOffset.mY = 0;
#endif
			if (cameraOffset.mY < FixP::halfOne && cameraOffset.mY > -FixP::halfOne) {
				cameraOffset.mY = 0;
			}
		} else {
			cameraOffset.mY = 0;
		}

		Mission::previousPlayerPosition = Mission::playerCrawler.position;

		needRedraw = true;
	}

	if (cameraOffset.mZ != 0 || cameraOffset.mX != 0 || cameraOffset.mY != 0) {
		needRedraw = true;
	}

	tickCamera();

	if (needRedraw) {
		drawMap(&Mission::playerCrawler);
	}

	return shouldContinue;
}

void initRoom(int room) {

	playerHeight = 0;
	playerHeightTarget = kCameraYDeltaPlayerDeath;
	playerHeightChangeRate = 0;

	resetGun();
	shouldContinue = kCrawlerGameInProgress;
	mBufferedCommand = kCommandNone;
	Mission::gameTicks = 0;
	visibilityCached = false;
	needsToRedrawVisibleMeshes = true;
	onLevelLoaded(room + 1);

	for (int c = 0; c < 256; ++c) {
		if (tileProperties.isPresent(c)) {
			colliders.setInMap(c, tileProperties.getFromMap(c).mBlockMovement);
		} else {
			colliders.setInMap(c, NULL);
		}
	}

	loadMap(room + 1, &colliders);
	textBuffer[0] = 0;
}
