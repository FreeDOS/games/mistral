#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "Compat.h"
#include "FixP.h"
#include "Utils.h"
#include "Enums.h"
#include "Vec.h"
#include "MapWithCharKey.h"
#include "VisibilityStrategy.h"
#include "Dungeon.h"

namespace Visibility {
	Vec::Vec2i transform(const enum EDirection from, const Vec::Vec2i currentPos) {

		Vec::Vec2i toReturn = Vec::Vec2i();

		switch (from) {
			case kNorth:
				toReturn = currentPos;
				break;
			case kSouth:
				Vec::init(&toReturn, (int8_t) (Mission::kMapSize - currentPos.x - 1),
						  (int8_t) (Mission::kMapSize - currentPos.y - 1));
				break;

			case kEast:
				Vec::init(&toReturn, (int8_t) (Mission::kMapSize - currentPos.y - 1),
						  (int8_t) (Mission::kMapSize - currentPos.x - 1));
				break;
			case kWest:
				Vec::init(&toReturn, (int8_t) (currentPos.y),
						  (int8_t) (currentPos.x));
				break;
			default:
				assert(false);
		}

		return toReturn;
	}

	void castVisibility(const enum EDirection from,
						enum EStatus *visMap,
						const Vec::Vec2i pos,
						Vec::Vec2i *distances,
						const bool cleanPrevious,
						MapWithCharKey<bool> *occluderTiles) {

		const Vec::Vec2i originalPos = transform(from, pos);
		Vec::Vec2i positions[Mission::kMapSize + Mission::kMapSize];

		/* The -1 is due to the fact I will add a new element. */
		Vec::Vec2i *stackHead = &positions[0];
		Vec::Vec2i *stackEnd = stackHead + (Mission::kMapSize + Mission::kMapSize);
		Vec::Vec2i *stackRoot = stackHead;

		Vec::Vec2i rightOffset = Vec::mapOffsetForDirection(kEast);
		Vec::Vec2i leftOffset = Vec::mapOffsetForDirection(kWest);
		Vec::Vec2i northOffset = Vec::mapOffsetForDirection(kNorth);
		uint8_t bucketPositions[Mission::kMapSize + Mission::kMapSize];

		if (cleanPrevious) {
			memset(visMap, 0, sizeof(kInvisible) * Mission::kMapSize * Mission::kMapSize);
		}

		*stackHead = originalPos;
		++stackHead;

		memset(distances, -128, sizeof(Vec::Vec2i) * 2 * Mission::kMapSize * Mission::kMapSize);
		memset(&bucketPositions, 0, sizeof(uint8_t) * (Mission::kMapSize + Mission::kMapSize));

		while (stackHead != stackRoot) {

			--stackHead;

			const Vec::Vec2i currentPos = *stackHead;

			Vec::Vec2i transformed = transform(from, currentPos);

			if (!(0 <= transformed.x && transformed.x < Mission::kMapSize && 0 <= transformed.y &&
				  transformed.y < Mission::kMapSize)) {
				continue;
			}

			if (visMap[(transformed.y * Mission::kMapSize) + transformed.x] == kVisible) {
				continue;
			}

			visMap[(transformed.y * Mission::kMapSize) + transformed.x] = kVisible;

			int verticalDistance = (currentPos.y - originalPos.y);

			int manhattanDistance =
					abs(verticalDistance) + abs(currentPos.x - originalPos.x);

			if (manhattanDistance < (2 * Mission::kMapSize)) {
				distances[(manhattanDistance * (Mission::kMapSize))
						  + (bucketPositions[manhattanDistance]++)] = transformed;
			}

			if (occluderTiles->getFromMap(Mission::map[transformed.y][transformed.x])) {
				continue;
			}

			int narrowing = abs(verticalDistance);

			if (((currentPos.x - originalPos.x) >= -narrowing)
				&& (currentPos.x - originalPos.x) <= 0
				&& (stackHead != stackEnd)) {
				Vec::init(stackHead++, (int8_t) (currentPos.x + leftOffset.x),
						  (int8_t) (currentPos.y + leftOffset.y));
			}

			if (((currentPos.x - originalPos.x) <= narrowing)
				&& (currentPos.x - originalPos.x) >= 0
				&& (stackHead != stackEnd)) {
				Vec::init(stackHead++, (int8_t) (currentPos.x + rightOffset.x),
						  (int8_t) (currentPos.y + rightOffset.y));
			}

			if (verticalDistance <= 0 && (stackHead != stackEnd)) {
				Vec::init(stackHead++, (int8_t) (currentPos.x + northOffset.x),
						  (int8_t) (currentPos.y + northOffset.y));
			}
		}
	}
}
