#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "Compat.h"
#include "FixP.h"
#include "Enums.h"
#include "Vec.h"
#include "Actor.h"
#include "MapWithCharKey.h"
#include "LoadBitmap.h"
#include "Dungeon.h"
#include "Tile3DProperties.h"
#include "Renderer.h"
#include "Rasterizer.h"
#include "Tesselation.h"
#include "PackedFileReader.h"

FixP::type playerHeight = 0;
FixP::type walkingBias = 0;
FixP::type playerHeightChangeRate = 0;
FixP::type playerHeightTarget = 0;

#ifdef TRUE_COLOURS_FRAMEBUFFER
int distanceForPenumbra = 1;
int distanceForDarkness = 65;
#else
int distanceForPenumbra = 16;
int distanceForDarkness = 32;
#endif

struct Projection projectionVertices[4];

const static FixP::type kMinZCull = 0;
struct Texture textures[kMaxTextures];
int usedTexture = 0;

#if __cplusplus > 199711L
static constexpr
#endif
FramebufferPixelFormat gammaForColour( FramebufferPixelFormat c0, uint8_t gamma) {

	int r0 = (c0 >> 24 ) & 0xFF;
	int r1 = max( 0, r0 - gamma);

	int g0 = (c0 >> 16 ) & 0xFF;
	int g1 = max( 0, g0 - gamma);

	int b0 = (c0 >> 8 ) & 0xFF;
	int b1 = max( 0, b0 - gamma);

	return ( 0xFF ) + (
			( r1 << 24 ) +
			( g1 << 16 ) +
			( b1 <<  8 ) );

	return c0;
}

void clearTextures() {
	usedTexture = 0;
}

struct Texture *makeTextureFrom(const char *filename) {
	PackedFile::StaticBuffer src = PackedFile::loadFromPath(filename);

	uint8_t *ptr = src.data + 4; //skip header;
	size_t sizeInDisk = src.size - 4;

	uint8_t *diskBuffer = (uint8_t *) calloc(1, sizeInDisk);
	memcpy(diskBuffer, ptr, sizeInDisk);

	BitmapPixelFormat pixel;
	int pixelIndex = 0;
	int y;
	BitmapPixelFormat buffer[NATIVE_TEXTURE_SIZE * NATIVE_TEXTURE_SIZE];

#ifdef PALETTE_COLOURS_FRAMEBUFFER
	for (size_t c = 0; c < sizeInDisk; c += 2) {
        pixel = diskBuffer[c];
        unsigned int repetitions = diskBuffer[c + 1];

        for (uint8_t d = 0; d < repetitions; ++d) {
            buffer[pixelIndex++] = pixel;
        }
    }
#else
	for (size_t c = 0; c < sizeInDisk; c += 5) {
		pixel = 0;

		if (diskBuffer[c + 3] < 255) {
			pixel = kTransparencyColour;
		} else {
			pixel += diskBuffer[c + 0] << 24;
			pixel += diskBuffer[c + 1] << 16;
			pixel += diskBuffer[c + 2] << 8;
			pixel += diskBuffer[c + 3] << 0;
		}

		unsigned int repetitions = diskBuffer[c + 4];

		for (uint8_t d = 0; d < repetitions; ++d) {
			buffer[pixelIndex++] = pixel;
		}
	}
#endif
    free(diskBuffer);

    assert((usedTexture + 1 ) < kMaxTextures);
	struct Texture *toReturn = &textures[usedTexture++];
    toReturn->uploadId = usedTexture;

    for (y = 0; y < NATIVE_TEXTURE_SIZE; ++y) {
		BitmapPixelFormat *sourceLine = &buffer[y * NATIVE_TEXTURE_SIZE];
		TexturePixelFormat *dstLine = &toReturn->rotations[0][(y * NATIVE_TEXTURE_SIZE)];
        for (int x = 0; x < NATIVE_TEXTURE_SIZE; ++x) {
            *dstLine = *sourceLine;
            sourceLine++;
            dstLine++;
        }
    }

    for (y = (NATIVE_TEXTURE_SIZE - 1); y >= 0; --y) {
		BitmapPixelFormat *sourceLine = &buffer[(y * NATIVE_TEXTURE_SIZE) + (NATIVE_TEXTURE_SIZE - 1)];
		TexturePixelFormat *dstLine = &toReturn->rotations[1][y];
        for (int x = (NATIVE_TEXTURE_SIZE - 1); x >= 0; --x) {
            *dstLine = *sourceLine;
            sourceLine--;
            dstLine += NATIVE_TEXTURE_SIZE;
        }
    }

    for (y = (NATIVE_TEXTURE_SIZE - 1); y >= 0; --y) {
		BitmapPixelFormat *sourceLine = &buffer[(y * NATIVE_TEXTURE_SIZE)];
		TexturePixelFormat *dstLine = &toReturn->rotations[2][(((NATIVE_TEXTURE_SIZE - 1) - y) * NATIVE_TEXTURE_SIZE) +
                                                   (NATIVE_TEXTURE_SIZE - 1)];
        for (int x = (NATIVE_TEXTURE_SIZE - 1); x >= 0; --x) {
            *dstLine = *sourceLine;
            sourceLine++;
            dstLine--;
        }
    }

    for (y = 0; y < NATIVE_TEXTURE_SIZE; ++y) {
		BitmapPixelFormat *sourceLine = &buffer[(((NATIVE_TEXTURE_SIZE - 1) - y) * NATIVE_TEXTURE_SIZE)];
		TexturePixelFormat *dstLine = &toReturn->rotations[3][y];
        for (int x = 0; x < NATIVE_TEXTURE_SIZE; ++x) {
            *dstLine = *sourceLine;
            sourceLine++;
            dstLine += NATIVE_TEXTURE_SIZE;
        }
    }

    for (y = 0; y < NATIVE_TEXTURE_SIZE; ++y) {
		BitmapPixelFormat *sourceLine = &buffer[y * NATIVE_TEXTURE_SIZE];
		TexturePixelFormat *dstLine = &toReturn->rowMajor[y];
        for (int x = 0; x < NATIVE_TEXTURE_SIZE; ++x) {
            *dstLine = *sourceLine;
            sourceLine++;
            dstLine += NATIVE_TEXTURE_SIZE;
        }
    }

    return toReturn;
}

void projectAllVertices(const int count) {
	const static FixP::type halfWidth = FixP::fromInt(HALF_XRES);
	const static FixP::type halfHeight = FixP::fromInt(HALF_YRES);
	const static FixP::type bias = FixP::Div(FixP::one, FixP::fromInt(128));

	int c = count;
	while (c--) {
		struct Projection *vertex = &projectionVertices[c];

		FixP::type z = (vertex->first.mZ);
		z += cameraOffset.mZ;

		if (z < FixP::one) {
			z = FixP::one;
		}

		FixP::type projected = FixP::Mul(z, FixP::halfOne);

		if (projected == FixP::zero) {
			projected += bias;
		}

		FixP::type oneOver = FixP::Div(halfHeight, projected);

		vertex->second.mX = (halfWidth + FixP::Mul(vertex->first.mX + cameraOffset.mX, oneOver));
		vertex->second.mY =
				(halfHeight
				 - FixP::Mul(vertex->first.mY + playerHeight + walkingBias + cameraOffset.mY, oneOver));
	}
}

void drawBillboardAt(const Vec::Vec3 center,
					 Bitmap *bitmap,
					 const FixP::type scale,
					 const int size) {

	if (center.mZ <= kMinZCull) {
		return;
	}

	Vec::Vec3 scaledCenter;

	Vec::init(&scaledCenter, center.mX, (center.mY), center.mZ);
	Vec::init(&projectionVertices[0].first, scaledCenter.mX, scaledCenter.mY,
			 scaledCenter.mZ);
	Vec::init(&projectionVertices[1].first, scaledCenter.mX, scaledCenter.mY,
			 scaledCenter.mZ);

	FixP::type minusScale = (-scale);

	Vec::addTo(&projectionVertices[0].first, FixP::minusOne, scale, FixP::zero);
	Vec::addTo(&projectionVertices[1].first, FixP::one, minusScale, FixP::zero);

	projectAllVertices(2);

	Vec::Vec2 ulz0 = projectionVertices[0].second;
	Vec::Vec2 lrz0 = projectionVertices[1].second;

	int z = FixP::toInt(center.mZ);

	if (z >= distanceForDarkness && useDither) {
		drawMask(ulz0.mX, ulz0.mY, lrz0.mX, lrz0.mY);
	} else {
		drawFrontWall(ulz0.mX, ulz0.mY, lrz0.mX, lrz0.mY, bitmap->data,
					  scale, z, true, size);
	}
}

void drawCornerAt(const Vec::Vec3 center,
				  const FixP::type scale,
				  const struct Texture *texture,
				  const uint8_t mask,
				  const bool enableAlpha,
				  const bool repeatTexture) {

	const FixP::type halfScale = scale;
	const FixP::type minusHalfScale = (-scale);
	const FixP::type textureScale = (repeatTexture ? halfScale : FixP::one);

	if (center.mZ <= kMinZCull) {
		return;
	}

	Vec::Vec3 scaledCenter;

	Vec::init(&scaledCenter, center.mX, center.mY, center.mZ);

	/*
		 2|\             /|
		  | \  center   / |
		  |  \    *    /  |
		  |   \0__|___/   |3
		  |   |   |  |   /
		   \  |   X  |  /
			\ |      | /
			 \|_____1|/


			behind
			 ___
	 left    |_|  right

		   front
  */

	projectionVertices[0].first = projectionVertices[1].first = projectionVertices[2].first = projectionVertices[3].first = scaledCenter;

	Vec::addTo(&projectionVertices[0].first,  FixP::minusOne, halfScale,  FixP::minusOne);
	Vec::addTo(&projectionVertices[1].first, FixP::one, minusHalfScale,  FixP::minusOne);
	Vec::addTo(&projectionVertices[2].first,  FixP::minusOne, halfScale, FixP::one);
	Vec::addTo(&projectionVertices[3].first, FixP::one, minusHalfScale, FixP::one);

	projectAllVertices(4);

	Vec::Vec2 p0 = projectionVertices[0].second;
	Vec::Vec2 p1 = projectionVertices[1].second;
	Vec::Vec2 p2 = projectionVertices[2].second;
	Vec::Vec2 p3 = projectionVertices[3].second;

	int z = FixP::toInt(center.mZ);

	if ( (mask & MASK_BEHIND) || (enableAlpha && (mask & MASK_FRONT))) {
		if (z >= distanceForDarkness && useDither) {
			drawMask(p2.mX, p2.mY, p3.mX, p3.mY);
		} else {
			drawFrontWall(p2.mX, p2.mY, p3.mX, p3.mY, texture->rotations[0],
						  (textureScale), z, enableAlpha, NATIVE_TEXTURE_SIZE);
		}
	}

	int originalZ = z;

	if (((mask & MASK_RIGHT) && FixP::toInt(center.mX) > 0) || (mask & MASK_FORCE_RIGHT)) {

		if (mask & ~MASK_BEHIND) {
			z -= 2;
		}

		if (z >= distanceForDarkness && useDither) {
			maskWall(p2.mX, p0.mX, p2.mY, p3.mY, p0.mY, p1.mY);
		} else {
			drawWall(p2.mX, p0.mX, p2.mY, p3.mY, p0.mY, p1.mY, texture->rowMajor,
					 (textureScale), z);
		}

		z = originalZ;
	}

	if (((mask & MASK_LEFT) && FixP::toInt(center.mX) < 0) || (mask & MASK_FORCE_LEFT)) {
		if (mask & ~MASK_BEHIND) {
			z -= 2;
		}

		if (z >= distanceForDarkness && useDither) {
			maskWall(p1.mX, p3.mX, p0.mY, p1.mY, p2.mY, p3.mY);
		} else {
			drawWall(p1.mX, p3.mX, p0.mY, p1.mY, p2.mY, p3.mY, texture->rowMajor,
					 (textureScale), z);
		}

		z = originalZ;
	}

	if ( (mask & MASK_FRONT) ) {
		if (mask & ~MASK_BEHIND) {
			z -= 2;
		}

		if (z >= distanceForDarkness && useDither) {
			drawMask(p0.mX, p0.mY, p1.mX, p1.mY);
		} else {
			drawFrontWall(p0.mX, p0.mY, p1.mX, p1.mY, texture->rotations[0],
						  (textureScale), z, enableAlpha, NATIVE_TEXTURE_SIZE);
		}
	}
}

void drawColumnAt(const Vec::Vec3 center,
				  const FixP::type scale,
				  const struct Texture *texture,
				  const uint8_t mask,
				  const bool enableAlpha,
				  const bool repeatTexture) {

	const FixP::type halfScale = scale;
	const FixP::type minusHalfScale = (-scale);
	const FixP::type textureScale = (repeatTexture ? halfScale : FixP::one);

	if (center.mZ <= kMinZCull) {
		return;
	}

	Vec::Vec3 scaledCenter;

	Vec::init(&scaledCenter, center.mX, center.mY, center.mZ);

	/*
		 2|\             /|
		  | \  center   / |
		  |  \    *    /  |
		  |   \0__|___/   |3
		  |   |   |  |   /
		   \  |   X  |  /
			\ |      | /
			 \|_____1|/

  */
	projectionVertices[0].first = projectionVertices[1].first = projectionVertices[2].first = projectionVertices[3].first = scaledCenter;

	Vec::addTo(&projectionVertices[0].first,  FixP::minusOne, halfScale,  FixP::minusOne);
	Vec::addTo(&projectionVertices[1].first, FixP::one, minusHalfScale,  FixP::minusOne);
	Vec::addTo(&projectionVertices[2].first,  FixP::minusOne, halfScale, FixP::one);
	Vec::addTo(&projectionVertices[3].first, FixP::one, minusHalfScale, FixP::one);

	projectAllVertices(4);

	Vec::Vec2 p0 = projectionVertices[0].second;
	Vec::Vec2 p1 = projectionVertices[1].second;
	Vec::Vec2 p2 = projectionVertices[2].second;
	Vec::Vec2 p3 = projectionVertices[3].second;

	int z = FixP::toInt(center.mZ);

	if (enableAlpha && (mask & MASK_FRONT)) {
		if (z >= distanceForDarkness && useDither) {
			drawMask(p2.mX, p2.mY, p3.mX, p3.mY);
		} else {
			drawFrontWall(p2.mX, p2.mY, p3.mX, p3.mY, texture->rotations[0],
						  (textureScale), z, enableAlpha, NATIVE_TEXTURE_SIZE);
		}
	}

	if ((mask & MASK_RIGHT) && FixP::toInt(center.mX) > 0) {
		if (z >= distanceForDarkness && useDither) {
			maskWall(p2.mX, p0.mX, p2.mY, p3.mY, p0.mY, p1.mY);
		} else {
			drawWall(p2.mX, p0.mX, p2.mY, p3.mY, p0.mY, p1.mY, texture->rowMajor,
					 (textureScale), z);
		}
	}

	if ((mask & MASK_LEFT) && FixP::toInt(center.mX) < 0) {
		if (z >= distanceForDarkness && useDither) {
			maskWall(p1.mX, p3.mX, p0.mY, p1.mY, p2.mY, p3.mY);
		} else {
			drawWall(p1.mX, p3.mX, p0.mY, p1.mY, p2.mY, p3.mY, texture->rowMajor,
					 (textureScale), z);
		}
	}

	if ((mask & MASK_BEHIND) || (mask & MASK_FRONT)) {
		if (z >= distanceForDarkness && useDither) {
			drawMask(p0.mX, p0.mY, p1.mX, p1.mY);
		} else {
			drawFrontWall(p0.mX, p0.mY, p1.mX, p1.mY, texture->rotations[0],
						  (textureScale), z, enableAlpha, NATIVE_TEXTURE_SIZE);
		}
	}
}

void drawRampAt(const Vec::Vec3 p0, const Vec::Vec3 p1,
				const struct Texture *texture, enum EDirection cameraDirection, bool flipTexture) {

	if (min(p0.mZ, p1.mZ) <= kMinZCull) {
		return;
	}

	if (cameraDirection == kNorth) {

		projectionVertices[0].first = projectionVertices[1].first = p0;
		projectionVertices[2].first = projectionVertices[3].first = p1;

		if (flipTexture) {
			cameraDirection = kSouth;
		}

	} else if (cameraDirection == kSouth) {

		projectionVertices[0].first = projectionVertices[1].first = p1;
		projectionVertices[2].first = projectionVertices[3].first = p0;

		if (flipTexture) {
			cameraDirection = kNorth;
		}

	} else {
		if (cameraDirection == kEast) {

			projectionVertices[0].first = projectionVertices[2].first = p1;
			projectionVertices[1].first = projectionVertices[3].first = p0;
		} else {
			projectionVertices[0].first = projectionVertices[2].first = p0;
			projectionVertices[1].first = projectionVertices[3].first = p1;
		}

		Vec::addTo(&projectionVertices[0].first,  FixP::minusOne, FixP::zero,  FixP::minusOne);
		Vec::addTo(&projectionVertices[1].first,      FixP::one, FixP::zero,  FixP::minusOne);
		Vec::addTo(&projectionVertices[2].first,  FixP::minusOne, FixP::zero,      FixP::one);
		Vec::addTo(&projectionVertices[3].first,      FixP::one, FixP::zero,      FixP::one);

		projectAllVertices(4);

		Vec::Vec2 llz0 = projectionVertices[0].second;
		Vec::Vec2 lrz0 = projectionVertices[1].second;
		Vec::Vec2 llz1 = projectionVertices[2].second;
		Vec::Vec2 lrz1 = projectionVertices[3].second;

		uint8_t uvCoords[6];
		int coords[6];

		if (flipTexture) {
			uvCoords[0] = uvCoords[2] = uvCoords[3] = 0;
			uvCoords[1] = uvCoords[4] = uvCoords[5] = 32;
		} else {
			uvCoords[0] = uvCoords[2] = uvCoords[3] = 32;
			uvCoords[1] = uvCoords[4] = uvCoords[5] = 0;
		}

		coords[0] = FixP::toInt(llz1.mX); // 2
		coords[1] = FixP::toInt(llz1.mY);
		coords[2] = FixP::toInt(lrz1.mX); // 3
		coords[3] = FixP::toInt(lrz1.mY);
		coords[4] = FixP::toInt(llz0.mX); // 0
		coords[5] = FixP::toInt(llz0.mY);

		drawTexturedTriangle(&coords[0], &uvCoords[0], (struct Texture*)texture );

		if (flipTexture) {
			uvCoords[0] = uvCoords[1] = uvCoords[4] = 32;
			uvCoords[2] = uvCoords[3] = uvCoords[5] = 0;
		} else {
			uvCoords[0] = uvCoords[1] = uvCoords[4] = 0;
			uvCoords[2] = uvCoords[3] = uvCoords[5] = 32;
		}

		coords[0] = FixP::toInt(llz0.mX); //0
		coords[1] = FixP::toInt(llz0.mY);
		coords[2] = FixP::toInt(lrz1.mX); //3
		coords[3] = FixP::toInt(lrz1.mY);
		coords[4] = FixP::toInt(lrz0.mX); //1
		coords[5] = FixP::toInt(lrz0.mY);

		drawTexturedTriangle( &coords[0], &uvCoords[0], (struct Texture*)texture );

		return;
	}

	Vec::addTo(&projectionVertices[0].first,  FixP::minusOne, FixP::zero,  FixP::minusOne);
	Vec::addTo(&projectionVertices[1].first, FixP::one, FixP::zero,  FixP::minusOne);
	Vec::addTo(&projectionVertices[2].first,  FixP::minusOne, FixP::zero, FixP::one);
	Vec::addTo(&projectionVertices[3].first, FixP::one, FixP::zero, FixP::one);

	projectAllVertices(4);

	Vec::Vec2 llz0 = projectionVertices[0].second;
	Vec::Vec2 lrz0 = projectionVertices[1].second;
	Vec::Vec2 llz1 = projectionVertices[2].second;
	Vec::Vec2 lrz1 = projectionVertices[3].second;

	int z = FixP::toInt(p0.mZ);

	if (z >= distanceForDarkness && useDither) {
		maskFloor(llz1.mY, lrz0.mY, llz1.mX, lrz1.mX, llz0.mX, lrz0.mX);
	} else {
		drawFloor(llz1.mY, lrz0.mY, llz1.mX, lrz1.mX, llz0.mX, lrz0.mX, z, texture->rotations[cameraDirection] );
	}
}

void drawFloorAt(const Vec::Vec3 center,
				 const struct Texture *texture, enum EDirection cameraDirection) {

	const static FixP::type threshold = FixP::zero;

	if (center.mZ <= kMinZCull) {
		return;
	}

	if (center.mY <= threshold || cameraOffset.mY < 0) {

		projectionVertices[0].first = projectionVertices[1].first = projectionVertices[2].first = projectionVertices[3].first = center;

		Vec::addTo(&projectionVertices[0].first,  FixP::minusOne, FixP::zero,  FixP::minusOne);
		Vec::addTo(&projectionVertices[1].first, FixP::one, FixP::zero,  FixP::minusOne);
		Vec::addTo(&projectionVertices[2].first,  FixP::minusOne, FixP::zero, FixP::one);
		Vec::addTo(&projectionVertices[3].first, FixP::one, FixP::zero, FixP::one);

		projectAllVertices(4);

		Vec::Vec2 llz0 = projectionVertices[0].second;
		Vec::Vec2 lrz0 = projectionVertices[1].second;
		Vec::Vec2 llz1 = projectionVertices[2].second;
		Vec::Vec2 lrz1 = projectionVertices[3].second;

		int z = FixP::toInt(center.mZ);

		if (z >= distanceForDarkness && useDither) {
			maskFloor(llz1.mY, lrz0.mY, llz1.mX, lrz1.mX, llz0.mX, lrz0.mX
			);
		} else {
			drawFloor(llz1.mY, lrz0.mY, llz1.mX, lrz1.mX, llz0.mX, lrz0.mX, z,
					  texture->rotations[cameraDirection]);
		}
	}
}

void drawCeilingAt(const Vec::Vec3 center,
				   const struct Texture *texture, enum EDirection cameraDirection) {

	const static FixP::type threshold = FixP::zero;

	if (center.mZ <= kMinZCull) {
		return;
	}

	if (center.mY >= threshold || cameraOffset.mY > 0) {
		projectionVertices[0].first = projectionVertices[1].first = projectionVertices[2].first = projectionVertices[3].first = center;

		Vec::addTo(&projectionVertices[0].first, FixP::minusOne, FixP::zero,  FixP::minusOne);
		Vec::addTo(&projectionVertices[2].first, FixP::minusOne, FixP::zero, FixP::one);
		Vec::addTo(&projectionVertices[1].first, FixP::one, FixP::zero,  FixP::minusOne);
		Vec::addTo(&projectionVertices[3].first, FixP::one, FixP::zero, FixP::one);

		projectAllVertices(4);

		Vec::Vec2 llz0 = projectionVertices[0].second;
		Vec::Vec2 lrz0 = projectionVertices[1].second;
		Vec::Vec2 llz1 = projectionVertices[2].second;
		Vec::Vec2 lrz1 = projectionVertices[3].second;

		int z = FixP::toInt(center.mZ);

		if (z >= distanceForDarkness && useDither) {
			maskFloor(llz1.mY, lrz0.mY, llz1.mX, lrz1.mX, llz0.mX, lrz0.mX
			);
		} else {
			drawFloor(llz1.mY, lrz0.mY, llz1.mX, lrz1.mX, llz0.mX, lrz0.mX, z,
					  texture->rotations[cameraDirection]);
		}
	}
}

void drawLeftNear(const Vec::Vec3 center,
				  const FixP::type scale,
				  const struct Texture *texture,
				  const uint8_t mask,
				  const bool repeatTexture) {

	if (center.mZ <= kMinZCull) {
		return;
	}

	const FixP::type halfScale = scale;
	const FixP::type minusHalfScale = (-scale);
	const FixP::type textureScale = (repeatTexture ? halfScale : FixP::one);

	FixP::type depth = FixP::one;
	FixP::type minusDepth =  FixP::minusOne;


	if (mask & MASK_BEHIND) {

		projectionVertices[0].first = projectionVertices[1].first = center;

		Vec::addTo(&projectionVertices[0].first,  FixP::minusOne, minusHalfScale,  FixP::minusOne);
		Vec::addTo(&projectionVertices[1].first, FixP::one, halfScale,  FixP::minusOne);

		projectAllVertices(2);

		drawMask(projectionVertices[0].second.mX, projectionVertices[0].second.mY,
				 projectionVertices[1].second.mX, projectionVertices[1].second.mY);

		return;
	}

	if (cameraDirection == kWest || cameraDirection == kEast) {
		depth =  FixP::minusOne;
		minusDepth = FixP::one;
	}

	projectionVertices[0].first = projectionVertices[1].first = projectionVertices[2].first = projectionVertices[3].first = center;

	Vec::addTo(&projectionVertices[0].first,  FixP::minusOne, halfScale, minusDepth);
	Vec::addTo(&projectionVertices[1].first, FixP::one, halfScale, depth);
	Vec::addTo(&projectionVertices[2].first,  FixP::minusOne, minusHalfScale, minusDepth);
	Vec::addTo(&projectionVertices[3].first, FixP::one, minusHalfScale, depth);

	projectAllVertices(4);

	Vec::Vec2 ulz0 = projectionVertices[0].second;
	Vec::Vec2 urz0 = projectionVertices[1].second;
	Vec::Vec2 llz0 = projectionVertices[2].second;
	Vec::Vec2 lrz0 = projectionVertices[3].second;
	int z = FixP::toInt(center.mZ);

	if (z >= distanceForDarkness && useDither) {
		maskWall(ulz0.mX, urz0.mX, ulz0.mY, llz0.mY, urz0.mY, lrz0.mY);
	} else {
		drawWall(ulz0.mX, urz0.mX, ulz0.mY, llz0.mY, urz0.mY, lrz0.mY, &texture->rowMajor[0],
				 textureScale, z);
	}
}

void drawRightNear(const Vec::Vec3 center,
				   const FixP::type scale,
				   const struct Texture *texture,
				   const uint8_t mask,
				   const bool repeatTexture) {

	if (center.mZ <= kMinZCull) {
		return;
	}

	const FixP::type halfScale = scale;
	const FixP::type textureScale = (repeatTexture ? halfScale : FixP::one);
	const FixP::type minusHalfScale = (-scale);

	FixP::type depth = FixP::one;
	FixP::type minusDepth =  FixP::minusOne;


	if (mask & MASK_BEHIND) {

		projectionVertices[0].first = projectionVertices[1].first = center;

		Vec::addTo(&projectionVertices[0].first,  FixP::minusOne, minusHalfScale,  FixP::minusOne);
		Vec::addTo(&projectionVertices[1].first, FixP::one, halfScale,  FixP::minusOne);

		projectAllVertices(2);

		drawMask(projectionVertices[0].second.mX, projectionVertices[0].second.mY,
				 projectionVertices[1].second.mX, projectionVertices[1].second.mY);

		return;
	}

	if (cameraDirection == kWest || cameraDirection == kEast) {
		depth =  FixP::minusOne;
		minusDepth = FixP::one;
	}

	projectionVertices[0].first = projectionVertices[1].first = projectionVertices[2].first = projectionVertices[3].first = center;

	Vec::addTo(&projectionVertices[0].first,  FixP::minusOne, halfScale, depth);
	Vec::addTo(&projectionVertices[1].first, FixP::one, halfScale, minusDepth);
	Vec::addTo(&projectionVertices[2].first,  FixP::minusOne, minusHalfScale, depth);
	Vec::addTo(&projectionVertices[3].first, FixP::one, minusHalfScale, minusDepth);

	projectAllVertices(4);

	Vec::Vec2 ulz0 = projectionVertices[0].second;
	Vec::Vec2 urz0 = projectionVertices[1].second;
	Vec::Vec2 llz0 = projectionVertices[2].second;
	Vec::Vec2 lrz0 = projectionVertices[3].second;
	int z = FixP::toInt(center.mZ);

	if (z >= distanceForDarkness && useDither) {
		maskWall(ulz0.mX, urz0.mX, ulz0.mY, llz0.mY, urz0.mY, lrz0.mY);
	} else {
		drawWall(ulz0.mX, urz0.mX, ulz0.mY, llz0.mY, urz0.mY, lrz0.mY, &texture->rowMajor[0],
				 textureScale, z);
	}
}


/*
    *         /|x1y0
    * x0y0   / |
    *       |  |
    *       |  |
    * x0y1  |  |
    *       \  |
    *        \ |
    *         \| x1y1
    */
void maskWall(
		FixP::type x0,
		FixP::type x1,
		FixP::type x0y0,
		FixP::type x0y1,
		FixP::type x1y0,
		FixP::type x1y1) {

	if (x0 > x1) {
		/* switch x0 with x1 */
		x0 = x0 + x1;
		x1 = x0 - x1;
		x0 = x0 - x1;

		/* switch x0y0 with x1y0 */
		x0y0 = x0y0 + x1y0;
		x1y0 = x0y0 - x1y0;
		x0y0 = x0y0 - x1y0;

		/* switch x0y1 with x1y1 */
		x0y1 = x0y1 + x1y1;
		x1y1 = x0y1 - x1y1;
		x0y1 = x0y1 - x1y1;
	}

	int x = FixP::toInt(x0);
	int limit = FixP::toInt(x1);

	if (x >= XRES || limit < 0 || x == limit) {
		return;
	}

	FixP::type upperY0 = x0y0;
	FixP::type lowerY0 = x0y1;
	FixP::type upperY1 = x1y0;
	FixP::type lowerY1 = x1y1;

	if (x0y0 > x0y1) {
		upperY0 = x0y1;
		lowerY0 = x0y0;
		upperY1 = x1y1;
		lowerY1 = x1y0;
	}

	FixP::type upperDy = (upperY1 - upperY0);
	FixP::type lowerDy = (lowerY1 - lowerY0);

	FixP::type y0 = upperY0;
	FixP::type y1 = lowerY0;

	FixP::type dX = FixP::fromInt(limit - x);
	FixP::type upperDyDx = FixP::Div(upperDy, dX);
	FixP::type lowerDyDx = FixP::Div(lowerDy, dX);

	TexturePixelFormat pixel = getPaletteEntry(0xFF000000);
	FramebufferPixelFormat *bufferData = &framebuffer[0];

	/*
	  0xFF here acts as a dirty value, indicating there is no last value.
	  But even if we had textures this big, it would be only at the end of
	  the run.
	  we can use this statically, since the textures are already loaded.
	  we don't need to fetch that data on every run.
  */
	int ix = x;

	for (; ix < limit; ++ix) {
		if (ix >= 0 && ix < XRES) {

			const FixP::type diffY = (y1 - y0);
			int iY0 = FixP::toInt(y0);
			int iY1 = FixP::toInt(y1);
			FramebufferPixelFormat *destinationLine = bufferData + (XRES_FRAMEBUFFER * iY0) + ix;
			int iy;

			if (diffY == FixP::zero) {
				continue;
			}

			if (iY0 < 0) {
				iY0 = 0;
			}

			if (iY1 >= YRES) {
				iY1 = YRES;
			}

			for (iy = iY0; iy < iY1; ++iy) {
				*(destinationLine) = TEXTURE_PIXEL_TO_FB_PIXEL(pixel);
				destinationLine += (XRES_FRAMEBUFFER);
			}
		}
		y0 += upperDyDx;
		y1 += lowerDyDx;
	}
}

/*
    *         /|x1y0
    * x0y0   / |
    *       |  |
    *       |  |
    * x0y1  |  |
    *       \  |
    *        \ |
    *         \| x1y1
    */
void drawWall(FixP::type x0,
			  FixP::type x1,
			  FixP::type x0y0,
			  FixP::type x0y1,
			  FixP::type x1y0,
			  FixP::type x1y1,
			  const TexturePixelFormat *texture,
			  const FixP::type textureScaleY,
			  const int z) {

	const TexturePixelFormat *data = texture;
	const int textureWidth = NATIVE_TEXTURE_SIZE;
	const FixP::type textureSize = FixP::fromInt(textureWidth);
	FramebufferPixelFormat *bufferData = &framebuffer[0];
	bool farForStipple = (z >= distanceForPenumbra);

	if (x0 > x1) {
		/* switch x0 with x1 */
		x0 = x0 + x1;
		x1 = x0 - x1;
		x0 = x0 - x1;

		/* switch x0y0 with x1y0 */
		x0y0 = x0y0 + x1y0;
		x1y0 = x0y0 - x1y0;
		x0y0 = x0y0 - x1y0;

		/* switch x0y1 with x1y1 */
		x0y1 = x0y1 + x1y1;
		x1y1 = x0y1 - x1y1;
		x0y1 = x0y1 - x1y1;
	}

	int x = FixP::toInt(x0);
	int limit = FixP::toInt(x1);

	if (x >= XRES || limit < 0 || x == limit) {
		return;
	}

	FixP::type upperY0 = x0y0;
	FixP::type lowerY0 = x0y1;
	FixP::type upperY1 = x1y0;
	FixP::type lowerY1 = x1y1;

	if (x0y0 > x0y1) {
		upperY0 = x0y1;
		lowerY0 = x0y0;
		upperY1 = x1y1;
		lowerY1 = x1y0;
	}

	FixP::type upperDy = (upperY1 - upperY0);
	FixP::type lowerDy = (lowerY1 - lowerY0);

	FixP::type y0 = upperY0;
	FixP::type y1 = lowerY0;

	FixP::type dX = FixP::fromInt(limit - x);

	FixP::type upperDyDx = FixP::Div(upperDy, dX);
	FixP::type lowerDyDx = FixP::Div(lowerDy, dX);

	FixP::type u = 0;

	/*
	   0xFF here acts as a dirty value, indicating there is no last value.
	   But even if we had textures this big, it would be only at the end of
	   the run.
	  we can use this statically, since the textures are already loaded.
	  we don't need to fetch that data on every run.
   */

	FixP::type du = FixP::Div(textureSize, dX);
	int ix = x;
	TexturePixelFormat pixel;
	UVCoord lastV;

	for (; ix < limit; ++ix) {
		if (ix >= 0 && ix < XRES) {

			const FixP::type diffY = (y1 - y0);
			FixP::type v = 0;
			int iu = FixP::toInt(u);
			int iY0 = FixP::toInt(y0);
			int iY1 = FixP::toInt(y1);
			const TexturePixelFormat *sourceLineStart = data + (iu * textureWidth);
			const TexturePixelFormat *lineOffset = sourceLineStart;
			FramebufferPixelFormat *destinationLine = bufferData + (XRES_FRAMEBUFFER * iY0) + ix;
			FixP::type dv;
			int iy;

			if (diffY == FixP::zero) {
				continue;
			}

			dv = FixP::Div(FixP::Mul(textureSize, textureScaleY), diffY);

			lastV = 0;
			pixel = *(lineOffset);

			for (iy = iY0; iy < iY1; ++iy) {

				if (iy < YRES && iy >= 0) {
					const int iv = FixP::toInt(v);
#ifndef GAMMA_DECAY
					const bool stipple = !((ix + iy) & 1);
#else
					const bool stipple = false;
#endif
					if (iv != lastV && !(shouldDrawLights && stipple && farForStipple)) {

						pixel = *(lineOffset);
						lineOffset = ((iv & (textureWidth - 1)) + sourceLineStart);
						lastV = iv;
					}

					if (pixel != kTransparencyColour) {
						FramebufferPixelFormat color = TEXTURE_PIXEL_TO_FB_PIXEL(pixel);

#ifndef GAMMA_DECAY
						if (shouldDrawLights && farForStipple && stipple) {
							color = getPaletteEntry(0xFF000000);
						}
#else
						if (shouldDrawLights && farForStipple) {
							color = gammaForColour(pixel, 2 * z);
						}
#endif
						*(destinationLine) = color;
					}
				}
				destinationLine += (XRES_FRAMEBUFFER);
				v += dv;
			}
		}
		y0 += upperDyDx;
		y1 += lowerDyDx;
		u += du;
	}
}

void drawMask(
		const FixP::type x0,
		const FixP::type y0,
		const FixP::type x1,
		const FixP::type y1) {

	int _x0 = FixP::toInt(x0);
	int _y0 = FixP::toInt(y0);
	int _x1 = FixP::toInt(x1);
	int _y1 = FixP::toInt(y1);

	if (_x0 < 0) {
		_x0 = 0;
	}

	if (_x1 < 0) {
		_x1 = 0;
	}

	if (_y0 < 0) {
		_y0 = 0;
	}

	if (_y1 < 0) {
		_y1 = 0;
	}

	if (_x0 >= XRES) {
		_x0 = XRES - 1;
	}

	if (_x1 >= XRES) {
		_x1 = XRES - 1;
	}

	if (_y0 >= YRES) {
		_y0 = YRES - 1;
	}

	if (_y1 >= YRES) {
		_y1 = YRES - 1;
	}

	fill(_x0, _y0, _x1 - _x0, _y1 - _y0, getPaletteEntry(0xFF000000), false);
}

void drawFrontWall(FixP::type x0,
				   FixP::type y0,
				   FixP::type x1,
				   FixP::type y1,
				   const TexturePixelFormat *texture,
				   const FixP::type textureScaleY,
				   const int z,
				   const bool enableAlpha,
				   const int size) {

	/* if we have a quad in which the base is smaller */
	if (y0 > y1) {
		/* switch y0 with y1 */
		y0 = y0 + y1;
		y1 = y0 - y1;
		y0 = y0 - y1;
	}

	int y = FixP::toInt(y0);
	int limit = FixP::toInt(y1);

	if (y == limit) {
		/* degenerate */
		return;
	}

	/* what if the quad is flipped horizontally? */
	if (x0 > x1) {
		x0 = x0 + x1;
		x1 = x0 - x1;
		x0 = x0 - x1;
	}


	/*
	  0xFF here acts as a dirty value, indicating there is no last value.
	  But even if we had textures this big, it would be only at the end of
	  the run.
   */
	int iX0 = FixP::toInt(x0);
	int iX1 = FixP::toInt(x1);

	if (iX0 == iX1) {
		/* degenerate case */
		return;
	}

	const int textureWidth = size;
	const int textureHeight = size;
	const FixP::type textureSize = FixP::fromInt(textureWidth);
	const FixP::type texWidth = FixP::fromInt(textureWidth);
	FixP::type diffX = (x1 - x0);

	FixP::type du = FixP::Div(texWidth, diffX);
	TexturePixelFormat pixel;
	FixP::type v = 0;
	UVCoord lastU;
	UVCoord lastV = 0xFF;
	const TexturePixelFormat *data = texture;
	FramebufferPixelFormat *bufferData = &framebuffer[0];
	bool farEnoughForStipple = (z >= distanceForPenumbra);
	FixP::type dY = (y1 - y0);
	FixP::type dv = FixP::Div(FixP::Mul(textureSize, textureScaleY) - FixP::one, dY);
	int iy = y;

	for (; iy < limit; ++iy) {

		if (iy < YRES && iy >= 0) {
			FixP::type u = 0;
			const UVCoord iv = FixP::toInt(v) % textureHeight;
			const TexturePixelFormat *sourceLineStart = data + (iv * textureHeight);
			FramebufferPixelFormat *destinationLine = bufferData + (XRES_FRAMEBUFFER * iy) + iX0;
			int ix;
			lastU = 0;
#ifdef PALETTE_COLOURS_FRAMEBUFFER
			if (!farEnoughForStipple
				&& ((!enableAlpha && iv == lastV)
					&& (iX1 < XRES && iX0 >= 0))) {
				int start = (0 >= iX0) ? 0 : iX0;
				int finish = ((XRES - 1) >= iX1) ? iX1 : (XRES - 1);
				v += dv;
				destinationLine = bufferData + (XRES_FRAMEBUFFER * iy);
				sourceLineStart = destinationLine - XRES_FRAMEBUFFER;
				memcpy(destinationLine + start, sourceLineStart + start,
					   finish - start);

				continue;
			}
#endif
			pixel = *(sourceLineStart);

			for (ix = iX0; ix < iX1; ++ix) {

				if (ix < XRES && ix >= 0) {
#ifndef GAMMA_DECAY
					const bool stipple = ((ix + iy) & 1) == 1;
#else
					const bool stipple = false;
#endif

					const UVCoord iu = FixP::toInt(u) % textureWidth;
					/*
								  only fetch the next texel if we really changed the
								  u, v coordinates (otherwise, would fetch the same
								  thing anyway)
								   */
					if (iu != lastU
						&& !(shouldDrawLights && stipple && farEnoughForStipple)) {

						pixel = *(sourceLineStart);
						sourceLineStart += (iu - lastU);
						lastU = iu;
						lastV = iv;
					}

					if (pixel != kTransparencyColour) {
						FramebufferPixelFormat color;

#ifndef GAMMA_DECAY
						if (shouldDrawLights && farEnoughForStipple && stipple) {
							color = getPaletteEntry(0xFF000000);
						}
#else
						if (shouldDrawLights && farEnoughForStipple) {
							color = gammaForColour(TEXTURE_PIXEL_TO_FB_PIXEL(pixel), 2 * z);
						}
#endif
						else {
							color = TEXTURE_PIXEL_TO_FB_PIXEL(pixel);
						}

						*(destinationLine) = color;
					}
				}
				++destinationLine;
				u += du;
			}
		}
		v += dv;
	}
}

/*
    *     x0y0 ____________ x1y0
    *         /            \
    *        /             \
    *  x0y1 /______________\ x1y1
    */
void maskFloor(
		FixP::type y0, FixP::type y1, FixP::type x0y0, FixP::type x1y0, FixP::type x0y1, FixP::type x1y1) {

	/*
	  0xFF here acts as a dirty value, indicating there is no last value.
	  But even if we had textures this big, it would be only at the end of
	  the run.
   */

	/* if we have a trapezoid in which the base is smaller */
	if (y0 > y1) {
		/* switch y0 with y1 */
		y0 = y0 + y1;
		y1 = y0 - y1;
		y0 = y0 - y1;

		/* switch x0y0 with x0y1 */
		x0y0 = x0y0 + x0y1;
		x0y1 = x0y0 - x0y1;
		x0y0 = x0y0 - x0y1;

		/* switch x1y0 with x1y1 */
		x1y0 = x1y0 + x1y1;
		x1y1 = x1y0 - x1y1;
		x1y0 = x1y0 - x1y1;
	}

	int y = FixP::toInt(y0);
	int limit = FixP::toInt(y1);

	if (y == limit || limit < 0 || y >= YRES) {
		return;
	}

	FixP::type upperX0 = x0y0;
	FixP::type upperX1 = x1y0;
	FixP::type lowerX0 = x0y1;
	FixP::type lowerX1 = x1y1;

	/* what if the trapezoid is flipped horizontally? */
	if (x0y0 > x1y0) {
		upperX0 = x1y0;
		upperX1 = x0y0;
		lowerX0 = x1y1;
		lowerX1 = x0y1;
	}

	FixP::type leftDX = (lowerX0 - upperX0);
	FixP::type rightDX = (lowerX1 - upperX1);
	FixP::type dY = (y1 - y0);
	FixP::type leftDxDy = FixP::Div(leftDX, dY);
	FixP::type rightDxDy = FixP::Div(rightDX, dY);
	FixP::type x0 = upperX0;
	FixP::type x1 = upperX1;
	int iy = y;
	FramebufferPixelFormat pixel = getPaletteEntry(0xFF000000);
	FramebufferPixelFormat *bufferData = &framebuffer[0];

	for (; iy < limit; ++iy) {

		if (iy < YRES && iy >= 0) {

			const FixP::type diffX = (x1 - x0);
			int iX0 = FixP::toInt(x0);
			int iX1 = FixP::toInt(x1);

			if (diffX == FixP::zero) {
				continue;
			}

			if (iX0 < 0) {
				iX0 = 0;
			}

			if (iX1 >= XRES) {
				iX1 = XRES - 1;
			}

			if (iX1 < 0) {
				iX1 = 0;
			}

			if (iX0 >= XRES) {
				iX0 = XRES - 1;
			}

			size_t len = (iX1 - iX0);
			FramebufferPixelFormat *ptr = bufferData + (XRES_FRAMEBUFFER * iy) + iX0;
			while (len--) {
				*ptr = pixel;
				++ptr;
			}
		}

		x0 += leftDxDy;
		x1 += rightDxDy;
	}
}

/*
    *     x0y0 ____________ x1y0
    *         /            \
    *        /             \
    *  x0y1 /______________\ x1y1
    */
void drawFloor(FixP::type y0,
			   FixP::type y1,
			   FixP::type x0y0,
			   FixP::type x1y0,
			   FixP::type x0y1,
			   FixP::type x1y1,
			   int z,
			   const TexturePixelFormat *texture) {

	/* if we have a trapezoid in which the base is smaller */
	if (y0 > y1) {
		/* switch y0 with y1 */
		y0 = y0 + y1;
		y1 = y0 - y1;
		y0 = y0 - y1;

		/* switch x0y0 with x0y1 */
		x0y0 = x0y0 + x0y1;
		x0y1 = x0y0 - x0y1;
		x0y0 = x0y0 - x0y1;

		/* switch x1y0 with x1y1 */
		x1y0 = x1y0 + x1y1;
		x1y1 = x1y0 - x1y1;
		x1y0 = x1y0 - x1y1;
	}

	int y = FixP::toInt(y0);
	int limit = FixP::toInt(y1);

	if (y == limit || limit < 0 || y >= YRES) {
		return;
	}

	FixP::type upperX0 = x0y0;
	FixP::type upperX1 = x1y0;
	FixP::type lowerX0 = x0y1;
	FixP::type lowerX1 = x1y1;

	/* what if the trapezoid is flipped horizontally? */
	if (x0y0 > x1y0) {
		upperX0 = x1y0;
		upperX1 = x0y0;
		lowerX0 = x1y1;
		lowerX1 = x0y1;
	}

	FixP::type leftDX = (lowerX0 - upperX0);
	FixP::type rightDX = (lowerX1 - upperX1);
	FixP::type dY = (y1 - y0);
	FixP::type leftDxDy = FixP::Div(leftDX, dY);
	FixP::type rightDxDy = FixP::Div(rightDX, dY);
	FixP::type x0 = upperX0;
	FixP::type x1 = upperX1;

	const TexturePixelFormat *data = texture;
	const int textureWidth = NATIVE_TEXTURE_SIZE;
	const FixP::type textureSize = FixP::fromInt(textureWidth);
	const TexturePixelFormat *sourceLineStart;
	bool farEnoughForStipple = (z >= distanceForPenumbra);
	FixP::type dv = FixP::Div(textureSize, dY);
	TexturePixelFormat pixel;
	FramebufferPixelFormat *bufferData = &framebuffer[0];
	FixP::type v = 0;
	UVCoord lastU;

	for (int iy = y; iy < limit; ++iy) {

		if (iy < YRES && iy >= 0) {

			const FixP::type diffX = (x1 - x0);
			int iX0 = FixP::toInt(x0);
			int iX1 = FixP::toInt(x1);

			FixP::type u = 0;
			FixP::type du;
			int iv;
			FramebufferPixelFormat *destinationLine;
			lastU = 0;

			if (diffX == FixP::zero) {
				continue;
			}

			du = FixP::Div(textureSize, diffX);
			iv = FixP::toInt(v);
			destinationLine = bufferData + (XRES_FRAMEBUFFER * iy) + iX0;
			sourceLineStart = data + (iv * textureWidth);
			pixel = *(sourceLineStart);

			for (int ix = iX0; ix < iX1; ++ix) {

				if (ix >= 0 && ix < XRES) {
					const int iu = FixP::toInt(u);

#ifndef GAMMA_DECAY
					const bool stipple = ((ix + iy) & 1) == 0;
#else
					const bool stipple = false;
#endif
					/*
								  only fetch the next texel if we really changed the
								  u, v coordinates (otherwise, would fetch the same
								  thing anyway)
								  */
					if (iu != lastU
						&& !(shouldDrawLights && stipple && farEnoughForStipple)) {

						pixel = *(sourceLineStart);
						sourceLineStart += (iu - lastU);
						lastU = iu;
					}

					if (pixel != kTransparencyColour) {
						FramebufferPixelFormat color = TEXTURE_PIXEL_TO_FB_PIXEL(pixel);

#ifndef GAMMA_DECAY
						if (shouldDrawLights && farEnoughForStipple && stipple) {
							color = getPaletteEntry(0xFF000000);
						}
#else
						if (shouldDrawLights && farEnoughForStipple) {
							color = gammaForColour(pixel, 2 * z);
						}
#endif

						*(destinationLine) = color;
					}
				}
				++destinationLine;
				u += du;
			}
		}

		x0 += leftDxDy;
		x1 += rightDxDy;
		v += dv;
	}
}

void fillBottomFlat(const int *coords, FramebufferPixelFormat colour) {

	FixP::type x0 = FixP::fromInt(coords[0]);
	FixP::type y0 = FixP::fromInt(coords[1]);
	FixP::type x1 = FixP::fromInt(coords[2]);
	FixP::type y1 = FixP::fromInt(coords[3]);
	FixP::type x2 = FixP::fromInt(coords[4]);
	FixP::type y2 = FixP::fromInt(coords[5]);

	FixP::type dX1X0 = (x1 - x0);
	FixP::type dX0X2 = (x0 - x2);
	FixP::type dY1Y0 = (y1 - y0);
	FixP::type dY2Y0 = (y2 - y0);

	if (dY2Y0 == 0 || dY1Y0 == 0) {
		return;
	}

	FixP::type dXDy2 = FixP::Div(dX0X2, dY2Y0);
	FixP::type dXDy1 = FixP::Div(dX1X0, dY1Y0);
	FixP::type fX0 = x0;
	FixP::type fX1 = x0;

	int yFinal = min(coords[3], coords[5]);
	for (int y = coords[1]; y < yFinal; ++y) {
		if (y >= YRES_FRAMEBUFFER) {
			return;
		} else if (y >= 0) {
			int iFX1 = max(min((XRES - 1), FixP::toInt(fX1)), 0);
			int iFX0 = max(min((XRES - 1), FixP::toInt(fX0)), 0);
			FramebufferPixelFormat *destination = &framebuffer[(XRES_FRAMEBUFFER * y) + min(iFX0, iFX1)];
			size_t len = abs(iFX1 - iFX0);
			while (len--) {
				*destination = colour;
				++destination;
			}
		}
		fX0 -= dXDy2;
		fX1 += dXDy1;
	}
}


void fillTopFlat(int *coords, FramebufferPixelFormat colour) {

	FixP::type x0 = FixP::fromInt(coords[0]);
	FixP::type y0 = FixP::fromInt(coords[1]);
	FixP::type x1 = FixP::fromInt(coords[2]);
	FixP::type y1 = FixP::fromInt(coords[3]);
	FixP::type x2 = FixP::fromInt(coords[4]);
	FixP::type y2 = FixP::fromInt(coords[5]);

	FixP::type dX1X0 = (x1 - x0);
	FixP::type dX2X0 = (x2 - x0);
	FixP::type dY0Y1 = (y0 - y1);
	FixP::type dY0Y2 = (y0 - y2);

	if (dY0Y1 == 0 || dY0Y2 == 0) {
		return;
	}

	FixP::type dXDy1 = FixP::Div(dX1X0, dY0Y1);
	FixP::type dXDy2 = FixP::Div(dX2X0, dY0Y2);
	FixP::type fX0 = x0; //p1
	FixP::type fX1 = x0; //p2

	int yFinal = max(coords[3], coords[5]);

	for (int y = coords[1]; y >= yFinal; --y) {
		if (y < 0) {
			return;
		} else if (y < YRES_FRAMEBUFFER) {
			int iFX1 = max(min((XRES - 1), FixP::toInt(fX1)), 0);
			int iFX0 = max(min((XRES - 1), FixP::toInt(fX0)), 0);
			FramebufferPixelFormat *destination = &framebuffer[(XRES_FRAMEBUFFER * y) + min(iFX0, iFX1)];
			size_t len = abs(iFX1 - iFX0);
			while (len--) {
				*destination = colour;
				++destination;
			}
		}

		fX0 += dXDy1;
		fX1 += dXDy2;
	}
}


void fillTriangle(int *coords, FramebufferPixelFormat colour) {

	int upper = -1;
	int lower = -1;
	int other = 0;

	for (int c = 0; c < 3; ++c) {
		if (upper == -1 || coords[(2 * c) + 1] < coords[(2 * upper) + 1]) {
			upper = c;
		}

		if (lower == -1 || coords[(2 * c) + 1] > coords[(2 * lower) + 1]) {
			lower = c;
		}
	}

	if (lower == 0 || upper == 0) {
		other = 1;
	}

	if ((lower == 1 || upper == 1) && (other == 1)) {
		other = 2;
	}

	if ((lower == 2 || upper == 2) && (other == 2)) {
		other = 0;
	}

	int newCoors[6];
	int newCoors2[6];

	newCoors[0] = coords[2 * upper];
	newCoors[1] = coords[(2 * upper) + 1];
	newCoors[2] = coords[2 * lower];
	newCoors[3] = coords[(2 * lower) + 1];
	newCoors[4] = coords[2 * other];
	newCoors[5] = coords[(2 * other) + 1];

	newCoors2[0] = coords[2 * lower];
	newCoors2[1] = coords[(2 * lower) + 1];
	newCoors2[2] = coords[2 * other];
	newCoors2[3] = coords[(2 * other) + 1];
	newCoors2[4] = coords[2 * upper];
	newCoors2[5] = coords[(2 * upper) + 1];


	fillBottomFlat(&newCoors[0], colour);
	fillTopFlat(&newCoors2[0], colour);
}

void drawTexturedBottomFlatTriangle(int *coords, UVCoord *uvCoords, struct Texture *texture) {
	int u, v;

	int yFinal = coords[5]; //not the lowest, neither the topmost

	FixP::type x0 = FixP::fromInt(coords[0]);
	FixP::type y0 = FixP::fromInt(coords[1]);
	FixP::type x1 = FixP::fromInt(coords[2]);
	FixP::type y1 = FixP::fromInt(coords[3]);
	FixP::type x2 = FixP::fromInt(coords[4]);
	FixP::type y2 = FixP::fromInt(coords[5]);

	FixP::type u0 = FixP::fromInt(uvCoords[0]);
	FixP::type v0 = FixP::fromInt(uvCoords[1]);
	FixP::type u1 = FixP::fromInt(uvCoords[2]);
	FixP::type v1 = FixP::fromInt(uvCoords[3]);
	FixP::type u2 = FixP::fromInt(uvCoords[4]);
	FixP::type v2 = FixP::fromInt(uvCoords[5]);


	FixP::type dX1X0 = (x1 - x0);
	FixP::type dX0X2 = (x0 - x2);
	FixP::type dY1Y0 = (y1 - y0);
	FixP::type dY2Y0 = (y2 - y0);


	if (dY2Y0 == 0 || dY1Y0 == 0) {
		return;
	}

	FixP::type dXDy2;
	FixP::type dXDy1;
	FixP::type fX0;
	FixP::type fX1;

	dXDy2 = FixP::Div(dX0X2, dY2Y0);
	dXDy1 = FixP::Div(dX1X0, dY1Y0);
	fX0 = x0;
	fX1 = x0;

	FixP::type fV2, fU2;
	FixP::type fV1 = fV2 = v0;
	FixP::type fU1 = fU2 = u0;
	FixP::type effectiveDelta;
	FixP::type fDU1;
	FixP::type fDU2;
	FixP::type fDV1;
	FixP::type fDV2;

	int y = coords[1];

#ifndef FASTER_TEXTURE_MAP
	effectiveDelta = FixP::fromInt((coords[5]) - y);
	fDU1 = FixP::Div((u2 - u0), effectiveDelta);
	fDV1 = FixP::Div((v2 - v0), effectiveDelta);

	effectiveDelta = (FixP::fromInt((coords[3]) - y));
	fDU2 = FixP::Div((u1 - u0), effectiveDelta);
	fDV2 = FixP::Div((v1 - v0), effectiveDelta);
#else
	effectiveDelta = FixP::Div(FixP::one, FixP::intToFix((coords[5]) - y));
	fDU1 = FixP::Mul((u2 - u0), effectiveDelta);
	fDV1 = FixP::Mul((v2 - v0), effectiveDelta);

	effectiveDelta = FixP::Div(FixP::one, FixP::intToFix((coords[3]) - y));
	fDU2 = FixP::Mul((u1 - u0), effectiveDelta);
	fDV2 = FixP::Mul((v1 - v0), effectiveDelta);
#endif

	for (; y < yFinal; ++y) {

		int iFX1;
		int iFX0;
		bool flipped;
		FixP::type texelLineX;
		FixP::type texelLineY;
		FixP::type texelLineDX;
		FixP::type texelLineDY;
		FixP::type oneOverLimit;
		int limit;

		fU1 += fDU1;
		fV1 += fDV1;
		fU2 += fDU2;
		fV2 += fDV2;

		flipped = (fX0 > fX1);

		if (flipped) {
			iFX1 = FixP::toInt(fX0);
			iFX0 = FixP::toInt(fX1);
		} else {
			iFX1 = FixP::toInt(fX1);
			iFX0 = FixP::toInt(fX0);
		}

		limit = iFX1 - iFX0;

		if (limit) {
			FramebufferPixelFormat *destination;
			oneOverLimit = FixP::Div(FixP::one, FixP::fromInt(limit));

			destination = &framebuffer[(XRES_FRAMEBUFFER * y) + iFX0];

			if (flipped) {
				texelLineDX = FixP::Mul((fU1 - fU2), oneOverLimit);
				texelLineDY = FixP::Mul((fV1 - fV2), oneOverLimit);
				texelLineX = fU2;
				texelLineY = fV2;
			} else {
				texelLineDX = FixP::Mul((fU2 - fU1), oneOverLimit);
				texelLineDY = FixP::Mul((fV2 - fV1), oneOverLimit);
				texelLineX = fU1;
				texelLineY = fV1;
			}

			if (y >= 0 && y <= YRES) {

				int xPos = iFX0;

				while (limit--) {
					u = abs(FixP::toInt(texelLineX)) % NATIVE_TEXTURE_SIZE;
					v = abs(FixP::toInt(texelLineY)) % NATIVE_TEXTURE_SIZE;

					if (xPos >= 0 && xPos <= XRES) {
						*destination = TEXTURE_PIXEL_TO_FB_PIXEL(*(&texture->rowMajor[0] + (NATIVE_TEXTURE_SIZE * v) + u));
					}
					++xPos;
					++destination;
					texelLineX += texelLineDX;
					texelLineY += texelLineDY;
				}
			}
		}
		fX0 -= dXDy2;
		fX1 += dXDy1;
	}
}


void drawTexturedTopFlatTriangle(int *coords, UVCoord *uvCoords, struct Texture *texture) {

	FixP::type x0 = FixP::fromInt(coords[0]);
	FixP::type y0 = FixP::fromInt(coords[1]);
	FixP::type x1 = FixP::fromInt(coords[2]);
	FixP::type y1 = FixP::fromInt(coords[3]);
	FixP::type x2 = FixP::fromInt(coords[4]);
	FixP::type y2 = FixP::fromInt(coords[5]);

	FixP::type u0 = FixP::fromInt(uvCoords[0]);
	FixP::type v0 = FixP::fromInt(uvCoords[1]);
	FixP::type u1 = FixP::fromInt(uvCoords[2]);
	FixP::type v1 = FixP::fromInt(uvCoords[3]);
	FixP::type u2 = FixP::fromInt(uvCoords[4]);
	FixP::type v2 = FixP::fromInt(uvCoords[5]);

	FixP::type dX1X0 = (x1 - x0);
	FixP::type dX2X0 = (x2 - x0);
	FixP::type dY0Y1 = (y0 - y1);
	FixP::type dY0Y2 = (y0 - y2);

	if (dY0Y1 == 0 || dY0Y2 == 0) {
		return;
	}

	FixP::type dXDy1 = FixP::Div(dX1X0, dY0Y1);
	FixP::type dXDy2 = FixP::Div(dX2X0, dY0Y2);
	FixP::type fX0 = x0; //p1
	FixP::type fX1 = x0; //p2

	FixP::type fU2, fV2;
	FixP::type fDU1;
	FixP::type fDV1;
	FixP::type fDU2;
	FixP::type fDV2;
	FixP::type fV1 = fV2 = v0;
	FixP::type fU1 = fU2 = u0;

	FixP::type effectiveDelta;
	int y = coords[1];

#ifndef FASTER_TEXTURE_MAP
	effectiveDelta = (FixP::fromInt(y - (coords[3])));
	fDU1 = FixP::Div((u1 - u0), effectiveDelta);
	fDV1 = FixP::Div((v1 - v0), effectiveDelta);

	effectiveDelta = (FixP::fromInt(y - (coords[5])));
	fDU2 = FixP::Div((u2 - u0), effectiveDelta);
	fDV2 = FixP::Div((v2 - v0), effectiveDelta);
#else
	effectiveDelta = FixP::Div(FixP::one, FixP::intToFix(y - (coords[3])));
	fDU1 = FixP::Mul((u1 - u0), effectiveDelta);
	fDV1 = FixP::Mul((v1 - v0), effectiveDelta);

	effectiveDelta = FixP::Div(FixP::one, FixP::intToFix(y - (coords[5])));
	fDU2 = FixP::Mul((u2 - u0), effectiveDelta);
	fDV2 = FixP::Mul((v2 - v0), effectiveDelta);
#endif

	int u, v;
	int yFinal = coords[3]; //not the upper, not the lowest

	for (; y >= yFinal; --y) {
		int iFX1;
		int iFX0;
		int flipped;
		FixP::type texelLineX;
		FixP::type texelLineY;
		FixP::type texelLineDX;
		FixP::type texelLineDY;
		FixP::type oneOverLimit;
		int limit;

		fU1 += fDU1;
		fV1 += fDV1;
		fU2 += fDU2;
		fV2 += fDV2;

		flipped = (fX0 > fX1);

		if (flipped) {
			iFX1 = FixP::toInt(fX0);
			iFX0 = FixP::toInt(fX1);
		} else {
			iFX1 = FixP::toInt(fX1);
			iFX0 = FixP::toInt(fX0);
		}

		limit = iFX1 - iFX0;

		if (limit) {
			FramebufferPixelFormat *destination;
			oneOverLimit = FixP::Div(FixP::one, FixP::fromInt(limit));

			destination = &framebuffer[(XRES_FRAMEBUFFER * y) + iFX0];

			if (flipped) {
				texelLineDX = FixP::Mul((fU1 - fU2), oneOverLimit);
				texelLineDY = FixP::Mul((fV1 - fV2), oneOverLimit);
				texelLineX = fU2;
				texelLineY = fV2;
			} else {
				texelLineDX = FixP::Mul((fU2 - fU1), oneOverLimit);
				texelLineDY = FixP::Mul((fV2 - fV1), oneOverLimit);
				texelLineX = fU1;
				texelLineY = fV1;
			}

			if (y >= 0 && y <= YRES) {

				int xPos = iFX0;


				while (limit--) {
					u = abs(FixP::toInt(texelLineX)) % NATIVE_TEXTURE_SIZE;
					v = abs(FixP::toInt(texelLineY)) % NATIVE_TEXTURE_SIZE;

					if (xPos >= 0 && xPos <= XRES) {
						*destination = TEXTURE_PIXEL_TO_FB_PIXEL(*(&texture->rowMajor[0] + (NATIVE_TEXTURE_SIZE * v) + u));
					}
					++xPos;
					++destination;
					texelLineX += texelLineDX;
					texelLineY += texelLineDY;
				}
			}
		}

		fX0 += dXDy1;
		fX1 += dXDy2;
	}
}

void drawTexturedTriangle(int *coords, UVCoord *uvCoords, struct Texture *texture) {

	int upper = -1;
	int lower = -1;
	int other = 0;

	for (int c = 0; c < 3; ++c) {
		if (upper == -1 || coords[(2 * c) + 1] < coords[(2 * upper) + 1]) {
			upper = c;
		}

		if (lower == -1 || coords[(2 * c) + 1] > coords[(2 * lower) + 1]) {
			lower = c;
		}
	}

	if (lower == 0 || upper == 0) {
		other = 1;
	}

	if ((lower == 1 || upper == 1) && (other == 1)) {
		other = 2;
	}

	if ((lower == 2 || upper == 2) && (other == 2)) {
		other = 0;
	}

	int newCoors[6];
	UVCoord newUV[6];

	newCoors[0] = coords[2 * upper];
	newCoors[1] = coords[(2 * upper) + 1];
	newCoors[2] = coords[2 * lower];
	newCoors[3] = coords[(2 * lower) + 1];
	newCoors[4] = coords[2 * other];
	newCoors[5] = coords[(2 * other) + 1];

	newUV[0] = 32 - uvCoords[(2 * upper) + 1];
	newUV[1] = uvCoords[2 * upper];

	newUV[2] = 32 - uvCoords[(2 * lower) + 1];
	newUV[3] = uvCoords[2 * lower];

	newUV[4] = 32 - uvCoords[(2 * other) + 1];
	newUV[5] = uvCoords[2 * other];

	drawTexturedBottomFlatTriangle(&newCoors[0], &newUV[0], texture);

	newCoors[0] = coords[2 * lower];
	newCoors[1] = coords[(2 * lower) + 1];
	newCoors[2] = coords[2 * other];
	newCoors[3] = coords[(2 * other) + 1];
	newCoors[4] = coords[2 * upper];
	newCoors[5] = coords[(2 * upper) + 1];

	newUV[0] = 32 - uvCoords[(2 * lower) + 1];
	newUV[1] = uvCoords[2 * lower];

	newUV[2] = 32 - uvCoords[(2 * other) + 1];
	newUV[3] = uvCoords[2 * other];

	newUV[4] = 32 - uvCoords[(2 * upper) + 1];
	newUV[5] = uvCoords[2 * upper];

	drawTexturedTopFlatTriangle(&newCoors[0], &newUV[0], texture);
}

