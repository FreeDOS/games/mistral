#include "Compat.h"
#include "FixP.h"

namespace FixP {
	const static int kIntegerPart = 16;

	int toInt(const type fp) {
		return (int32_t) (fp) >> kIntegerPart;
	}

	type fromInt(const int v) {
		return ((type) ((v) << kIntegerPart));
	}

	type Mul(const type v1, const type v2) {
		return ((type) ((((v1) >> 6) * ((v2) >> 6)) >> 4));
	}

	type Div(const type v1, const type v2) {
		return ((type) ((((int64_t) (v1)) * (1 << kIntegerPart)) / (v2)));
	}
}
