#include <stdlib.h>
#include <string.h>

#include "Compat.h"
#include "FixP.h"
#include "Enums.h"
#include "Vec.h"
#include "Actor.h"
#include "LoadBitmap.h"
#include "Dungeon.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "Renderer.h"
#include "Rasterizer.h"
#include "Tesselation.h"

int clippingY1 = YRES_FRAMEBUFFER;
bool shouldDrawLights = true;
bool useDither = true;
struct Bitmap *defaultFont;

#ifdef TRUE_COLOURS_FRAMEBUFFER
#if __cplusplus > 199711L
static constexpr
#endif
FramebufferPixelFormat blendColours( FramebufferPixelFormat c0, FramebufferPixelFormat c1) {

	int r0 = (c0 >> 24 ) & 0xFF;
	int r1 = (c1 >> 24 ) & 0xFF;

	int g0 = (c0 >> 16 ) & 0xFF;
	int g1 = (c1 >> 16 ) & 0xFF;

	int b0 = (c0 >> 8 ) & 0xFF;
	int b1 = (c1 >> 8 ) & 0xFF;

	return ( 0xFF ) + (
			( ( ( r0 + r1 ) / 2) << 24 ) +
			( ( ( g0 + g1 ) / 2) << 16 ) +
			( ( ( b0 + b1 ) / 2) << 8 ) );
}
#endif

void drawRect(
		const int x,
		const int y,
		const size_t dx,
		const size_t dy,
		const FramebufferPixelFormat pixel) {

	if (pixel == kTransparencyColour) {
		return;
	}

	FramebufferPixelFormat *destination = &framebuffer[0];
	FramebufferPixelFormat *destinationLineStart = destination + (XRES_FRAMEBUFFER * (y)) + x;

	for (int _c = 0; _c <= dx; ++_c) {
		*destinationLineStart = pixel;
		*(destinationLineStart + (XRES_FRAMEBUFFER * dy)) = pixel;
		++destinationLineStart;
	}


	for (int py = 1; py < (dy); ++py) {
		destinationLineStart = destination + (XRES_FRAMEBUFFER * (y + py)) + x;
		*destinationLineStart = pixel;
		destinationLineStart += dx;
		*destinationLineStart = pixel;
	}

}

void fill(
		const int x,
		const int y,
		const size_t dx,
		const size_t dy,
		const FramebufferPixelFormat pixel,
		const bool stipple) {

	if (pixel == kTransparencyColour) {
		return;
	}

	FramebufferPixelFormat *destination = &framebuffer[0];

#ifdef PALETTE_COLOURS_FRAMEBUFFER
	for (int py = 0; py < dy; ++py) {
		FramebufferPixelFormat *destinationLineStart = destination + (XRES_FRAMEBUFFER * (y + py)) + x;

		if (!stipple) {
			int px;
			for (px = 0; px < dx; ++px) {
				*destinationLineStart = pixel;
				destinationLineStart++;
			}
		} else {
			int px;
			for (px = 0; px < dx; ++px) {
				destinationLineStart++;
				if ((px + py) & 1) {
					*destinationLineStart = pixel;
				}
			}
		}
	}
#else
	for (int py = 0; py < dy; ++py) {
		FramebufferPixelFormat *destinationLineStart = destination + (XRES_FRAMEBUFFER * (y + py)) + x;

		if (!stipple) {
			int px;
			for (px = 0; px < dx; ++px) {
				*destinationLineStart = pixel;
				destinationLineStart++;
			}
		} else {
			int px;
			for (px = 0; px < dx; ++px) {
				*destinationLineStart =  blendColours(*destinationLineStart, pixel);
				destinationLineStart++;
			}
		}
	}
#endif
}

void drawBitmap(const int dx,
				const int dy,
				struct Bitmap *tile,
				const bool transparent) {

	size_t height = tile->height;
	size_t width = tile->width;

	if ((dy + height) >= YRES_FRAMEBUFFER) {
		height = (YRES_FRAMEBUFFER - dy);
	}

	FramebufferPixelFormat *destination = &framebuffer[0];
	BitmapPixelFormat *sourceLine = tile->data;

	for (size_t y = 0; y < height; ++y) {
		FramebufferPixelFormat *destinationLineStart = destination + (XRES_FRAMEBUFFER * (dy + y)) + dx;
		BitmapPixelFormat *sourceLineStart = sourceLine + (width * y);
		size_t x;

		if ((dy + y) >= clippingY1) {
			return;
		}

		for (x = 0; x < width; ++x) {
			BitmapPixelFormat pixel = *sourceLineStart;

			if ((x + dx ) < XRES_FRAMEBUFFER && (y + dy) < YRES_FRAMEBUFFER ) {			
			  if (!transparent || (pixel != kTransparencyColour)) {
			    *destinationLineStart = pixel;
			  }
			}

			++sourceLineStart;
			++destinationLineStart;
		}
	}
}

void drawRepeatBitmap(
		const int x,
		const int y,
		const size_t dx,
		const size_t dy,
		struct Bitmap *tile) {

	size_t repeatX = (dx / tile->width) + 1;
	size_t repeatY = (dy / tile->height) + 1;

	int py = y;

	for (size_t c = 0; c < repeatY; ++c) {

		int px = x;

		for (size_t d = 0; d < repeatX; ++d) {
		  // drawBitmap will handle the cropping
		  drawBitmap(px, py, tile, false);
		  px += tile->width;
		}

		py += tile->height;
	}
}

void drawTextAt(const int x, const int y, const char *text, const FramebufferPixelFormat colour) {

	int dstX = (x - 1) * 8;
	int dstY = (y - 1) * 8;
	FramebufferPixelFormat *dstBuffer = &framebuffer[0];
	size_t fontWidth = defaultFont->width;
	BitmapPixelFormat *fontPixelData = defaultFont->data;

	size_t len = strlen(text);
	for (size_t c = 0; c < len; ++c) {
		int ascii = text[c] - ' ';
		int line = ascii >> 5;
		int col = ascii & 31;
		BitmapPixelFormat *letter =
				fontPixelData + (col * 8) + (fontWidth * (line * 8));

		if (text[c] == '\n' || dstX >= XRES_FRAMEBUFFER) {
			dstX = (x - 1) * 8;
			dstY += 8;
			continue;
		}

		if (text[c] == ' ' || text[c] == '\r') {
			dstX += 8;
			continue;
		}

		for (int srcY = 0; srcY < 8; ++srcY) {

			BitmapPixelFormat *letterSrc = letter + (fontWidth * srcY);
			FramebufferPixelFormat *letterDst = dstBuffer + dstX + (XRES_FRAMEBUFFER * (dstY + srcY));

			for (int srcX = 0; srcX < 8; ++srcX) {

				if ((*letterSrc) != kTransparencyColour) {
					*letterDst = colour;
				}

				++letterSrc;
				++letterDst;
			}
		}
		dstX += 8;
	}
}

void renderPageFlip(OutputPixelFormat *stretchedBuffer, FramebufferPixelFormat *currentFrame,
					FramebufferPixelFormat *prevFrame, int turnState, int turnTarget, bool scale200To240) {

	FramebufferPixelFormat index;
	FramebufferPixelFormat *src;
	OutputPixelFormat *dst;

	if (abs(turnTarget - turnStep) < PAGE_FLIP_INCREMENT) {
		turnStep =  turnTarget;
	}

	if (scale200To240) {
		int dstY = 0;
		int scaller = 0;
		int heightY;

		if (turnTarget == turnState || (mTurnBuffer != kCommandNone) ) {

			for (int y = 0; y < YRES_FRAMEBUFFER; ++y) {

				if (scaller == 4) {
					heightY = 2;
				} else {
					heightY = 1;
				}

				for (int chunky = 0; chunky < heightY; ++chunky) {

					dst = stretchedBuffer;
					src = &currentFrame[(XRES_FRAMEBUFFER * y)];
					dst += (XRES_FRAMEBUFFER * (dstY + chunky));

					for (int x = 0; x < XRES_FRAMEBUFFER; ++x) {
						index = *src;
						*dst = FB_PIXEL_TO_OUTPUT_PIXEL(palette, index);
						++src;
						++dst;
					}
				}

				dstY++;
				scaller++;

				if (scaller == 5) {
					scaller = 0;
					dstY++;
				}
			}

			if (mTurnBuffer != kCommandNone) {
				mBufferedCommand = mTurnBuffer;
			}

			mTurnBuffer = kCommandNone;

			memcpy(prevFrame, currentFrame, XRES_FRAMEBUFFER * YRES_FRAMEBUFFER * sizeof(FramebufferPixelFormat));

		} else if (turnState < turnTarget) {

			for (int y = 0; y < YRES_FRAMEBUFFER; ++y) {

				if (scaller == 4) {
					heightY = 2;
				} else {
					heightY = 1;
				}

				for (int chunky = 0; chunky < heightY; ++chunky) {

					dst = stretchedBuffer;
					dst += (XRES_FRAMEBUFFER * (dstY + chunky));

					for (int x = 0; x < XRES_FRAMEBUFFER; ++x) {
						if (x < XRES && y >= 8) {
							if (x >= turnStep ) {
								index = prevFrame[(XRES_FRAMEBUFFER * y) + x - turnStep];
							} else {
								index = currentFrame[(XRES_FRAMEBUFFER * y) + x - ( XRES_FRAMEBUFFER - XRES)- turnStep];
							}

						} else {
							index = currentFrame[(XRES_FRAMEBUFFER * y) + x];
						}

						*dst = FB_PIXEL_TO_OUTPUT_PIXEL(palette, index);
						++dst;
					}
				}

				dstY++;
				scaller++;

				if (scaller == 5) {
					scaller = 0;
					dstY++;
				}
			}

			turnStep += PAGE_FLIP_INCREMENT;
		} else {

			for (int y = 0; y < YRES_FRAMEBUFFER; ++y) {

				if (scaller == 4) {
					heightY = 2;
				} else {
					heightY = 1;
				}

				for (int chunky = 0; chunky < heightY; ++chunky) {

					dst = stretchedBuffer;
					dst += (XRES_FRAMEBUFFER * (dstY + chunky));

					for (int x = 0; x < XRES_FRAMEBUFFER; ++x) {

						if (x < XRES && y >= 8) {

							if (x >= turnStep) {
								index = currentFrame[(XRES_FRAMEBUFFER * y) + x - turnStep];
							} else {
								index = prevFrame[(XRES_FRAMEBUFFER * y) + x - (XRES_FRAMEBUFFER - XRES) - turnStep];
							}

						} else {
							index = currentFrame[(XRES_FRAMEBUFFER * y) + x];
						}

						*dst = FB_PIXEL_TO_OUTPUT_PIXEL(palette, index);
						++dst;
					}
				}

				dstY++;
				scaller++;

				if (scaller == 5) {
					scaller = 0;
					dstY++;
				}
			}
			turnStep -= PAGE_FLIP_INCREMENT;
		}
	} else {

		if (turnTarget == turnStep || (mTurnBuffer != kCommandNone)) {

			for (int y = 0; y < YRES_FRAMEBUFFER; ++y) {

				dst = stretchedBuffer;
				src = &currentFrame[(XRES_FRAMEBUFFER * y)];
				dst += (XRES_FRAMEBUFFER * y);

				for (int x = 0; x < XRES_FRAMEBUFFER; ++x) {
					index = *src;
					*dst = FB_PIXEL_TO_OUTPUT_PIXEL(palette, index);
					++src;
					++dst;
				}
			}

			if (mTurnBuffer != kCommandNone) {
				mBufferedCommand = mTurnBuffer;
			}

			mTurnBuffer = kCommandNone;

        	memcpy(prevFrame, currentFrame, XRES_FRAMEBUFFER * YRES_FRAMEBUFFER * sizeof(FramebufferPixelFormat));

		} else if (turnState < turnTarget) {

			for (int y = 0; y < YRES_FRAMEBUFFER; ++y) {
				dst = stretchedBuffer;
				dst += (XRES_FRAMEBUFFER * y);
				for (int x = 0; x < XRES_FRAMEBUFFER; ++x) {
					if (x < XRES && y >= 8) {
						if (x >= turnStep) {
							index = prevFrame[(XRES_FRAMEBUFFER * y) + x - turnStep];
						} else {
							index = currentFrame[(XRES_FRAMEBUFFER * y) + x - (XRES_FRAMEBUFFER - XRES) - turnStep];
						}

					} else {
						index = currentFrame[(XRES_FRAMEBUFFER * y) + x];
					}
					*dst = FB_PIXEL_TO_OUTPUT_PIXEL(palette, index);
					++dst;
				}
			}

			turnStep += PAGE_FLIP_INCREMENT;
		} else {

			for (int y = 0; y < YRES_FRAMEBUFFER; ++y) {
				dst = stretchedBuffer;
				dst += (XRES_FRAMEBUFFER * y);
				for (int x = 0; x < XRES_FRAMEBUFFER; ++x) {
					if (x < XRES && y >= 8) {

						if (x >= turnStep) {
							index = currentFrame[(XRES_FRAMEBUFFER * y) + x - turnStep];
						} else {
							index = prevFrame[(XRES_FRAMEBUFFER * y) + x - (XRES_FRAMEBUFFER - XRES) - turnStep];
						}

					} else {
						index = currentFrame[(XRES_FRAMEBUFFER * y) + x];
					}

					*dst = FB_PIXEL_TO_OUTPUT_PIXEL(palette, index);
					++dst;
				}
			}
			turnStep -= PAGE_FLIP_INCREMENT;
		}
	}
}
