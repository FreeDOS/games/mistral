#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "Compat.h"
#include "FixP.h"
#include "Enums.h"
#include "Vec.h"
#include "MapWithCharKey.h"
#include "Dungeon.h"
#include "Globals.h"
#include "LoadBitmap.h"
#include "Tile3DProperties.h"
#include "Renderer.h"
#include "Rasterizer.h"
#include "Tesselation.h"
#include "VisibilityStrategy.h"
#include "PackedFileReader.h"

FramebufferPixelFormat framebuffer[XRES_FRAMEBUFFER * YRES_FRAMEBUFFER];
FramebufferPixelFormat previousFrame[XRES_FRAMEBUFFER * YRES_FRAMEBUFFER];

#ifndef SOFTWARE_PALETTE_FRAMEBUFFER
uint8_t palette; // dummy to satisfy the macro
#else
PaletteFormat palette[256];
#endif

bool hasSnapshot = false;
Vec::Vec2i cursor;

Vec::Vec2i gunSpeed;
Vec::Vec2i gunPosition;
Vec::Vec2i gunTargetPosition;
bool grabbingDisk = false;
int showMuzzleFlashSpriteTime = kMuzzleFlashTimeInMs;

bool covered = false;
int currentTarget = Mission::kTargetNone;

bool visibilityCached = false;
bool needsToRedrawVisibleMeshes = true;
bool shouldShowDamageHighlight = false;
bool shouldShowDetectedHighlight = false;
int highlightDisplayTime = 0;

MapWithCharKey<bool> occluders;
MapWithCharKey<bool> colliders;
MapWithCharKey<bool> enemySightBlockers;

enum EDirection cameraDirection;
Vec::Vec3 mCamera;
Vec::Vec2i cameraPosition;
Vec::Vec3 cameraOffset;

bool linesOfSight[Mission::kMapSize][Mission::kMapSize];
bool revealed[Mission::kMapSize][Mission::kMapSize];

MapWithCharKey<CTile3DProperties> tileProperties;

enum ECommand mBufferedCommand = kCommandNone;
enum ECommand mTurnBuffer = kCommandNone;
int turnTarget = 0;
int turnStep = 0;

enum Visibility::EStatus visMap[Mission::kMapSize * Mission::kMapSize];
Vec::Vec2i distances[2 * Mission::kMapSize * Mission::kMapSize];

struct Bitmap *backdrop = NULL;
struct Bitmap *foe0;
struct Bitmap *foe1;
struct Bitmap *hostage;
struct Bitmap *foeBack;
struct Bitmap *target;
struct Bitmap *clue;
struct Bitmap *deadFoe;
struct Bitmap *pistol;
struct Bitmap *diskInHand;
struct Bitmap *muzzleFlash;
struct Bitmap *myMuzzleFlash;

void hideGun() {
	gunTargetPosition.y = YRES;
	showMuzzleFlashSpriteTime = 0;
}

void resetGun() {
	gunPosition.y = YRES;
	gunPosition.x = XRES / 4;
	gunTargetPosition.y = YRES;
	gunTargetPosition.x = XRES / 4;
	showMuzzleFlashSpriteTime = 0;
}

void shootGun() {
	gunTargetPosition.y = YRES - pistol->height;
	gunTargetPosition.x = gunPosition.x;
	gunPosition.x -= 8;

	if (gunPosition.x < 0) {
		gunPosition.x = 0;
	}

	gunPosition.y = (5 * (YRES - pistol->height)) / 4;
}

void grabDisk() {
	grabbingDisk = true;
	gunPosition.y = YRES / 2;
	gunTargetPosition.y = YRES;
}

void showGun(const bool showMuzzleFlash) {
	grabbingDisk = false;
	showMuzzleFlashSpriteTime =
			(showMuzzleFlash && cursor.x != -1) ? kMuzzleFlashTimeInMs : 0;
	gunTargetPosition.y = YRES - pistol->height;
}

void loadTileProperties(const uint8_t levelNumber) {

	tileProperties.clearMap();
	occluders.clearMap();
	colliders.clearMap();
	enemySightBlockers.clearMap();

	gunTargetPosition.x = gunPosition.x;

	char buffer[64];

	sprintf(buffer, "props%d.bin", levelNumber);

	struct PackedFile::StaticBuffer data = PackedFile::loadFromPath(buffer);

	loadPropertyList(&buffer[0], &tileProperties);

	for (int c = 0; c < 256; ++c) {

		if (tileProperties.isPresent(c)) {
			const struct CTile3DProperties prop = tileProperties.getFromMap(c);
			occluders.setInMap(c, prop.mBlockVisibility);
			enemySightBlockers.setInMap(c, prop.mBlockEnemySight);
		} else {
			occluders.setInMap(c, false);
			enemySightBlockers.setInMap(c, false);
		}
	}

	free(data.data);
}

void loadTexturesForLevel(const uint8_t levelNumber) {
	char tilesFilename[64];

	sprintf(tilesFilename, "tiles%d.lst", levelNumber);

	struct PackedFile::StaticBuffer data = PackedFile::loadFromPath(tilesFilename);
	size_t size = data.size;
	char *head = (char *) data.data;
	char *end = head + size;
	char *nameStart = head;

	clearTextures();

	while (head != end) {
		char val = *head;
		if (val == '\n' || val == 0) {
			*head = 0;
			makeTextureFrom(nameStart);
			nameStart = head + 1;
		}
		++head;
	}

	free(data.data);

	target = loadBitmap("target.img");
	foe0 = loadBitmap("enemy0.img");
	foe1 = loadBitmap("enemy1.img");
	foeBack = loadBitmap("enemyb.img");
	deadFoe = loadBitmap("enemyd.img");
	clue = loadBitmap("clue.img");
	hostage = loadBitmap("hostage.img");
	pistol = loadBitmap("pistol.img");
	diskInHand = loadBitmap("handdisk.img");
	muzzleFlash = loadBitmap("muzzle.img");
	myMuzzleFlash = loadBitmap("myflash.img");
	backdrop = loadBitmap("backdrop.img");
}

void updateCursorForRenderer(const int x, const int z) {
	needsToRedrawVisibleMeshes = true;
	visibilityCached = false;
	cursor.x = x;
	cursor.y = z;

	if (x == -1) {
		gunTargetPosition.x = XRES / 4;
	}
}

void tickCamera() {
	if (abs(cameraOffset.mY) <= 1000) {
		cameraOffset.mY = 0;
	}

	if (cameraOffset.mY > 0) {
		cameraOffset.mY -= FixP::halfOne;
	} else if (cameraOffset.mZ > 0) {
		cameraOffset.mZ -= FixP::halfOne;
	} else if (cameraOffset.mZ < 0) {
		cameraOffset.mZ += FixP::halfOne;
	} else if (cameraOffset.mX > 0) {
		cameraOffset.mX -= FixP::halfOne;
	} else if (cameraOffset.mX < 0) {
		cameraOffset.mX += FixP::halfOne;
	} else if (cameraOffset.mY < 0) {
		cameraOffset.mY += FixP::halfOne;
	}
	needsToRedrawVisibleMeshes = true;
}

void drawMap(const struct Mission::Actor *current) {

	const Vec::Vec2i mapCamera = current->position;
	cameraDirection = current->direction;
	hasSnapshot = true;

	if (Mission::gameTicks == 0) {
		grabbingDisk = false;
	}

	if (visibilityCached) {
		return;
	}

	visibilityCached = true;
	needsToRedrawVisibleMeshes = true;

	cameraPosition = mapCamera;

	switch (cameraDirection) {
		case kNorth:
			mCamera.mX = FixP::fromInt(((Mission::kMapSize - 1) * 2) - (2 * cameraPosition.x));
			mCamera.mZ = FixP::fromInt((2 * cameraPosition.y) - ((Mission::kMapSize * 2) - 1));
			break;

		case kSouth:
			mCamera.mX = FixP::fromInt((2 * cameraPosition.x));
			mCamera.mZ = FixP::fromInt(-2 * cameraPosition.y - 1);
			break;

		case kWest:
			mCamera.mX = FixP::fromInt((2 * cameraPosition.y));
			mCamera.mZ = FixP::fromInt((2 * cameraPosition.x) - 1);
			break;

		case kEast:
			mCamera.mX = FixP::fromInt(-(2 * cameraPosition.y));
			mCamera.mZ = FixP::fromInt(((Mission::kMapSize * 2) - 1) - (2 * cameraPosition.x));
			break;
	}

	if ((cameraPosition.x + cameraPosition.y) & 1) {
		walkingBias = kWalkingBias;
	} else {
		walkingBias = 0;
	}

	Visibility::castVisibility(cameraDirection, visMap, cameraPosition,
				   distances, true, &occluders);

	++Mission::gameTicks;
}

enum ECommand getInput() {
	const enum ECommand toReturn = mBufferedCommand;
	mBufferedCommand = kCommandNone;
	return toReturn;
}

void render(const long ms) {

	if (!hasSnapshot) {
		return;
	}

	const static FixP::type standardHeight = FixP::Div(FixP::fromInt(180), FixP::fromInt(100));

	if (playerHeight < playerHeightTarget) {
		playerHeight += playerHeightChangeRate;
	}

	if (abs(gunTargetPosition.y - gunPosition.y) > 3) {
		gunSpeed.y = (gunTargetPosition.y > gunPosition.y) ? 4 : -4;
		gunPosition.y += gunSpeed.y;
		needsToRedrawVisibleMeshes = true;
	}

	if (gunTargetPosition.x != gunPosition.x) {
		gunSpeed.x = (gunTargetPosition.x > gunPosition.x) ? 4 : -4;
		gunPosition.x += gunSpeed.x;
		needsToRedrawVisibleMeshes = true;
	}

	if (needsToRedrawVisibleMeshes) {
		const static char directions[4] = {'N', 'E', 'S', 'W'};
		enum EActorsSnapshotElement actorsSnapshotElement;
		enum EItemsSnapshotElement itemsSnapshotElement;
		Vec::Vec3 tmp, tmp2;
		uint8_t lastElement = kInvalidElement;
		uint8_t element = 0;
		bool onTarget;
		Vec::Vec3 position;
		FixP::type tileHeight = 0;
		uint8_t facesMask;
		int x, z;
		highlightDisplayTime -= ms;
		needsToRedrawVisibleMeshes = false;

		clippingY1 = YRES;

		int bgPos = (YRES / 2) - 100;

		for (int c = 0; c < (XRES / 32); ++c) {
			drawBitmap(c * 32, bgPos, backdrop, false);
		}

		if (bgPos > 0) {
			fill(0, 0, XRES, bgPos, getPaletteEntry(0xFF000000), false);
		}

		// fill any missing part of the backdrop
		fill(0, YRES / 2, XRES, YRES / 2, getPaletteEntry(0xFF000000), false);

		element = Mission::map[cameraPosition.y][cameraPosition.x];

		assert(tileProperties.isPresent(element));

		struct CTile3DProperties tileProp = tileProperties.getFromMap(element);

		tileHeight = tileProp.mFloorHeight;

		FixP::type cameraHeight = -FixP::Mul(FixP::two, tileHeight);
		mCamera.mY = cameraHeight - standardHeight;

		for (int distance = (Mission::kMapSize + Mission::kMapSize - 1); distance >= 0; --distance) {
			uint8_t bucketPos;

			for (bucketPos = 0; bucketPos < Mission::kMapSize; ++bucketPos) {

				Vec::Vec2i visPos = distances[(distance * Mission::kMapSize) + bucketPos];

				if (visPos.x < 0 || visPos.y < 0 || visPos.x >= Mission::kMapSize
					|| visPos.y >= Mission::kMapSize) {
					bucketPos = Mission::kMapSize;
					continue;
				}

				revealed[visPos.y][visPos.x] = true;

				facesMask = MASK_LEFT | MASK_FRONT | MASK_RIGHT;

				position.mY = mCamera.mY;

				switch (cameraDirection) {

					case kNorth:
						x = visPos.x;
						z = visPos.y;
						onTarget = (cursor.x == x && cursor.y == z);
						element = Mission::map[z][x];

						actorsSnapshotElement = Mission::mActors[z][x];
						itemsSnapshotElement = Mission::mItems[z][x];

						position.mX =
								mCamera.mX + FixP::fromInt(-2 * ((Mission::kMapSize - 1) - x));
						position.mZ =
								mCamera.mZ + FixP::fromInt(2 * (Mission::kMapSize) - (2 * z));

						if (x > 0) {
							facesMask |= (Mission::map[z][(x - 1)] != element) ?
										 MASK_RIGHT :
										 0;
						}

						/* remember, bounds - 1! */
						if ((x < (Mission::kMapSize - 1)) && (Mission::map[z][(x + 1)] == element)) {
							facesMask &= ~MASK_LEFT;
						}

						if ((z < (Mission::kMapSize - 1)) && (Mission::map[(z + 1)][x] == element)) {
							facesMask &= ~MASK_FRONT;
						}

						if (z == cameraPosition.y - 1) {
							if (occluders.getFromMap(element)) {
								facesMask &= ~MASK_FRONT;
								facesMask |= MASK_BEHIND;
							} else {
								facesMask |= MASK_FRONT;
							}
						}

						break;

					case kSouth:
						x = visPos.x;
						z = visPos.y;

						element = Mission::map[z][x];
						onTarget = (cursor.x == x && cursor.y == z);
						actorsSnapshotElement = Mission::mActors[z][x];
						itemsSnapshotElement = Mission::mItems[z][x];

						position.mX = mCamera.mX + FixP::fromInt(-2 * x);
						position.mZ = mCamera.mZ + FixP::fromInt(2 + 2 * z);

						/*						remember, bounds - 1!*/

						if ((x > 0) && (Mission::map[z][(x - 1)] == element)) {
							facesMask &= ~MASK_LEFT;
						}

						if ((x < (Mission::kMapSize - 1)) && (Mission::map[z][(x + 1)] == element)) {
							facesMask &= ~MASK_RIGHT;
						}
						/*
										if (z < 0) {
											facesMask[1] = (visibleElementsMap[(z - 1)][x] !=
															element);
										}
				*/
						if (z == (cameraPosition.y) + 1) {

							if (occluders.getFromMap(element)) {
								facesMask &= ~MASK_FRONT;
								facesMask |= MASK_BEHIND;
							} else {
								facesMask |= MASK_FRONT;
							}
						}

						break;
					case kWest:
						x = visPos.y;
						z = visPos.x;

						element = Mission::map[x][z];
						onTarget = (cursor.x == z && cursor.y == x);
						itemsSnapshotElement = Mission::mItems[x][z];
						actorsSnapshotElement = Mission::mActors[x][z];

						position.mX = mCamera.mX + FixP::fromInt(-2 * x);
						position.mZ = mCamera.mZ + FixP::fromInt(2 - 2 * z);

						/* remember, bounds - 1! */

						if ((x > 0) && (Mission::map[(x - 1)][z] == element)) {
							facesMask &= ~MASK_LEFT;
						}

						if ((x < (Mission::kMapSize - 1)) && (Mission::map[(x + 1)][z] == element)) {
							facesMask &= ~MASK_RIGHT;
						}

						if ((z < (Mission::kMapSize - 1)) && (Mission::map[x][(z + 1)] == element)) {
							facesMask &= ~MASK_FRONT;
						}

						if (z == (cameraPosition.x) - 1) {

							if (occluders.getFromMap(element)) {
								facesMask &= ~MASK_FRONT;
								facesMask |= MASK_BEHIND;
							} else {
								facesMask |= MASK_FRONT;
							}
						}
						break;

					case kEast:
						x = visPos.y;
						z = visPos.x;

						element = Mission::map[x][z];
						onTarget = (cursor.x == z && cursor.y == x);
						actorsSnapshotElement = Mission::mActors[x][z];
						itemsSnapshotElement = Mission::mItems[x][z];

						position.mX = mCamera.mX + FixP::fromInt(2 * x);
						position.mZ = mCamera.mZ + FixP::fromInt(2 * (z - Mission::kMapSize + 1));


						/* remember, bounds - 1! */
						if ((x > 0) && (Mission::map[(x - 1)][z] == element)) {
							facesMask &= ~MASK_RIGHT;
						}

						if ((x < (Mission::kMapSize - 1)) && (Mission::map[(x + 1)][z] == element)) {
							facesMask &= ~MASK_LEFT;
						}

						if ((z < (Mission::kMapSize - 1)) && (Mission::map[x][(z - 1)] == element)) {
							facesMask &= ~MASK_FRONT;
						}

						if (z == (cameraPosition.x) + 1) {

							if (occluders.getFromMap(element)) {
								facesMask &= ~MASK_FRONT;
								facesMask |= MASK_BEHIND;
							} else {
								facesMask |= MASK_FRONT;
							}
						}
						break;
					default:
						assert(false);
				}

				assert(tileProperties.isPresent(element));

				if (lastElement != element) {
					tileProp = tileProperties.getFromMap(element);
				}

				FixP::type heightDiff = tileProp.mCeilingHeight - tileProp.mFloorHeight;
				lastElement = element;

				if (tileProp.mFloorRepeatedTextureIndex != 0xFF
					&& tileProp.mFloorRepetitions > 0) {

					switch (tileProp.mGeometryType) {
						case kRightNearWall:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, FixP::zero,
									   ((tileProp.mFloorHeight * 2)
										- FixP::fromInt(tileProp.mFloorRepetitions)),
									   FixP::zero);

							drawRightNear(
									tmp, FixP::fromInt(tileProp.mFloorRepetitions),
									&textures[tileProp.mFloorRepeatedTextureIndex],
									facesMask, true);

							break;

						case kLeftNearWall:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, FixP::zero,
									   ((tileProp.mFloorHeight * 2)
										- FixP::fromInt(tileProp.mFloorRepetitions)),
									   FixP::zero);

							drawLeftNear(
									tmp, FixP::fromInt(tileProp.mFloorRepetitions),
									&textures[tileProp.mFloorRepeatedTextureIndex], facesMask, true);
							break;

						case kCube:
						default:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, FixP::zero,
									   ((tileProp.mFloorHeight * 2)
										- FixP::fromInt(tileProp.mFloorRepetitions)),
									   FixP::zero);

							drawColumnAt(
									tmp, FixP::fromInt(tileProp.mFloorRepetitions),
									&textures[tileProp.mFloorRepeatedTextureIndex],
									facesMask, false, true);
							break;
					}
				}

				if (tileProp.mCeilingRepeatedTextureIndex != 0xFF
					&& tileProp.mCeilingRepetitions > 0) {

					switch (tileProp.mGeometryType) {
						case kRightNearWall:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, FixP::zero,
									   ((tileProp.mCeilingHeight * 2)
										+ FixP::fromInt(tileProp.mCeilingRepetitions)),
									   FixP::zero);

							drawRightNear(
									tmp, FixP::fromInt(tileProp.mCeilingRepetitions),
									&textures[tileProp.mCeilingRepeatedTextureIndex],
									facesMask, true);
							break;

						case kLeftNearWall:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, FixP::zero,
									   ((tileProp.mCeilingHeight * 2)
										+ FixP::fromInt(tileProp.mCeilingRepetitions)),
									   FixP::zero);

							drawLeftNear(
									tmp, FixP::fromInt(tileProp.mCeilingRepetitions),
									&textures[tileProp.mCeilingRepeatedTextureIndex],
									facesMask, true);
							break;

						case kCube:
						default:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, FixP::zero,
									   ((tileProp.mCeilingHeight * 2)
										+ FixP::fromInt(tileProp.mCeilingRepetitions)),
									   FixP::zero);

							drawColumnAt(
									tmp, FixP::fromInt(tileProp.mCeilingRepetitions),
									&textures[tileProp.mCeilingRepeatedTextureIndex],
									facesMask, false, true);
							break;
					}
				}

				if (tileProp.mFloorTextureIndex != 0xFF) {

					tmp.mX = position.mX;
					tmp.mY = position.mY;
					tmp.mZ = position.mZ;

					Vec::addTo(&tmp, 0, (tileProp.mFloorHeight * 2), 0);


					drawFloorAt(tmp, &textures[tileProp.mFloorTextureIndex], cameraDirection);
				}

				if (tileProp.mCeilingTextureIndex != 0xFF) {

					enum EDirection newDirection = cameraDirection;

					tmp.mX = position.mX;
					tmp.mY = position.mY;
					tmp.mZ = position.mZ;

					Vec::addTo(&tmp, 0, (tileProp.mCeilingHeight * 2), 0);

					if (cameraDirection == kNorth) {
						newDirection = kSouth;
					}
					if (cameraDirection == kSouth) {
						newDirection = kNorth;
					}

					drawCeilingAt(
							tmp, &textures[tileProp.mCeilingTextureIndex], newDirection);
				}

				if (tileProp.mGeometryType != kNoGeometry
					&& tileProp.mMainWallTextureIndex != kNoTexture) {

					int integerPart = FixP::toInt(tileProp.mCeilingHeight)
									  - FixP::toInt(tileProp.mFloorHeight);

					FixP::type adjust = 0;

					if (((heightDiff * 2) - FixP::fromInt(integerPart)) >= FixP::halfOne) {
						adjust = FixP::Div(FixP::halfOne, FixP::four);
					}

					switch (tileProp.mGeometryType) {
						case kWallNorth:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, FixP::zero,
									   ((tileProp.mFloorHeight * 2) + heightDiff),
									   FixP::zero);

							switch (cameraDirection) {
								case kNorth:
									facesMask = MASK_BEHIND;
									break;
								case kWest:
									facesMask = MASK_FORCE_LEFT;
									break;
								case kSouth:
									facesMask = MASK_FRONT;
									break;
								case kEast:
									facesMask = MASK_FORCE_RIGHT;
									break;
								default:
									facesMask = 0;
									break;
							}

							drawCornerAt(tmp, (heightDiff + FixP::Div(adjust, FixP::two)),
										 &textures[tileProp.mMainWallTextureIndex],
										 facesMask, tileProp.mNeedsAlphaTest,
										 tileProp.mRepeatMainTexture);
							break;
						case kWallWest:


							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, FixP::zero,
									   ((tileProp.mFloorHeight * 2) + heightDiff),
									   FixP::zero);

							switch (cameraDirection) {
								case kNorth:
									facesMask = MASK_FORCE_RIGHT;
									break;
								case kWest:
									facesMask = MASK_BEHIND;
									break;
								case kSouth:
									facesMask = MASK_FORCE_LEFT;
									break;
								case kEast:
									facesMask = MASK_FRONT;
									break;
								default:
									facesMask = 0;
									break;
							}

							drawCornerAt(tmp, (heightDiff + FixP::Div(adjust, FixP::two)),
										 &textures[tileProp.mMainWallTextureIndex],
										 facesMask, tileProp.mNeedsAlphaTest,
										 tileProp.mRepeatMainTexture);
							break;

						case kWallCorner:


							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, FixP::zero,
									   ((tileProp.mFloorHeight * 2) + heightDiff),
									   FixP::zero);

							switch (cameraDirection) {
								case kNorth:
									facesMask = MASK_BEHIND | MASK_FORCE_RIGHT;
									break;
								case kWest:
									facesMask = MASK_FORCE_LEFT | MASK_BEHIND;
									break;
								case kSouth:
									facesMask = MASK_FRONT | MASK_FORCE_LEFT;
									break;
								case kEast:
									facesMask = MASK_FORCE_RIGHT | MASK_FRONT;
									break;
								default:
									facesMask = 0;
									break;
							}

							drawCornerAt(tmp, (heightDiff + FixP::Div(adjust, FixP::two)),
										 &textures[tileProp.mMainWallTextureIndex],
										 facesMask, tileProp.mNeedsAlphaTest,
										 tileProp.mRepeatMainTexture);
							break;
						case kRightNearWall:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, FixP::zero,
									   ((tileProp.mFloorHeight * 2) + heightDiff),
									   FixP::zero);

							drawRightNear(
									tmp, (heightDiff + FixP::Div(adjust, FixP::two)),
									&textures[tileProp.mMainWallTextureIndex],
									facesMask, tileProp.mRepeatMainTexture);
							break;

						case kLeftNearWall:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, FixP::zero,
									   ((tileProp.mFloorHeight * 2) + heightDiff),
									   FixP::zero);

							drawLeftNear(
									tmp, (heightDiff + FixP::Div(adjust, FixP::two)),
									&textures[tileProp.mMainWallTextureIndex],
									facesMask, tileProp.mRepeatMainTexture);
							break;
						case kRampNorth: {
							uint8_t flipTextureVertical = 0;
							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							tmp2.mX = position.mX;
							tmp2.mY = position.mY;
							tmp2.mZ = position.mZ;


							flipTextureVertical = (cameraDirection == kSouth || cameraDirection == kEast);

							Vec::addTo(&tmp, 0, (tileProp.mFloorHeight * 2), 0);
							Vec::addTo(&tmp2, 0, (tileProp.mCeilingHeight * 2), 0);

							drawRampAt(tmp, tmp2, &textures[tileProp.mMainWallTextureIndex], cameraDirection,
									   flipTextureVertical);
						}
							break;

						case kRampSouth: {
							uint8_t flipTextureVertical = 0;
							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							tmp2.mX = position.mX;
							tmp2.mY = position.mY;
							tmp2.mZ = position.mZ;


							Vec::addTo(&tmp2, 0, (tileProp.mFloorHeight * 2), 0);
							Vec::addTo(&tmp, 0, (tileProp.mCeilingHeight * 2), 0);

							flipTextureVertical = (cameraDirection == kSouth || cameraDirection == kWest);

							drawRampAt(tmp, tmp2, &textures[tileProp.mMainWallTextureIndex], cameraDirection,
									   flipTextureVertical);
						}
							break;

						case kRampEast: {
							uint8_t flipTextureVertical = 0;
							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							tmp2.mX = position.mX;
							tmp2.mY = position.mY;
							tmp2.mZ = position.mZ;

							flipTextureVertical = (cameraDirection == kSouth || cameraDirection == kEast);

							Vec::addTo(&tmp2, 0, (tileProp.mFloorHeight * 2), 0);
							Vec::addTo(&tmp, 0, (tileProp.mCeilingHeight * 2), 0);

							drawRampAt(tmp, tmp2, &textures[tileProp.mMainWallTextureIndex],
									   (enum EDirection) ((cameraDirection + 1) & 3), flipTextureVertical);
						}
							break;
						case kRampWest: {
							uint8_t flipTextureVertical = 0;
							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							tmp2.mX = position.mX;
							tmp2.mY = position.mY;
							tmp2.mZ = position.mZ;


							Vec::addTo(&tmp2, 0, (tileProp.mFloorHeight * 2), 0);
							Vec::addTo(&tmp, 0, (tileProp.mCeilingHeight * 2), 0);

							flipTextureVertical = (cameraDirection == kNorth || cameraDirection == kWest);

							drawRampAt(tmp, tmp2, &textures[tileProp.mMainWallTextureIndex],
									   (enum EDirection) ((cameraDirection + 3) & 3), flipTextureVertical);
						}
							break;
						case kCube:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, FixP::zero,
									   ((tileProp.mFloorHeight * 2) + heightDiff),
									   FixP::zero);

							drawColumnAt(tmp, (heightDiff + FixP::Div(adjust, FixP::two)),
										 &textures[tileProp.mMainWallTextureIndex],
										 facesMask, tileProp.mNeedsAlphaTest,
										 tileProp.mRepeatMainTexture);
						default:
							break;
					}
				}

				if (actorsSnapshotElement != kNobody) {
					struct Bitmap *sprite = NULL;
					switch (actorsSnapshotElement) {
						case kEnemyFiring:
						case kEnemy0:
							sprite = foe0;
							break;
						case kEnemy1:
							sprite = foe1;
							break;
						case kEnemyBack:
							sprite = foeBack;
							break;
						case kPlayer:
							break;
						case kNobody:
						default:
							assert(false);
							break;
					}

					if (sprite != NULL) {
						tmp.mX = position.mX;
						tmp.mY = position.mY;
						tmp.mZ = position.mZ;

						Vec::addTo(&tmp, 0, ((tileProp.mFloorHeight * 2) + FixP::one), 0);

						if (actorsSnapshotElement == kEnemyFiring) {
							bool original = shouldDrawLights;
							shouldDrawLights = false;
							drawBillboardAt(tmp, sprite, FixP::one, sprite->width);

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, 0,
									   ((tileProp.mFloorHeight * 2) + FixP::one + FixP::halfOne),
									   0);

							drawBillboardAt(tmp, muzzleFlash, FixP::one, 32);
							shouldDrawLights = original;
						} else {
							drawBillboardAt(tmp, sprite, FixP::one, sprite->width);
						}

						if (onTarget) {
							bool original = shouldDrawLights;
							shouldDrawLights = false;

							gunTargetPosition.x =
									(XRES / 4) + (FixP::toInt(position.mX) * 4);

							if (gunTargetPosition.x < 0 || gunTargetPosition.x > XRES) {

								gunTargetPosition.x = XRES / 4;
								hideGun();
							}

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, 0,
									   (tileProp.mFloorHeight * 2) + standardHeight, 0);

							drawBillboardAt(tmp, target, FixP::one, 32);

							shouldDrawLights = original;
						}
					}
				}

				if (itemsSnapshotElement != kNoItem) {
					switch (itemsSnapshotElement) {
						case kClue:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, 0, (tileProp.mFloorHeight * 2) + FixP::one, 0);

							drawBillboardAt(tmp, clue, FixP::one, 32);
							break;

						case kHostage:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, 0,
									   FixP::Mul(tileProp.mFloorHeight, FixP::two) +
									   FixP::Div(FixP::one, FixP::four) +
									   FixP::Div(FixP::one, FixP::two),
									   0);

							drawBillboardAt(tmp, hostage, FixP::one, hostage->width);
							break;

						case kDeadEnemy:

							tmp.mX = position.mX;
							tmp.mY = position.mY;
							tmp.mZ = position.mZ;

							Vec::addTo(&tmp, 0,
									   FixP::Mul(tileProp.mFloorHeight, FixP::two) +
									   FixP::Div(FixP::one, FixP::four) +
									   FixP::Div(FixP::one, FixP::two),
									   0);

							drawBillboardAt(tmp, deadFoe, FixP::one, deadFoe->width);
							break;

						case kNoItem:
							break;

						default:
							assert(false);
							break;
					}
				}
			}
		}

		if (gunTargetPosition.y == gunPosition.y) {
			if (grabbingDisk) {
				showGun(false);
				grabbingDisk = false;
			}
			gunSpeed.y = 0;
		}

		if (gunTargetPosition.x == gunPosition.y) {
			gunSpeed.y = 0;
		}

		clippingY1 = YRES_FRAMEBUFFER;
		if (!grabbingDisk) {
			if (showMuzzleFlashSpriteTime) {
				showMuzzleFlashSpriteTime -= ms;

				needsToRedrawVisibleMeshes = true;

				drawBitmap(gunPosition.x + pistol->width / 2,
						   gunPosition.y - (pistol->height / 3), myMuzzleFlash,
						   true);
			}

			drawBitmap(gunPosition.x, gunPosition.y, pistol, true);
		} else {
			drawBitmap(gunPosition.x, gunPosition.y, diskInHand, true);
		}

		clippingY1 = YRES_FRAMEBUFFER;

		if (shouldShowDamageHighlight) {

			fill(0, 0, XRES, YRES, getPaletteEntry(0xFFFF0000), true);

			if (highlightDisplayTime <= 0) {
				shouldShowDamageHighlight = false;
			}

			needsToRedrawVisibleMeshes = true;
		}

		if (shouldShowDetectedHighlight) {

			fill(0, 0, XRES, YRES, getPaletteEntry(0xFFFFFF00), true);
			if (highlightDisplayTime <= 0) {
				shouldShowDetectedHighlight = false;
			}
			needsToRedrawVisibleMeshes = true;
		}

		fill(0, 0, XRES_FRAMEBUFFER, 8, getPaletteEntry(0xFF000000), false);

		drawTextAt(2, 1, "Agent in the field", getPaletteEntry(0xFFFFFFFF));

		fill(XRES, 8, XRES_FRAMEBUFFER - XRES, YRES_FRAMEBUFFER - 8, getPaletteEntry(0xFFFFFFFF), false);


		for (int y = 0; y < Mission::kMapSize; ++y) {
			for (x = 0; x < Mission::kMapSize; ++x) {
				uint8_t tile = Mission::map[y][x];

				if (!revealed[y][x]) {
					linesOfSight[y][x] = false;
					continue;
				}

				fill(XRES + (x), 16 + (2 * y), 1, 2, getPaletteEntry(0xFF666666), false);

				if (colliders.getFromMap(tile)) {
					fill(XRES + (x), 16 + (2 * y), 1, 2, getPaletteEntry(0xFF000000), false);
				}

				if (linesOfSight[y][x]) {
					fill(XRES + (x), 16 + (2 * y), 1, 2, getPaletteEntry(0xFF999999), false);
				}

				if (Mission::mActors[y][x] != kNobody) {
					fill(XRES + (x), 16 + (2 * y), 1, 2, getPaletteEntry(0xFFAAAAAA), false);
				}

				if (cursor.y == y && x == cursor.x) {
					fill(XRES + (x), 16 + (2 * y), 1, 2, getPaletteEntry(0xFFFFFFFF), false);
				}

				if (tile == Mission::kClueSpawnPointToken || tile == Mission::kMapExitToken) {
					fill(XRES + (x), 16 + (2 * y), 1, 2, getPaletteEntry(0xFF999999), false);
				}

				linesOfSight[y][x] = false;
			}
		}

		// draw the map border
		drawRect(XRES, 16, 63, 128, getPaletteEntry(0xFF000000));

		FramebufferPixelFormat current = getPaletteEntry(0xFF009900);
		FramebufferPixelFormat notCurrent = getPaletteEntry(0xFF00FF00);

		// draw the lines for the position
		fill(XRES, 16 + (2 * cameraPosition.y), (cameraPosition.x), 1,
			 (cameraDirection == kWest) ? current : notCurrent, false);

		fill(XRES + (cameraPosition.x), 16 + (2 * cameraPosition.y),
			 XRES_FRAMEBUFFER - (XRES + (cameraPosition.x)), 1,
			 (cameraDirection == kEast) ? current : notCurrent, false);

		fill(XRES + (cameraPosition.x), 16, 1, (2 * cameraPosition.y),
			 (cameraDirection == kNorth) ? current : notCurrent, false);

		fill(XRES + (cameraPosition.x),
             16 + (2 * cameraPosition.y),
             1,
			 (2 * Mission::kMapSize + 16 ) - (16 + (2 * cameraPosition.y)),
			 (cameraDirection == kSouth) ? current : notCurrent, false);

		fill(XRES + (cameraPosition.x), 16 + (2 * cameraPosition.y), 1,
			 2, current, false);

		char buffer[64];
		int hudPos = ((XRES_FRAMEBUFFER / 8) - 6);

		sprintf(buffer, "HP:%d \nAmmo:%d", Mission::playerCrawler.life, Mission::playerCrawler.ammo);
		drawTextAt(hudPos, 22, buffer, getPaletteEntry(0xFF000000));
		sprintf(buffer, "Dir: %c", directions[(int) (cameraDirection)]);
		drawTextAt(hudPos, 1, "Status", getPaletteEntry(0xFFFFFFFF));
		drawTextAt(hudPos, 24, buffer, getPaletteEntry(0xFF000000));

		if (covered) {
			drawTextAt(hudPos, 21, "Covered", getPaletteEntry(0xFF00FF00));
		}
	}
}
