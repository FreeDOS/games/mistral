#include <assert.h>
#include <string.h>

#include "Compat.h"
#include "FixP.h"
#include "Utils.h"
#include "Enums.h"
#include "Vec.h"


#ifndef __DJGPP__
long timeEllapsed = 0;

#ifndef WIN32

long uclock() {
	timeEllapsed += (1000 / 60);
	return timeEllapsed;
}

#endif

#endif

namespace Utils {

	Vec::Vec2i mapOffsetForDirection(const enum EDirection aDirection) {

		Vec::Vec2i toReturn = Vec::Vec2i();

		switch (aDirection) {
			case kEast:
				Vec::init(&toReturn, 1, 0);
				break;
			case kWest:
				Vec::init(&toReturn, -1, 0);
				break;
			case kSouth:
				Vec::init(&toReturn, 0, 1);
				break;
			case kNorth:
				Vec::init(&toReturn, 0, -1);
				break;
			default:
				assert(false);
		}

		return toReturn;
	}

	bool isBigEndian() {
		union {
			uint32_t i;
			char c[4];
		} e = {0x01000000};

		return e.c[0];
	}

	uint32_t toNativeEndianess(const uint32_t val) {

		if (isBigEndian()) {

			uint32_t b0 = (val & 0x000000ffu) << 24u;
			uint32_t b1 = (val & 0x0000ff00u) << 8u;
			uint32_t b2 = (val & 0x00ff0000u) >> 8u;
			uint32_t b3 = (val & 0xff000000u) >> 24u;

			return b0 | b1 | b2 | b3;
		} else {
			return val;
		}
	}

	FixP::type lerpFix(const FixP::type v0, const FixP::type v1, const FixP::type dt, const FixP::type total) {
		FixP::type delta = (v1 - v0);
		FixP::type progress = FixP::Div(dt, total);
		FixP::type reach = FixP::Mul(delta, progress);

		return (v0 + reach);
	}

	int lerpInt(const int v0, const int v1, const long t, const long total) {
		return FixP::toInt(lerpFix(FixP::fromInt(v0), FixP::fromInt(v1), FixP::fromInt(t),
								   FixP::fromInt(total)));
	}

	int countLines(const char *text) {
		size_t len = strlen(text);
		int lines = 1;    /* initial line + final line must be accounted for */
		int charsInLine = 0;
		for (int c = 0; c < len; ++c) {
			if (text[c] == '\n') {
				lines++;
				charsInLine = 0;
			} else {
				charsInLine++;
			}
		}

		return lines;
	}
}
