#include <assert.h>

#include "Compat.h"
#include "FixP.h"
#include "Enums.h"
#include "Vec.h"

namespace Vec {
	void init(Vec2i *vec, int8_t x, int8_t y) {
		vec->x = x;
		vec->y = y;
	}

	void init(Vec3 *vec, FixP::type x, FixP::type y, FixP::type z) {
		vec->mX = x;
		vec->mY = y;
		vec->mZ = z;
	}

	void addTo(Vec3 *to, FixP::type x, FixP::type y, FixP::type z) {
		to->mX = (to->mX + x);
		to->mY = (to->mY + y);
		to->mZ = (to->mZ + z);
	}


	Vec2i mapOffsetForDirection(const enum EDirection aDirection) {

		Vec2i toReturn = Vec2i();

		switch (aDirection) {
			case kEast:
				init(&toReturn, 1, 0);
				break;
			case kWest:
				init(&toReturn, -1, 0);
				break;
			case kSouth:
				init(&toReturn, 0, 1);
				break;
			case kNorth:
				init(&toReturn, 0, -1);
				break;
			default:
				assert(false);
		}

		return toReturn;
	}

	enum EDirection leftOf(const enum EDirection d) {
		switch (d) {
			case kNorth:
				return kWest;
			case kSouth:
				return kEast;
			case kEast:
				return kNorth;
			case kWest:
				return kSouth;
			default:
				return d;
		}
	}

	enum EDirection rightOf(const enum EDirection d) {
		switch (d) {
			case kNorth:
				return kEast;
			case kSouth:
				return kWest;
			case kEast:
				return kSouth;
			case kWest:
				return kNorth;
		}
		assert(false);
		return kNorth;
	}
}
