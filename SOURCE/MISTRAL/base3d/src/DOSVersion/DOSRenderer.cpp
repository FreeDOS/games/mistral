#include <conio.h>
#include <dpmi.h>
#include <go32.h>
#include <pc.h>
#include <bios.h>
#include <sys/movedata.h>
#include <sys/farptr.h>
#include <sys/nearptr.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdint.h>

#include "Enums.h"
#include "FixP.h"
#include "Vec.h"
#include "MapWithCharKey.h"
#include "PackedFileReader.h"
#include "Tile3DProperties.h"
#include "SoundSystem.h"
#include "Dungeon.h"
#include "Renderer.h"
#include "Rasterizer.h"
#include "Tesselation.h"
#include "LoadBitmap.h"

extern enum ESoundDriver soundDriver;
OutputPixelFormat stretchedBuffer[320 * 200];

enum EColor {
	COLOR_BLACK,
	COLOR_BLUE,
	COLOR_YELLOW = 14,
	COLOR_WHITE,
};

void graphicsShutdown() {
	textmode(C80);
	clrscr();
	printf(
			"Thanks for playing!\nDOS is back with a vengeance.\n\nSource code and "
			"licenses:\nhttps://bitbucket.org/MontyOnTheRun/the-mistral-report\n\n");
}

void putStr(int x, int y, const char *str, int fg, int bg) {
	int col = x;
	int row = y;
	int currAttrib = (bg << 4) | fg;
	size_t len = strlen(str);

	for (int ch = 0; ch < len; ++ch) {
		char c = str[ch];
		++col;
		_farpokeb(_dos_ds, 0xB800 * 16 + ((80 * row * 2) + (col * 2)), c);
		_farpokeb(_dos_ds, 0xB800 * 16 + ((80 * row * 2) + (col * 2)) + 1,
				  currAttrib);
	}
}

void drawTitleBox() {

	putStr(1, 2, "\xc9", COLOR_WHITE, COLOR_BLUE);

	for (int c = 0; c < 75; ++c) {
		putStr(c + 2, 2, "\xcd", COLOR_WHITE, COLOR_BLUE);
	}
	putStr(76, 2, "\xbb", COLOR_WHITE, COLOR_BLUE);

	for (int d = 3; d < 10; ++d) {
		putStr(1, d, "\xba", COLOR_WHITE, COLOR_BLUE);

		for (int c = 0; c < 75; ++c) {

			putStr(c + 2, d, " ", COLOR_WHITE, COLOR_BLUE);
		}
		putStr(76, d, "\xba", COLOR_WHITE, COLOR_BLUE);
	}

	putStr(1, 10, "\xc8", COLOR_WHITE, COLOR_BLUE);

	for (int c = 0; c < 75; ++c) {
		putStr(c + 2, 10, "\xcd", COLOR_WHITE, COLOR_BLUE);
	}
	putStr(76, 10, "\xbc", COLOR_WHITE, COLOR_BLUE);

	putStr(17, 4, "The Mistral Report - Invisible Affairs - v1.1", COLOR_WHITE,
		   COLOR_BLUE);

	putStr(5, 6, "Program and Audio-visual (C) 2018-2019 by Brotherhood of 13h",
		   COLOR_WHITE, COLOR_BLUE);

	putStr(30, 7, "Licensed under GPLv3 ", COLOR_WHITE, COLOR_BLUE);
}

void querySoundDriver() {
	putStr(30, 14, "Select Sound Driver:", COLOR_YELLOW, COLOR_BLACK);
	putStr(27, 16, "1) IBM Sound", COLOR_YELLOW, COLOR_BLACK);
	putStr(27, 17, "2) Ad lib Sound Board", COLOR_YELLOW, COLOR_BLACK);
	putStr(27, 18, "3) OPL2LPT on LPT1", COLOR_YELLOW, COLOR_BLACK);
	putStr(27, 19, "4) No Sound", COLOR_YELLOW, COLOR_BLACK);
	int option = -1;

	while (option == -1) {

		gotoxy(53, 15);
		option = getch();

		switch (option) {
			case '1':
				/*				soundDriver = ESoundDriver::kPcSpeaker; */
				soundDriver = kPcSpeaker;
				return;
			case '2':
				/*				soundDriver = ESoundDriver::kAdlib; */
				soundDriver = kAdlib;
				setupOPL2(0x0388);
				return;
			case '3':
				/*				soundDriver = ESoundDriver::kOpl2Lpt; */
				soundDriver = kOpl2Lpt;
				setupOPL2(-1);
				return;
			case '4':
				return;
			default:
				option = -1;
		}
	}
}

void graphicsInit() {
	textmode(C80);
	clrscr();
	drawTitleBox();
	querySoundDriver();

	__dpmi_regs reg;

	reg.x.ax = 0x13;
	__dpmi_int(0x10, &reg);

	outp(0x03c8, 0);

	for (int r = 0; r < 4; ++r) {
		for (int g = 0; g < 8; ++g) {
			for (int b = 0; b < 8; ++b) {
				outp(0x03c9, (r * (16)));
				outp(0x03c9, (g * (8)));
				outp(0x03c9, (b * (8)));
			}
		}
	}

	defaultFont = loadBitmap("font.img");
}

void handleSystemEvents() {

	int lastKey = 0;

	if (kbhit()) {
		unsigned char getched = getch();
		switch (getched) {
			case 'c':
				mBufferedCommand = kCommandFire3;
				visibilityCached = false;
				break;

			case 27:
			case 'q':
				mBufferedCommand = kCommandBack;
				break;

			case 's':
				mBufferedCommand = kCommandStrafeLeft;
				visibilityCached = false;
				break;
			case 'd':
				mBufferedCommand = kCommandStrafeRight;
				visibilityCached = false;
				break;

			case 'v':
				visibilityCached = false;
				break;
			case 'b':
				visibilityCached = false;
				break;

			case 'j':
				useDither = false;
				visibilityCached = false;
				break;
			case 'k':
				useDither = true;
				visibilityCached = false;
				break;
			case 13:
			case 'z':
				mBufferedCommand = kCommandFire1;
				needsToRedrawVisibleMeshes = true;
				visibilityCached = false;
				break;

			case 'x':
			case ' ':
				mBufferedCommand = kCommandFire2;
				needsToRedrawVisibleMeshes = true;
				visibilityCached = false;
				break;

			case 224:
			case 0: {
				char arrow = getch();
				switch (arrow) {
					case 75:
						mTurnBuffer = kCommandLeft;
						visibilityCached = false;
						needsToRedrawVisibleMeshes = true;
						break;
					case 72:
						mBufferedCommand = kCommandUp;
						visibilityCached = false;
						break;
					case 77:
						mTurnBuffer = kCommandRight;
						visibilityCached = false;
						needsToRedrawVisibleMeshes = true;
						break;
					case 80:
						mBufferedCommand = kCommandDown;
						visibilityCached = false;
						needsToRedrawVisibleMeshes = true;
						break;
				}
			}
				break;
		}

		if (mBufferedCommand != kCommandLeft && mBufferedCommand != kCommandRight && mBufferedCommand != kCommandNone) {
			turnStep = 0;
			turnTarget = 0;
		}
	}
}

void flipRenderer() {
	if (turnTarget == turnStep || (mTurnBuffer != kCommandNone)) {
		dosmemput(&framebuffer[0], 320 * 200, 0xa0000);
		memcpy(previousFrame, framebuffer, 320 * 200);

		mBufferedCommand = mTurnBuffer;
		mTurnBuffer = kCommandNone;

	} else {
		renderPageFlip(&stretchedBuffer[0], &framebuffer[0], &previousFrame[0], turnStep, turnTarget, false);
		dosmemput(&stretchedBuffer[0], 320 * 200, 0xa0000);
	}
}

void clear() {}
