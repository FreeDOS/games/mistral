#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "Compat.h"
#include "FixP.h"
#include "Utils.h"
#include "Enums.h"
#include "Vec.h"
#include "Dungeon.h"
#include "SpyTravel.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "Renderer.h"
#include "Rasterizer.h"
#include "Tesselation.h"
#include "Engine.h"
#include "SoundSystem.h"
#include "Actor.h"
#include "Globals.h"

namespace Mission {
	Vec::Vec2i previousPlayerPosition;
	long gameTicks = 0;
	uint8_t map[Mission::kMapSize][Mission::kMapSize];
	struct Mission::Actor playerCrawler;
	enum EActorsSnapshotElement mActors[Mission::kMapSize][Mission::kMapSize];
	enum EItemsSnapshotElement mItems[Mission::kMapSize][Mission::kMapSize];

	uint8_t collisionMap[256];

	int enemyWithClue[8] = {
			10,
			0,
			2,
			7,
			7,
			5,
			0,
			0,
	};

	const char *keyLocations[8] = {
#ifndef RATIO_16_9
			"The key is located at the \n"
			"staging area, by the north gate.",

			"The key should be near the\n"
			"lavatories, by the cliff.",

			"Look for a safe room by the\n"
			"south alcove. It's actually\n"
			"their hideout!",

			"For the pass, gain access to one\n"
			"of the rooms connected to the\n"
			"terrace.",

			"There should be a key located on\n"
			"the control room, at the central\n"
			"zone.",

			"Both targets are on the ship.\n"
			"Look for the control tower to\n"
			"gain access.",

			"There's a heavy guarded room\n"
			"that overlooks the bridge.\n"
			"It holds the key.",

			"The control room the for this\n"
			"base is located near the central\n"
			"computer.",
#else
	" The key is located at the staging\n"
			" area, by the north gate.",

			" The key should be near the\n"
			" lavatories, by the cliff.",

			" Look for a safe room by the south\n"
			" alcove. It's actually their\n"
			" hideout!",

			" For the pass, gain access to one\n"
			" of the rooms connected to the\n"
			" terrace.",

			" There should be a key located on\n"
			" the control room, at the central\n"
			" zone.",

			" Both targets are on the ship.\n"
			" Look for the control tower to\n"
			" gain access.",

			" There's a heavy guarded room that\n"
			" overlooks the bridge. It holds the\n"
			" key.",

			" The control room the for this base\n"
			" is located near the central\n"
			" computer.",
#endif
	};

	struct Actor *enemies[kMaxAgentsInBase];
	int enemyWithInfo;
	int enemiesInBase = 0;
	struct Item key;
	struct Item info;
	struct GameSnapshot gameStatus; //duplication detected!

	void updateLog() {
		char buffer[128];
		const char *keysAndTargets;

#ifndef RATIO_16_9
		if (gameStatus.targetLocated) {
			if (!gameStatus.keyCollected) {
				keysAndTargets = "Target located. Key still\npending.\n";
			} else {
				keysAndTargets = "Target and key located.\n ";
			}
		} else if (gameStatus.keyCollected) {
			keysAndTargets = "Key collected. Target location\nstill pending.\n";
		} else {
			keysAndTargets = "";
		}
#else
		if (gameStatus.targetLocated) {
			if (!gameStatus.keyCollected) {
				keysAndTargets = " Target located. Key still pending.\n";
			} else {
				keysAndTargets = " Target and key located.\n ";
			}
		} else if (gameStatus.keyCollected) {
			keysAndTargets = " Key collected. Target location\n still pending.\n";
		} else {
			keysAndTargets = "";
		}
#endif

		sprintf(&buffer[0], "%s", keysAndTargets);

		if (gameStatus.infoCollected && !gameStatus.keyCollected) {
			int location = SpyGame::getPlayerLocation();

			sprintf(textBuffer, "%s%s", &buffer[0], keyLocations[location]);

		} else {
			sprintf(textBuffer, "%s", &buffer[0]);
		}
	}

	void pursue(struct Actor *spy, Vec::Vec2i target) {

		int dx = (target.x - spy->position.x);
		int dy = (target.y - spy->position.y);
		int x = spy->position.x;
		int y = spy->position.y;
		int incX = 0;
		int incY = 0;

		if (abs(dx) >= abs(dy)) {
			if (dx >= 0) {
				incX = 1;
				spy->direction = kEast;
			} else {
				incX = -1;
				spy->direction = kWest;
			}
		} else {
			if (dy >= 0) {
				incY = 1;
				spy->direction = kSouth;
			} else {
				incY = -1;
				spy->direction = kNorth;
			}
		}

		int iX = x + incX;
		int iY = y + incY;

		spy->position.x = iX;
		spy->position.y = iY;

		if (spy->symbol == kEnemy1) {
			spy->symbol = kEnemy0;
		} else {
			spy->symbol = kEnemy1;
		}
	}

	bool isPositionAllowed(int x, int y) {

		return (0 <= x) && (x < kMapSize) && (0 <= y) && (y < kMapSize)
			   && collisionMap[map[y][x]] != kTileCollisionToken;
	}

	bool isCovered(Vec::Vec2i position) {
		return (!isPositionAllowed(position.x, position.y - 1))
			   || (!isPositionAllowed(position.x, position.y + 1))
			   || (!isPositionAllowed(position.x + 1, position.y))
			   || (!isPositionAllowed(position.x - 1, position.y));
	}

	void tickEnemy(struct Actor *actor) {

		Vec::Vec2i pos0 = actor->position;
		Vec::Vec2i pos1 = playerCrawler.position;

		int chances = gameStatus.detected || ((!gameStatus.covered) || ((gameStatus.turn % 4) == 0));

		int x = actor->position.x;
		int y = actor->position.y;

		mActors[y][x] = kNobody;

		if (actor->life == 0) {
			mItems[y][x] = kDeadEnemy;
			return;
		}

		if (chances && canSeeSpy(pos0, actor->direction, pos1)) {
			actor->target = playerCrawler.position;

			/*First detection*/
			if (!gameStatus.detected) {
				setPlayerWasDetected();
				gameStatus.detected = true;
			}

			/*Move towards player*/
			pursue(actor, actor->target);

			if (!isPositionAllowed(actor->position.x, actor->position.y)
				|| (actor->position.x == actor->target.x &&
					actor->position.y == actor->target.y)) {
				actor->position.x = x;
				actor->position.y = y;
			}

			if ((gameStatus.turn % 2) == 0) {

				/*Shooting at player*/
				actor->symbol = kEnemyFiring;
				playSound(ENEMY_FIRING_GUN);

				/*Bullseye!*/
				if (!gameStatus.covered || ((gameStatus.turn % 4) == 0)) {
					playerCrawler.life--;
					visibilityCached = false;
					needsToRedrawVisibleMeshes = true;

					setIsDamage();
					if (playerCrawler.life <= 0) {
						gameStatus.should_continue = kCrawlerGameOver;
					}
				}
			}
		} else if (isPositionAllowed(actor->position.x, actor->position.y) &&
				   isPositionAllowed(actor->target.x, actor->target.y)) {

			if ((actor->target.x != actor->position.x)
				|| (actor->target.y != actor->position.y)) {

				EActorsSnapshotElement prevSymbol = actor->symbol;

				pursue(actor, actor->target);

				if (!isPositionAllowed(actor->position.x, actor->position.y) ||
					((actor->position.x == playerCrawler.position.x)
					 && (actor->position.y == playerCrawler.position.y))) {
					actor->position.x = x;
					actor->position.y = y;
					actor->symbol = prevSymbol;
				}

				if ((abs(actor->position.x - actor->target.x) <= 1)
					&& (abs(actor->position.y - actor->target.y) <= 1)) {
					actor->target.x = actor->target.y = Vec::kDirectionInvalid;
				}
			} else {
				actor->symbol = kEnemy0;
				actor->direction = Vec::rightOf(actor->direction);
			}
		} else {
			actor->symbol = kEnemy1;
			actor->direction = Vec::leftOf(actor->direction);
		}

		mActors[actor->position.y][actor->position.x] = actor->symbol;
	}

	struct GameSnapshot tick(const enum ECommand command) {
		int oldTurn = gameStatus.turn;
		mActors[playerCrawler.position.y][playerCrawler.position.x] = kNobody;

		if (playerCrawler.life > 0) {

			Vec::Vec2i offset = Vec::mapOffsetForDirection(
					playerCrawler.direction);

			if (((playerCrawler.position.y + offset.y) == key.position.y)
				&& ((playerCrawler.position.x + offset.x) == key.position.x)
				&& key.present) {
				playSound(INFORMATION_ACQUIRED_SOUND);
				key.present = false;
				gameStatus.keyCollected = true;
				grabDisk();
				gameStatus.turn++;
			}

			if (((playerCrawler.position.y + offset.y) == info.position.y)
				&&
				((playerCrawler.position.x + offset.x) == info.position.x)
				&& info.present) {
				playSound(INFORMATION_ACQUIRED_SOUND);
				info.present = false;
				grabDisk();
				gameStatus.infoCollected = true;
				gameStatus.turn++;
			}

			switch (command) {
				case kCommandRight:
					playerCrawler.direction = Vec::rightOf(playerCrawler.direction);
					turnTarget = 0;
					turnStep = XRES;
					break;

				case kCommandLeft:
					playerCrawler.direction = Vec::leftOf(playerCrawler.direction);
					turnTarget = XRES;
					turnStep = 0;
					break;
				case kCommandUp: {
					gameStatus.turn++;

					playerCrawler.position.x += offset.x;
					playerCrawler.position.y += offset.y;

					if (collisionMap[map[playerCrawler.position.y]
					[playerCrawler.position.x]]
						== kTileCollisionToken) {
						playerCrawler.position.x -= offset.x;
						playerCrawler.position.y -= offset.y;
					}
					cameraOffset.mZ = FixP::two;
				}
					break;
				case kCommandDown: {
					gameStatus.turn++;
					playerCrawler.position.x -= offset.x;
					playerCrawler.position.y -= offset.y;

					if (collisionMap[map[playerCrawler.position.y]
					[playerCrawler.position.x]]
						== kTileCollisionToken) {
						playerCrawler.position.x += offset.x;
						playerCrawler.position.y += offset.y;
					}
					cameraOffset.mZ = -FixP::two;
				}
					break;
				case kCommandFire1:
					if (currentTarget == kTargetNone) {
						selectNextPlayerTarget();
					} else {

						if (playerCrawler.ammo > 0) {
							/* fire */
							playerCrawler.ammo--;
							shootGun();

							if (currentTarget != kTargetNone) {

								if (currentTarget == enemyWithInfo) {
									info.position = enemies[currentTarget]->position;
									info.present = true;
								}

								enemies[currentTarget]->life = 0;
								gameStatus.turn++;
								if (enemiesInBase > 0) {
									selectNextPlayerTarget();
								} else {
									currentTarget = kTargetNone;
								}
							}
						}
					}
					break;
				case kCommandFire2:
					selectNextPlayerTarget();
					break;
				case kCommandFire4:
					break;
				case kCommandNone:
					break;
				case kCommandBack:
					break;
				case kCommandPassTurn: {
					gameStatus.turn++;
				}
					break;
				case kCommandStrafeLeft: {
					Vec::Vec2i offsetLeft =
							Vec::mapOffsetForDirection(Vec::leftOf(playerCrawler.direction));
					gameStatus.turn++;

					playerCrawler.position.x += offsetLeft.x;
					playerCrawler.position.y += offsetLeft.y;

					if (collisionMap[map[playerCrawler.position.y]
					[playerCrawler.position.x]]
						== kTileCollisionToken) {
						playerCrawler.position.x -= offsetLeft.x;
						playerCrawler.position.y -= offsetLeft.y;
					}
					cameraOffset.mX = -FixP::two;
				}
					break;
				case kCommandStrafeRight: {
					Vec::Vec2i offsetRight =
							Vec::mapOffsetForDirection(Vec::rightOf(playerCrawler.direction));
					gameStatus.turn++;

					playerCrawler.position.x += offsetRight.x;
					playerCrawler.position.y += offsetRight.y;

					if (collisionMap[map[playerCrawler.position.y]
					[playerCrawler.position.x]]
						== kTileCollisionToken) {
						playerCrawler.position.x -= offsetRight.x;
						playerCrawler.position.y -= offsetRight.y;
					}
					cameraOffset.mX = FixP::two;
				}
					break;
				default:
					break;
			}
		} else {
			gameStatus.turn++;
		}

		if (currentTarget != kTargetNone) {
			gameStatus.playerTarget = enemies[currentTarget]->position;
		}

		mActors[playerCrawler.position.y][playerCrawler.position.x] = kPlayer;

		if (map[playerCrawler.position.y][playerCrawler.position.x] == kMapExitToken) {

			if (!key.present) {
				gameStatus.should_continue = kCrawlerClueAcquired;
			} else {
				gameStatus.targetLocated = true;
			}
		}

		if (oldTurn != gameStatus.turn) {

			if (currentTarget != kTargetNone &&
				(enemies[currentTarget]->life <= 0 ||
				 !canSeeSpy(playerCrawler.position, playerCrawler.direction,
							enemies[currentTarget]->position))) {
				currentTarget = kTargetNone;
				gameStatus.playerTarget.x = gameStatus.playerTarget.y = 0;
			}


			updateLog();

			for (int c = 0; c < enemiesInBase; ++c) {
				tickEnemy(enemies[c]);
			}

			if (key.present) {
				mItems[key.position.y][key.position.x] = kClue;
			} else {
				mItems[key.position.y][key.position.x] = kNoItem;
			}

			if (info.present) {
				mItems[info.position.y][info.position.x] = kClue;
			}

			gameStatus.covered = isCovered(playerCrawler.position);
		}

		return gameStatus;
	}

	void selectNextPlayerTarget() {
		int iterations = 0;
        bool canBeSeen;
        int life;
		do {
			int index = currentTarget;
			index = ((index + 1) % enemiesInBase);
			currentTarget = index;
			++iterations;
            canBeSeen = canSeeSpy(playerCrawler.position, playerCrawler.direction, enemies[currentTarget]->position);
            life = enemies[currentTarget]->life;
		} while ((iterations <= enemiesInBase) &&
                 ((life == 0) || !canBeSeen ));

		if (iterations > enemiesInBase) {
			currentTarget = kTargetNone;
		}
	}

	void loadMap(const uint8_t *mapData,
				 const char *collisions,
				 const int mapIndex) {

		gameStatus.keyCollected = false;
		gameStatus.targetLocated = false;
		gameStatus.infoCollected = false;
		gameStatus.covered = false;
		gameStatus.detected = false;
		gameStatus.should_continue = kCrawlerGameInProgress;
		gameStatus.mapIndex = mapIndex;

		playerCrawler.ammo = 15;
		playerCrawler.direction = kNorth;

		enemiesInBase = 0;

		playerCrawler.symbol = kPlayer;
		playerCrawler.direction = kNorth;
		playerCrawler.life = 5;
		playerCrawler.ammo = 15;

		currentTarget = kTargetNone;

		memcpy(&collisionMap, collisions, 256);
		memset(enemies, 0, sizeof(struct Actor *) * kMaxAgentsInBase);

		key.present = false;
		info.present = false;

		const uint8_t *ptr = mapData;

		for (int y = 0; y < kMapSize; ++y) {
			for (int x = 0; x < kMapSize; ++x) {
				uint8_t current = *ptr;
				map[y][x] = current;
				mItems[y][x] = kNoItem;
				mActors[y][x] = kNobody;


				if (current == kPlayerSpawnPointToken) {
					playerCrawler.position.x = x;
					playerCrawler.position.y = y;
					mActors[playerCrawler.position.y][playerCrawler.position.x] = kPlayer;
				}

				if (current == kClueSpawnPointToken) {
					Vec::init(&key.position, x, y);
					key.present = true;
					mItems[y][x] = kClue;
				}

				if (current == kEnemyType0SpawnPointToken || current == kEnemyType1SpawnPointToken) {

					struct Actor *soldier =
							(struct Actor *) calloc(1, sizeof(struct Actor));

					assert(soldier != NULL);

					soldier->symbol = current == kEnemyType0SpawnPointToken ? kEnemy0 : kEnemy1;
					soldier->direction = kNorth;
					soldier->life = 1;
					soldier->position.x = x;
					soldier->position.y = y;
					soldier->target.x = Vec::kDirectionInvalid;
					soldier->target.y = Vec::kDirectionInvalid;
					mActors[y][x] = soldier->symbol;
					enemies[enemiesInBase] = soldier;
					enemiesInBase++;
				}

				if (current == kHostageSpawnPointToken) {
					mItems[y][x] = kHostage;
				}

				++ptr;
			}
			++ptr;
		}

		enemyWithInfo = enemyWithClue[SpyGame::getPlayerLocation()];

		if (gameStatus.mapIndex < 8 && SpyGame::hasBeenRescued(gameStatus.mapIndex)) {
			gameStatus.should_continue = kCrawlerClueAcquired;
		}
	}
}
