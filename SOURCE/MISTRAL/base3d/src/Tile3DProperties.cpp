#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "Compat.h"
#include "FixP.h"
#include "MapWithCharKey.h"
#include "Tile3DProperties.h"
#include "PackedFileReader.h"

void loadPropertyList(const char *propertyFile, MapWithCharKey<CTile3DProperties> *map) {

	struct PackedFile::StaticBuffer buffer = PackedFile::loadFromPath(propertyFile);
	uint8_t *limit = buffer.data + buffer.size;
	uint8_t *bufferHead = buffer.data;

	map->clearMap();

	while (bufferHead != limit) {
		FixP::type val = 0;
		uint8_t key = *(bufferHead++);
		CTile3DProperties prop;
		prop.mNeedsAlphaTest = *(bufferHead++) == 1;
		prop.mBlockVisibility = *(bufferHead++)== 1;
		prop.mBlockMovement = *(bufferHead++)== 1;
		prop.mBlockEnemySight = *(bufferHead++)== 1;
		prop.mRepeatMainTexture = *(bufferHead++)== 1;
		prop.mCeilingTextureIndex = *(bufferHead++);
		prop.mFloorTextureIndex = *(bufferHead++);
		prop.mMainWallTextureIndex = *(bufferHead++);
		prop.mGeometryType = (enum GeometryType) (*(bufferHead++));
		prop.mCeilingRepeatedTextureIndex = *(bufferHead++);
		prop.mFloorRepeatedTextureIndex = *(bufferHead++);
		prop.mCeilingRepetitions = *(bufferHead++);
		prop.mFloorRepetitions = *(bufferHead++);

		val += (*(bufferHead++) << 0u);
		val += (*(bufferHead++) << 8u);
		val += (*(bufferHead++) << 16u);
		val += (*(bufferHead++) << 24u);
		prop.mCeilingHeight = val;

		val = 0;
		val += (*(bufferHead++) << 0u);
		val += (*(bufferHead++) << 8u);
		val += (*(bufferHead++) << 16u);
		val += (*(bufferHead++) << 24u);
		prop.mFloorHeight = val;

		map->setInMap(key, prop);
	}

	free(buffer.data);
}
