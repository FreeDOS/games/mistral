#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

#include "Compat.h"
#include "FixP.h"
#include "Utils.h"
#include "Enums.h"
#include "Vec.h"
#include "PackedFileReader.h"

#ifdef ANDROID
#include <jni.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <android/bitmap.h>
#include <android/asset_manager.h>

extern AAssetManager *defaultAssetManager;

int android_read(void *cookie, char *buf, int size) {
	return AAsset_read((AAsset *) cookie, buf, size);
}

int android_write(void *cookie, const char *buf, int size) {
	return EACCES;
}

fpos_t android_seek(void *cookie, fpos_t offset, int whence) {
	return AAsset_seek((AAsset *) cookie, offset, whence);
}

int android_close(void *cookie) {
	AAsset_close((AAsset *) cookie);
	return 0;
}


FILE *android_fopen(const char* filename) {

	AAsset *asset = AAssetManager_open(defaultAssetManager, "base.pfs", 0);
	if (!asset) {
		return NULL;
	}

	return funopen(asset, android_read, android_write, android_seek, android_close);

}
#endif

namespace PackedFile {

	static const size_t kDataPath_MaxLength = 256;

	char mDataPath[kDataPath_MaxLength];

	void init(const char *dataFilePath) {
		sprintf(mDataPath, "%s", dataFilePath);
	}

	struct StaticBuffer loadFromPath(const char *path) {

#ifndef ANDROID
		FILE *mDataPack = fopen(mDataPath, "rb");
#else
		FILE *mDataPack = android_fopen(&mDataPath[0]);
#endif
		struct StaticBuffer toReturn;
		uint32_t offset = 0;
		uint16_t entries = 0;
		char buffer[85];

		assert(fread(&entries, 2, 1, mDataPack));

		for (int c = 0; c < entries; ++c) {
			uint8_t stringSize = 0;

			assert(fread(&offset, 4, 1, mDataPack));
			offset = Utils::toNativeEndianess(offset);
			assert(fread(&stringSize, 1, 1, mDataPack));
			assert(fread(&buffer, stringSize + 1, 1, mDataPack));

			if (!strcmp(buffer, path)) {
				goto found;
			}
		}

found:

		if (offset == 0) {
			printf("failed to load %s\n", path);
			exit(-1);
		}

		fseek(mDataPack, offset, SEEK_SET);

		assert(fread(&toReturn.size, 4, 1, mDataPack));
		toReturn.size = Utils::toNativeEndianess(toReturn.size);
		toReturn.data = (uint8_t *) malloc(toReturn.size);

		assert(fread(toReturn.data, sizeof(uint8_t),toReturn.size, mDataPack));
		fclose(mDataPack);

		return toReturn;
	}
}
